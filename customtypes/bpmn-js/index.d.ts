declare module 'bpmn-js/dist/bpmn-modeler.production.min.js';
declare module 'bpmn-js/dist/bpmn-viewer.production.min.js';
declare module 'bpmn-js-properties-panel';
declare module 'bpmn-js-properties-panel/lib/provider/bpmn';
declare module 'bpmn-js-properties-panel/lib/factory/EntryFactory';
declare module 'bpmn-js/lib/features/palette/PaletteProvider';
declare module 'xml2js';