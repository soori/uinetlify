import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

export const listener = functions.https.onRequest((request, response) => {
  return admin.firestore().collection('chatroom').add({
    type: 'group',
    electionName: 'ASSEMBLY-TS-2018',
    partyName: null,
    contestantName: 'PROBLEMS',
    constitutionName: 'MEDCHAL',
    createdAt: Date.now(),
    messages: []
  });
});