import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';


// Adds a message that welcomes new users into the chat.
export const listener = functions.auth.user().onCreate((user) => {
    const fullName = user.display_name || 'Anonymous';
    return admin.database().ref('messages').push({
        name: 'Firebase Bot',
        photoUrl: '/assets/images/firebase-logo.png', // Firebase logo
        text: `${fullName} signed in for the first time! Welcome!`
    });
});