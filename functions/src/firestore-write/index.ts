import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

// Adds a message that welcomes new users into the chat.
export const listener = functions.auth.user().onCreate((user) => {
  const fireBaseUser = {
    uid: user.uid,
    name: user.display_name,
    imageUrl: user.photoURL,
    prefLang: 'en',
    email: user.email,
    phone: user.phoneNumber,
    authProvider: user.providerData[0].providerId
  };
  const userRef: FirebaseFirestore.DocumentReference = admin.firestore().doc(`users/${fireBaseUser.uid}`);
  return userRef.set(fireBaseUser, { merge: true });
  /*  userRef.set(fireBaseUser, { merge: true }).then(
     data => console.log('user document created successfully')
   ).catch( err => console.log('error')
   ); */
});


