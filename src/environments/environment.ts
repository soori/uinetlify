// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  appType: 'WEB',
  tenantId: 1,
  production: false,
  apiEndpoint: 'http://localhost:5000',
  firebase: {
    apiKey: 'AIzaSyDRQZo-MXBE7ekKUSWmQ_9dplYhWWmeSb0',
    authDomain: 'iid-whatnext.firebaseapp.com',
    databaseURL: 'https://iid-whatnext.firebaseio.com',
    projectId: 'iid-whatnext',
    storageBucket: 'iid-whatnext.appspot.com',
    messagingSenderId: '723793348384',
    appId: '1:723793348384:web:e8e41fc4d23ac906'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
