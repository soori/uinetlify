import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { AppRouteComponent } from './admin/route/route.component';
import { AppService } from './app.service';
import { AppSettings } from './app.settings';
import { Settings } from './app.settings.model';
import { UserLangPreference } from './store/model/user.lang.model';
import { UserLanguageState } from './store/userlang.state';

declare var device: any;

@Component({
  selector: 'body[app-root]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  public settings: Settings;
  addPage: any;
  pageData: any[] = [];
  pages: any;
  navigation: any;
  index = 0;
  topRoutes: any[] = [];
  constructor(public appSettings: AppSettings, translate: TranslateService, private store: Store<UserLanguageState>,
    private cdRef: ChangeDetectorRef, private router: Router, private service: AppService, private route: ActivatedRoute) {

    this.navigation = JSON.parse(localStorage.getItem('navigation') || 'null');
    this.settings = this.appSettings.settings;
    translate.setDefaultLang('en');
    const browserLang = navigator.language;
    translate.use(browserLang);
    this.store.dispatch({
      type: 'ADD_LANG',
      payload: <UserLangPreference>{
        prefLang: browserLang
      }
    });
  }

  ngOnInit() {
    document.addEventListener('deviceready', () => {
      alert(device.platform);
    }, false);
    // this.getPages();
  }
  // remove later
  ngAfterViewInit(): void {
    this.settings.loadingSpinner = false;
    this.cdRef.detectChanges();
  }
  getPages() {
    this.service.getPages().subscribe(data => {
      this.pages = data;
      this.pages.forEach((element: any) => {
        const url: any[] = element.details.url.split('/');
        this.index = 0;
        if (this.router.config.length > 0) {
          this.addDynamicRoutes(url, element, this.router.config, this.index);
        }

        const appROutes: any[] = [];
        for (let i = 0; i < this.router.config.length; i++) {
          const routePath: string | undefined = this.router.config[i].path;
          appROutes.push(routePath);
        }
        if (appROutes.indexOf(url[0]) === -1) {
          this.topRoutes.push(element);
        }
      });
      this.topRoutes.forEach(item => {
        this.pageData.push({
          path: item.details.url,
          component: AppRouteComponent,
          data: { page: item }
        });
      });
      this.router.config.unshift(...this.pageData);
      localStorage.setItem('pageData', JSON.stringify(this.pageData));
    });
  }

  routePage(url: any) {
    this.router.navigate(['./' + url], { relativeTo: this.route });
  }

  addDynamicRoutes(url: any, pageData: any, route: any, index: any) {
    route.forEach((root: any) => {
      if (root.path) {
        if (root.path === url[index]) {
          index++;
          setTimeout(() => {
            if ((root as any)._loadedConfig) {
              if (url.length > (index + 1)) {
                (root as any)._loadedConfig.routes.forEach((element: any) => {
                  if (!element.children) {
                    element.children = [];
                  }
                  this.addDynamicRoutes(url, pageData, element.children, index);
                });
              } else {
                setTimeout(() => {
                  (root as any)._loadedConfig.routes.push({
                    path: url[index],
                    component: AppRouteComponent,
                    data: { page: pageData }
                  });
                }, 1000);
              }
            }
          }, 1500);
        }
      }
    });
  }


}
