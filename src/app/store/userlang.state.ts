import { UserLangPreference } from './model/user.lang.model';

export interface UserLanguageState {
    readonly userLangPref: UserLangPreference[];
}
