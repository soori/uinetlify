import { UserLangPreference } from './../model/user.lang.model';

export const ADD_LANG = 'ADD_LANG';
export const UPDATE_LANG = 'UPDATE_LANG';

export function userLangPreferenceReducer(state: UserLangPreference[] = [], action:any) {
    switch (action.type) {
        case ADD_LANG:
            return [...state, action.payload];
        case UPDATE_LANG:
            return state.map((item, index) => {
                return Object.assign({}, item, { prefLang: action.payload.prefLang });
                return item;
            }
            );
        default:
            return state;
    }
}

