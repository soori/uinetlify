import { Location } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { RegisterService } from '../register.service';
import { RegisterDialogComponent } from './register-dialog/register-dialog.component';

@Component({
  selector: 'app-visitor-registration-homepage',
  templateUrl: './visitor-registration-homepage.component.html',
  styleUrls: ['./visitor-registration-homepage.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VisitorRegistrationHomePageComponent implements OnInit, AfterViewInit {
  public settings: Settings;
  isLoading: Boolean = false;
  permissionsLoaded: Boolean = false;
  user: any = {};
  isSubmitted: Boolean = false;
  registerType: any;

  constructor(public appSettings: AppSettings, private service: RegisterService, private location: Location,
    protected translate: TranslateService, protected store: Store<UserLanguageState>, public dialog: MatDialog,
    private router: Router, private route: ActivatedRoute) {
    this.settings = this.appSettings.settings;
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.service.getPermissions().subscribe(
      data => {
        localStorage.setItem('userPermissions', JSON.stringify(data));
        this.permissionsLoaded = true;
      }
    );
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }

  goBack() {
    this.router.navigate(['./../../homepage/visitor'], { relativeTo: this.route });
  }
  itemclick(event: any) {
    this.registerType = event._elementRef.nativeElement.id;
    const dialogRef = this.dialog.open(RegisterDialogComponent, {
      width: '90vw',
      height: '80vh',
      data: { registerType: this.registerType }
    });

    dialogRef.afterClosed().subscribe(selectedImages => {
    });
  }


  submitDetails() {
    this.isSubmitted = true;
    this.dialog.open(RegisterDialogComponent, {
      data: { registerType: 'qrCode', user: this.user }
    });
  }

}
