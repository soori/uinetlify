import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { AppSettings } from '../../../../app.settings';
import { Settings } from '../../../../app.settings.model';
import { RegisterService } from '../../register.service';

@Component({
  selector: 'app-visitor-pre-register-homepage',
  templateUrl: './visitor-pre-register-homepage.component.html',
  styleUrls: ['./visitor-pre-register-homepage.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VisitorPreRegistrationHomePageComponent implements OnInit, AfterViewInit {
  public settings: Settings;
  isLoading: Boolean = false;

  hasDevices: boolean;
  hasPermission: boolean;
  qrResultString: string;
  registerNumber: any;

  showQRScan: Boolean = false;

  availableDevices: MediaDeviceInfo[];
  currentDevice: MediaDeviceInfo;

  constructor(public appSettings: AppSettings, private service: RegisterService, private location: Location,
    protected translate: TranslateService, protected store: Store<UserLanguageState>,
    private router: Router, private route: ActivatedRoute) {
    this.settings = this.appSettings.settings;
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }

  goBack() {
    this.router.navigate(['./../../../homepage/visitor'], { relativeTo: this.route });
  }

  scanQRCode() {
    this.showQRScan = !this.showQRScan;
    // if (this.showQRScan) {

    // } else {
    //   this.hasDevices = false;
    //   this.availableDevices = null;
    //   this.currentDevice = null;
    // }
  }

  handleQrCodeResult(event: any) {
    alert(event);

  }

}
