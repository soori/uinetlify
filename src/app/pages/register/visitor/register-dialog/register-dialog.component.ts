import { Component, Inject, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { RegisterService } from '../../register.service';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.scss']
})
export class RegisterDialogComponent implements OnInit {
  registerType: any;
  public deviceId: string;
  isSubmitted: Boolean = false;
  constructor(public dialogRef: MatDialogRef<RegisterDialogComponent>, private service: RegisterService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.registerType = this.data.registerType;
  }

  getQRCOde() {
    return JSON.stringify(this.data.user);
  }
  close() {
    this.dialogRef.close(this.data);
  }

}
