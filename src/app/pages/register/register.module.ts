import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NguCarouselModule } from '@ngu/carousel';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { SharedModule } from '../../shared/shared.module';
import { UserRegistrationHomePageComponent } from './user/user-registration-homepage.component';
import { VisitorCheckoutHomePageComponent } from './visitor/checkout/visitor-checkout-homepage.component';
import { VisitorPreRegistrationHomePageComponent } from './visitor/preregister/visitor-pre-register-homepage.component';
import { VisitorRegistrationHomePageComponent } from './visitor/visitor-registration-homepage.component';
import { RegisterDialogComponent } from './visitor/register-dialog/register-dialog.component';

export const routes = [
  { path: 'visitor', component: VisitorRegistrationHomePageComponent, pathMatch: 'full' },
  { path: 'visitor/preregister', component: VisitorPreRegistrationHomePageComponent, pathMatch: 'full' },
  { path: 'visitor/checkout', component: VisitorCheckoutHomePageComponent, pathMatch: 'full' },
  { path: 'user', component: UserRegistrationHomePageComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    SharedModule,
    AgmCoreModule,
    ScrollToModule.forRoot(),
    NguCarouselModule
  ],
  declarations: [
    VisitorRegistrationHomePageComponent,
    UserRegistrationHomePageComponent,
    VisitorPreRegistrationHomePageComponent,
    VisitorCheckoutHomePageComponent,
    RegisterDialogComponent
  ],
  providers: [
  ],
  entryComponents: [RegisterDialogComponent]
})
export class RegisterModule { }
