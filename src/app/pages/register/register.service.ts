import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class RegisterService {

    constructor(private http: HttpClient) { }

    getPermissions(): Observable<any[]> {
        return this.http.get<any[]>(`${environment.apiEndpoint}/config/global/preauthenticationconfiguration/6/1/1/authCodes`);
    }
    SaveFaceRecogintion(formData: FormData) {
        return this.http.post('http://192.168.0.3:5000/face', formData);
    }

}
