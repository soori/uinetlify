import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { RegisterService } from '../register.service';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-user-registration-homepage',
  templateUrl: './user-registration-homepage.component.html',
  styleUrls: ['./user-registration-homepage.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserRegistrationHomePageComponent implements OnInit, AfterViewInit {
  public menuItems: any = {};
  public settings: Settings;
  isLoading: Boolean = false;
  constructor(public appSettings: AppSettings, private service: RegisterService, private location: Location, protected translate: TranslateService) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }

  goBack() {
    this.location.back();
  }
}
