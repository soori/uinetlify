import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class HomepageService {
    constructor(private http: HttpClient) { }

    getPermissions() {
        return this.http.get<any[]>(`${environment.apiEndpoint}/config/global/preauth/homepage/6/1/authPermissions`);
    }

    getVisitorTypes() {
        return [
            {
                icon: 'person_add',
                title: 'Account Registration',
                desc: ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
                routerLink: './../../registration/visitor',
                permissionCode: 'A01'
            },
            {
                icon: 'directions_walk',
                title: 'Walk in (New Appointment)',
                desc: ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
                routerLink: './../../registration/visitor/preregister',
                permissionCode: 'A01'
            },
            {
                icon: 'event',
                title: 'Pre Registered Appointment',
                desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
                routerLink: './../../registration/visitor/preregister',
                permissionCode: 'A02'
            },
            {
                icon: 'exit_to_app',
                title: 'Checkout',
                desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
                routerLink: './../../registration/visitor/checkout',
                permissionCode: 'A03'
            }
        ];
    }

}
