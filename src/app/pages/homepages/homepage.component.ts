import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { HomepageService } from './homepage.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomePageComponent implements OnInit, AfterViewInit {
  public settings: Settings;
  public features: any = {};
  public userType: any;
  public permissionsLoaded: Boolean = false;
  constructor(private route: ActivatedRoute, public appSettings: AppSettings, private service: HomepageService,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.settings = this.appSettings.settings;
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userType = params['usertype'] || null;
    });
    if (!this.userType) {
      this.route.queryParamMap.subscribe(queryParams => {
        this.userType = queryParams.get('usertype') || null;
      });
    }
    //localStorage.clear();
    this.features = this.service.getVisitorTypes();
    this.service.getPermissions().subscribe(
      data => {
        localStorage.setItem('userPermissions', JSON.stringify(data));
        this.permissionsLoaded = true;
      }
    );
  }
  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }
}
