import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LandingPageService {

    constructor(private http: HttpClient) { }

    getPermissions(appType: number): Observable<any[]> {
        return this.http.get<any[]>(`${environment.apiEndpoint}/config/global/preauth/landingpage/${appType}/1/usertypeCodes`);
    }
    getLoginTypes() {
        return [
            {
                icon: 'recent_actors',
                title: 'Visitor',
                desc: ' Eliminate work and human error by having the system automatically alert employees when their visitors sign in .',
                routerLink: './../homepage/visitor',
                permissionCode: 6
            },
            {
                icon: 'how_to_reg',
                title: 'Employee',
                desc: 'Eliminate work and human error by having the system automatically alert system when their employees log in .',
                routerLink: './../pages/preauth/authtype',
                permissionCode: 10
            },
            {
                icon: 'group_add',
                title: 'Admin',
                desc: ' Eliminate work and human error by having the system automatically alert employees when their visitors sign in .',
                routerLink: './../homepage/visitor',
                permissionCode: 1
            },
            {
                icon: 'verified_user',
                title: 'Product Admin',
                desc: ' Eliminate work and human error by having the system automatically alert employees when their visitors sign in .',
                routerLink: './../homepage/visitor',
                permissionCode: 2
            },
            {
                icon: 'sentiment_satisfied_alt',
                title: 'User',
                desc: 'Eliminate work and human error by having the system automatically alert system when their employees log in .',
                routerLink: './../pages/preauth/authtype',
                permissionCode: 3
            },
            {
                icon: 'perm_identity',
                title: 'Super User',
                desc: 'Eliminate work and human error by having the system automatically alert system when their employees log in .',
                routerLink: './../pages/preauth/authtype',
                permissionCode: 4
            }, {
                icon: 'account_circle',
                title: 'Guest',
                desc: ' Eliminate work and human error by having the system automatically alert employees when their visitors sign in .',
                routerLink: './../homepage/visitor',
                permissionCode: 5
            },
            {
                icon: 'format_bold',
                title: 'BOT',
                desc: 'Eliminate work and human error by having the system automatically alert system when their employees log in .',
                routerLink: './../pages/preauth/authtype',
                permissionCode: 8
            },
            {
                icon: 'contact_phone',
                title: 'Support',
                desc: 'Eliminate work and human error by having the system automatically alert system when their employees log in .',
                routerLink: './../pages/preauth/authtype',
                permissionCode: 9
            }
            ,
            {
                icon: 'flag',
                title: 'Auditor',
                desc: 'Eliminate work and human error by having the system automatically alert system when their employees log in .',
                routerLink: './../pages/preauth/authtype',
                permissionCode: 7
            }
        ];
    }

}
