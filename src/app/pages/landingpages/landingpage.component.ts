import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { LandingPageService } from './landingpage.service';


@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LandingPageComponent implements OnInit, AfterViewInit {
  public settings: Settings;
  public features: any = {};
  public permissionsLoaded: Boolean = false;
  public deviceType: any;
  constructor(private route: ActivatedRoute,
    public appSettings: AppSettings, private _service: LandingPageService,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.settings = this.appSettings.settings;
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.deviceType = params['device'] || null;
    });
    if (!this.deviceType) {
      this.route.queryParamMap.subscribe(queryParams => {
        this.deviceType = queryParams.get('device') || null;
      });
    }
    localStorage.removeItem('userPermissions');
    this.permissionsLoaded = false;
    this._service.getPermissions(this.getDeviceType(this.deviceType)).subscribe(
      data => {
        localStorage.setItem('userPermissions', JSON.stringify(data));
        this.permissionsLoaded = true;
        this.features = this._service.getLoginTypes();
      }
    );
  }
  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }

  getDeviceType(deviceTypeString: string): number {
    if (deviceTypeString) {
      if (deviceTypeString.toLowerCase().indexOf('web') !== -1) {
        return 1;
      }
      if (deviceTypeString.toLowerCase().indexOf('kiosk') !== -1) {
        return 2;
      }
      if (deviceTypeString.toLowerCase().indexOf('android') !== -1) {
        return 3;
      }
      if (deviceTypeString.toLowerCase().indexOf('ios') !== -1) {
        return 4;
      }
      if (deviceTypeString.toLowerCase().indexOf('thick') !== -1) {
        return 5;
      }
    }
    return 1;
  }
}
