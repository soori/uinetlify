import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ThemeModule } from '../../theme/theme.module';
import { SharedModule } from '../../shared/shared.module';
import { LandingPageComponent } from './landingpage.component';
import { LandingPagesRoutingModule } from './landingpages-routing.module';

@NgModule({
  declarations: [LandingPageComponent

  ],
  imports: [
    CommonModule,
    LandingPagesRoutingModule,
    SharedModule,
    ThemeModule
  ]
})
export class LandingPagesModule { }
