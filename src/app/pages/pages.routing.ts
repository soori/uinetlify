import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRouteComponent } from '../admin/route/route.component';

const routes: Routes = [
  { path: 'homepage', loadChildren: './homepages/homepages.module#HomepagesModule' },
  { path: 'landinpage', loadChildren: './landingpages/landingpages.module#LandingPagesModule' },
  { path: 'registration', loadChildren: './register/register.module#RegisterModule' },
  { path: '**', component: AppRouteComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserPagesRoutingModule { }
