import { Menu } from './menu.model';

export const verticalMenuItems = [
    new Menu(1, 'Pre Authentication', '', 'eco', true, 0),
    new Menu(2, 'Landing page', '/admin/pages/preauth/landingpage', 'flight_land', false, 1),
    new Menu(3, 'Home page', '/admin/pages/preauth/homepage', 'home', false, 1),
    new Menu(4, 'Authentication types', '/admin/pages/preauth/authtype', 'how_to_reg', false, 1),
    new Menu(5, 'User Management', '', 'verified_user', true, 0),
    new Menu(6, 'Roles', '/admin/pages/usermanagement/role/home', 'sentiment_satisfied', false, 5),
    new Menu(7, 'Users', '/admin/pages/usermanagement/users/home', 'person', false, 5),
    new Menu(8, 'Builder', '', 'category', true, 0),
    new Menu(9, 'Template', '/admin/builder/template', 'ballot', false, 8),
    new Menu(10, 'Page', '/admin/builder/page', 'pages', false, 8),
    new Menu(7, 'Menu', '/admin/pages/menu/home', 'person', false, 5)
];

export const horizontalMenuItems = [
    new Menu(1, 'Admin Landing Page', '/admin/kiosk', 'work', false, 0),
    new Menu(4, 'Pre AuthConfig', '/admin/pages/preauth', 'work', false, 0),
    new Menu(5, 'User Management', '/admin/usermanagementconfiguration', 'work', false, 0),
    new Menu(6, 'Page Builder', '/admin/form-builder', 'work', false, 0),
    new Menu(2, 'Id Wallet', '/portal/idWallet', 'work', false, 0),
    new Menu(3, 'Id Bank', '/portal/idBank', 'work', false, 0),
];



