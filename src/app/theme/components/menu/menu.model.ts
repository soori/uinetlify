export class Menu {
    constructor(public id: number,
        public display_name: string,
        public router_link: string,
        public icon: string,
        public has_sub_menu: boolean,
        public parent_id: number) { }
}
