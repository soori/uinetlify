import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { horizontalMenuItems, verticalMenuItems } from './menu';
import { Menu } from './menu.model';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private location: Location, private router: Router, private http: HttpClient) { }

  // public getHorizontalMenuItems(): Observable<any> {
  //   return this.http.get(`${environment.apiEndpoint}/config/global/menu/fullMenu`);
  // }

  public getVerticalMenuItemsFromApi(): Observable<Menu[]> {
    return this.http.get<Menu[]>(`${environment.apiEndpoint}/menu`);
  }

  public getVerticalMenuItems(): Array<Menu> {
    return verticalMenuItems;
  }

  public getHorizontalMenuItems(): Array<Menu> {
    return horizontalMenuItems;
  }

  public expandActiveSubMenu(menu: any) {
    const url = this.location.path();
    const routerLink = url; // url.substring(1, url.length);
    const activeMenuItem = menu.filter((item: any) => item.routerLink === routerLink);
    if (activeMenuItem[0]) {
      let menuItem = activeMenuItem[0];
      while (menuItem.parentId !== 0) {
        const parentMenuItem = menu.filter((item: any) => item.id === menuItem.parentId)[0];
        menuItem = parentMenuItem;
        this.toggleMenuItem(menuItem.id);
      }
    }
  }

  public toggleMenuItem(menuId: any) {
    const menuItem = document.getElementById('menu-item-' + menuId);
    const subMenu = document.getElementById('sub-menu-' + menuId);
    if (subMenu) {
      if (subMenu.classList.contains('show')) {
        subMenu.classList.remove('show');
        if (menuItem && menuItem.classList.contains('expanded')) {
          menuItem.classList.remove('expanded');
        }
      } else {
        subMenu.classList.add('show');
        if (menuItem) {
          menuItem.classList.add('expanded');
        }
      }
    }
  }

  public closeOtherSubMenus(menu: any, menuId: any) {
    const currentMenuItem = menu.filter((item: any) => item.id === menuId)[0];
    if (currentMenuItem.parentId === 0 && !currentMenuItem.target) {
      menu.forEach((item: any) => {
        if (item.id !== menuId) {
          const subMenu = document.getElementById('sub-menu-' + item.id);
          const menuItem = document.getElementById('menu-item-' + item.id);
          if (subMenu) {
            if (subMenu.classList.contains('show')) {
              subMenu.classList.remove('show');
              if (menuItem) {
                menuItem.classList.remove('expanded');
              }
            }
          }
        }
      });
    }
  }

  public closeAllSubMenus() {
    const menu = document.getElementById('vertical-menu');
    if (menu) {
      for (let i = 0; i < menu.children[0].children.length; i++) {
        const child = menu.children[0].children[i];
        if (child) {
          if (child.children[0].classList.contains('expanded')) {
            child.children[0].classList.remove('expanded');
            child.children[1].classList.remove('show');
          }
        }
      }
    }
  }
}
