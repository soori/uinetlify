import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserMenuComponent implements OnInit {
  public userImage = 'assets/img/users/user.jpg';
  @Output()
  openSettingsDrawer: EventEmitter<any> = new EventEmitter();
  showSettings: Boolean = false;
  loggedInUser: any;
  constructor(public afAuth: AngularFireAuth, public router: Router) { }

  ngOnInit() {
    this.loggedInUser = JSON.parse(localStorage.getItem('LoggedInUser') || 'null');
  }

  logout() {
    localStorage.clear();
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/authenticate']);
    });
  }
  openSettings() {
    this.showSettings = true;
    this.openSettingsDrawer.emit(this.showSettings);
  }
}
