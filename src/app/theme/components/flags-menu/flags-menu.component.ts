import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { Store } from '@ngrx/store';
import { UserLangPreference } from 'src/app/store/model/user.lang.model';

@Component({
  selector: 'app-flags-menu',
  templateUrl: './flags-menu.component.html',
  styleUrls: ['./flags-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlagsMenuComponent implements OnInit {

  public settings: Settings;
  constructor(public appSettings: AppSettings, public translate: TranslateService, private store: Store<UserLanguageState>) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {
  }

  changeLang(lang: string) {
    this.translate.use(lang);
    this.store.dispatch({
      type: 'UPDATE_LANG',
      payload: <UserLangPreference>{
        prefLang: lang
      }
    });
  }

}
