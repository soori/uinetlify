import { Component, OnInit } from '@angular/core';
import { MenuService } from '../menu/menu.service';
import { Menu } from '../menu/menu.model';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  toppings: any;
  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

  public menuItems: any;
  public favorites: any;
  constructor(public menuService: MenuService) { }

  ngOnInit() {
    this.menuService.getVerticalMenuItemsFromApi().subscribe(
      data => {
        const response: Menu[] = data;
        this.menuItems = response.filter((menu: Menu) => (menu.router_link && menu.router_link.length > 0));
      }
    );
  }
}
