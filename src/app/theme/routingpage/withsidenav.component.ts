import {
  AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component,
  HostListener, OnInit, QueryList, ViewChild, ViewChildren
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { PerfectScrollbarConfigInterface, PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { AppService } from '../../app.service';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { MenuService } from '../components/menu/menu.service';
import { rotate } from '../utils/app-animation';

@Component({
  selector: 'app-routing-page-withsidenav',
  templateUrl: './withsidenav.component.html',
  styleUrls: ['./withsidenav.component.scss'],
  animations: [rotate],
  providers: [MenuService]
})
export class WithSideNavRoutingComponent implements OnInit, AfterViewInit, AfterViewChecked {
  @ViewChild('sidenav', { static: false }) sidenav: any;
  @ViewChild('backToTop', { static: false }) backToTop: any;
  @ViewChild('options', { static: false }) options: any;
  @ViewChildren(PerfectScrollbarDirective) pss: QueryList<PerfectScrollbarDirective>;
  public optionsPsConfig: PerfectScrollbarConfigInterface = {};
  public settings: Settings;
  public showSidenav: Boolean = false;
  public showInfoContent: Boolean = false;
  public toggleSearchBar: Boolean = false;
  private defaultMenu: string; // declared for return default menu when window resized
  public menus = ['vertical', 'horizontal'];
  public menuOption: string;
  public menuTypes = ['default', 'compact', 'mini'];
  public menuTypeOption: string;

  constructor(public appSettings: AppSettings, public router: Router, private menuService: MenuService,
    private appService: AppService, private cdRef: ChangeDetectorRef) {
    this.settings = this.appSettings.settings;
    this.appService.currentData.subscribe(data => data ? this.settings = data : null);
  }

  ngOnInit() {
    this.optionsPsConfig.wheelPropagation = false;
    if (window.innerWidth <= 960) {
      this.settings.menu = 'vertical';
      this.settings.sidenavIsOpened = false;
      this.settings.sidenavIsPinned = false;
    }
    this.menuOption = this.settings.menu;
    this.menuTypeOption = this.settings.menuType;
    this.defaultMenu = this.settings.menu;
  }

  ngAfterViewInit() {
    setTimeout(() => { this.settings.loadingSpinner = false; }, 300);
    this.backToTop.nativeElement.style.display = 'none';
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.scrollToTop();
      }
      if (window.innerWidth <= 960) {
        this.sidenav.close();
      }
    });
    if (this.settings.menu === 'vertical') {
      this.menuService.getVerticalMenuItemsFromApi().subscribe(data => {
        this.menuService.expandActiveSubMenu(data);
      });
    }
  }
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
  openSettings(value: any) {
    if (value) {
      this.options.toggle();
    }
  }
  public toggleSidenav() {
    this.sidenav.toggle();
  }

  public chooseMenu() {
    this.settings.menu = this.menuOption;
    this.defaultMenu = this.menuOption;
    if (this.menuOption === 'horizontal') {
      this.settings.fixedSidenav = false;
    }
    this.router.navigate(['/']);
  }

  public chooseMenuType() {
    this.settings.menuType = this.menuTypeOption;
  }

  public changeTheme(theme: any) {
    this.settings.theme = theme;
  }

  public closeInfoContent(showInfoContent: any) {
    this.showInfoContent = !showInfoContent;
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    if (window.innerWidth <= 960) {
      this.settings.sidenavIsOpened = false;
      this.settings.sidenavIsPinned = false;
      this.settings.menu = 'vertical';
    } else {
      (this.defaultMenu === 'horizontal') ? this.settings.menu = 'horizontal' : this.settings.menu = 'vertical';
      this.settings.sidenavIsOpened = true;
      this.settings.sidenavIsPinned = true;
    }
  }

  public onPsScrollY(event: any) {
    (event.target.scrollTop > 300) ?
      this.backToTop.nativeElement.style.display = 'flex' : this.backToTop.nativeElement.style.display = 'none';
  }

  public scrollToTop() {
    this.pss.forEach(ps => {
      if (ps.elementRef.nativeElement.id === 'main') {
        ps.scrollToTop(0, 250);
      }
    });
  }

  public closeSubMenus() {
    if (this.settings.menu === 'vertical') {
      this.menuService.closeAllSubMenus();
    }
  }
}
