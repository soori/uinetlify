import { FirestoreSettingsToken } from '@angular/fire/firestore';
import { OverlayContainer } from '@angular/cdk/overlay';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { PerfectScrollbarConfigInterface, PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { AppSettings } from '../app.settings';
import { ErrorComponent } from '../pages/errors/error/error.component';
import { NotFoundComponent } from '../pages/errors/not-found/not-found.component';
import { SharedModule } from '../shared/shared.module';
import { BlankComponent } from './../pages/blank/blank.component';
import { ApplicationsComponent } from './components/applications/applications.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { FlagsMenuComponent } from './components/flags-menu/flags-menu.component';
import { FullScreenComponent } from './components/fullscreen/fullscreen.component';
import { HorizontalMenuComponent } from './components/menu/horizontal-menu/horizontal-menu.component';
import { VerticalMenuComponent } from './components/menu/vertical-menu/vertical-menu.component';
import { MessagesComponent } from './components/messages/messages.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { TopInfoContentComponent } from './components/top-info-content/top-info-content.component';
import { UserMenuComponent } from './components/user-menu/user-menu.component';
import { PipesModule } from './pipes/pipes.module';
import { WithSideNavRoutingComponent } from './routingpage/withsidenav.component';
import { CustomOverlayContainer } from './utils/custom-overlay-container';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  wheelPropagation: true,
  suppressScrollX: true
};


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    SharedModule,
    PipesModule,
    PerfectScrollbarModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      isolate: true
    })
  ],
  exports: [
    WithSideNavRoutingComponent,
    BlankComponent,
    TopInfoContentComponent,
    SidenavComponent,
    VerticalMenuComponent,
    HorizontalMenuComponent,
    FlagsMenuComponent,
    FullScreenComponent,
    ApplicationsComponent,
    MessagesComponent,
    UserMenuComponent,
    FavoritesComponent,
    NotFoundComponent,
    ErrorComponent
  ],
  declarations: [
    WithSideNavRoutingComponent,
    BlankComponent,
    TopInfoContentComponent,
    SidenavComponent,
    VerticalMenuComponent,
    HorizontalMenuComponent,
    FlagsMenuComponent,
    FullScreenComponent,
    ApplicationsComponent,
    MessagesComponent,
    UserMenuComponent,
    FavoritesComponent,
    NotFoundComponent,
    ErrorComponent
  ],
  providers: [
    AppSettings,
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
    { provide: OverlayContainer, useClass: CustomOverlayContainer },
    { provide: FirestoreSettingsToken, useValue: {} }
  ]
})
export class ThemeModule { }
