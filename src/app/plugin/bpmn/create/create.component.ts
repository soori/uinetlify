import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { CustomPaletteProvider } from './../bpmn-js-props-provider/CustomPaletteProvider';
import { CustomPropsProvider } from './../bpmn-js-props-provider/CustomPropsProvider';
import { InjectionNames, Modeler, OriginalPaletteProvider, OriginalPropertiesProvider, PropertiesPanelModule } from './../bpmn-js/bpmn';
import { ProcessBuilderService } from './../bpmn.service';

const customModdle = {
  name: 'customModdle',
  uri: 'http://example.com/custom-moddle',
  prefix: 'custom',
  xml: {
    tagAlias: 'lowerCase'
  },
  associations: [],
  types: [
    {
      'name': 'ExtUserTask',
      'extends': [
        'bpmn:UserTask'
      ],
      'properties': [
        {
          'name': 'worklist',
          'isAttr': true,
          'type': 'String'
        }
      ]
    },
  ]
};

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {



  title = 'Angular/BPMN';
  jsonXML: any = {};
  modeler: any;
  isLoading: Boolean = false;

  constructor(private service: ProcessBuilderService, private _messageService: MessageService, private http: HttpClient,
    protected router: Router, protected route: ActivatedRoute, protected translate: TranslateService,
    protected store: Store<UserLanguageState>, private _service: ProcessBuilderService) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  ngOnInit(): void {
    this.modeler = new Modeler({
      container: '#canvas',
      width: '100%',
      height: '600px',
      additionalModules: [
        PropertiesPanelModule,
        // Re-use original bpmn-properties-module, see CustomPropsProvider
        { [InjectionNames.bpmnPropertiesProvider]: ['type', OriginalPropertiesProvider.propertiesProvider[1]] },
        { [InjectionNames.propertiesProvider]: ['type', CustomPropsProvider] },
        // Re-use original palette, see CustomPaletteProvider
        { [InjectionNames.originalPaletteProvider]: ['type', OriginalPaletteProvider] },
        { [InjectionNames.paletteProvider]: ['type', CustomPaletteProvider] },
        { httpClient: ['value', this.http] }
      ],
      propertiesPanel: {
        parent: '#properties'
      },
      moddleExtension: {
        custom: customModdle
      }
    });
    this.load();
  }

  handleError(err: any) {
    if (err) {
      console.warn('Ups, error: ', err);
    }
  }

  load(): void {
    const url = '/assets/bpmn/initial.bpmn';
    this.http.get(url, {
      headers: { observe: 'response' }, responseType: 'text'
    }).subscribe(
      (x: any) => {
        this.modeler.importXML(x, this.handleError);
      },
      this.handleError
    );
  }

  save(): void {
    this.modeler.saveXML((err: any, xml: any) => {
      console.log('Result of saving XML: ', err, xml);
      this.service.saveProcess({ content: xml }).subscribe(data => {
        this.isLoading = false;
        this._messageService.info('Process saved successfully');
        this.router.navigate(['./../home'], { relativeTo: this.route });
      },
        error => {
          this._messageService.info('Error occured, Try again !');
          this.isLoading = false;
        });
    });
  }

}

