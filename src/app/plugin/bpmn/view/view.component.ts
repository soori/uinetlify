import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { Viewer } from './../bpmn-js/bpmn';
import { ProcessBuilderService } from './../bpmn.service';


@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  title = 'Angular/BPMN';
  jsonXML: any = {};
  viewer: any;
  isLoading: Boolean = false;
  processId: any;
  svg: any;

  constructor(private service: ProcessBuilderService, private _messageService: MessageService,
    private http: HttpClient, protected router: Router, protected route: ActivatedRoute,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  ngOnInit(): void {
    this.viewer = new Viewer({
      container: '#canvas',
      width: '100%',
      height: '600px'
    });
    this.processId = this.route.snapshot.paramMap.get('id');
    this.load();
  }

  handleError(err: any) {
    if (err) {
      console.warn('Ups, error: ', err);
    }
  }

  load() {
    const processData: any = JSON.parse(localStorage.getItem('selectedProcess') || 'null');
    if (processData) {
      this.viewer.importXML(processData.content, this.handleError);
    }
  }

  modify() {
    this.router.navigate(['./../../edit/' + this.processId], { relativeTo: this.route });
  }


}
