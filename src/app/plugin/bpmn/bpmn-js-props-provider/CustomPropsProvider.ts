import { HttpClient } from '@angular/common/http';
import { EntryFactory, IPropertiesProvider } from '../bpmn-js/bpmn';
export class CustomPropsProvider implements IPropertiesProvider {

  static $inject = ['translate', 'bpmnPropertiesProvider', 'eventBus', 'httpClient'];
  selectedTask: any = {};
  panelProps: any = {
    'id': 'Advanced',
    'label': 'Advanced',
    'groups': [{
      'id': 'Advanced',
      'label': 'Advanced',
      'entries': [{
        'type': 'selectBox',
        'id': 'externalTopic',
        'label': 'Topic',
        'selectOptions': [
          {
            'name': 'Runscript',
            'value': 'Runscript'
          },
          {
            'name': 'Update',
            'value': 'Update'
          },
          {
            'name': 'Servicetask',
            'value': 'Servicetask'
          }
        ],
        'modelProperty': 'externalTopic'
      },
      {
        'type': 'checkBox',
        'id': 'dueDate',
        'description': 'Click to select',
        'label': 'DueDate',
        'modelProperty': 'dueDate'
      },
      {
        'type': 'textBox',
        'id': 'condition',
        'label': 'conditionText',
        'modelProperty': 'conditionText'
      },
      {
        'type': 'textField',
        'id': 'condition1',
        'label': 'conditionText1',
        'modelProperty': 'conditionText1',
        'canBeHidden': false
      },
      {
        'type': 'label',
        'id': 'Label',
        'labelText': 'Label text',
        'modelProperty': 'labelText'
      }
      ]
    }]
  };
  // Note that names of arguments must match injected modules, see InjectionNames.
  constructor(private translate: any, private bpmnPropertiesProvider: any, private eventBus: any, private httpClient: HttpClient) {
    this.testingService();
  }

  testingService(): void {
    const url = '/assets/bpmn/initial.bpmn';
    this.httpClient.get(url, {
      headers: { observe: 'response' }, responseType: 'text'
    }).subscribe(
      (x: any) => {
        console.log(" from  CustomPropsProvider");
      },
    );
  }

  getTabs(element: any) {
    console.log(this.eventBus);
    this.eventBus.on('element.click', function (event) {
      console.log(event.element, ' was clicked');
    });
    const selectedTask = JSON.parse(localStorage.getItem('selectedTask') || 'null');
    console.log(selectedTask);
    return this.bpmnPropertiesProvider.getTabs(element)
      .concat(this.getPanelProps());
  }

  getPanelProps() {
    const panelProp: any = {
      id: this.panelProps.id,
      label: this.translate(this.panelProps.label),
      groups: []
    };
    this.panelProps.groups.forEach(element => {
      panelProp.groups.push({ id: element.id, label: this.translate(element.label), entries: this.getEntries() });
    });
    return panelProp;
  }

  getEntries() {
    const entries: any = [];
    this.panelProps.groups[0].entries.forEach(element => {
      if (element.type === 'selectBox') {
        entries.push(EntryFactory.selectBox({
          id: element.id,
          label: this.translate(element.label),
          selectOptions: element.selectOptions,
          modelProperty: element.modelProperty,
        }));
      } else if (element.type === 'textBox') {
        entries.push(EntryFactory.textBox({
          id: element.id,
          label: this.translate(element.label),
          modelProperty: element.modelProperty
        }));
      } else if (element.type === 'checkBox') {
        entries.push(EntryFactory.checkbox({
          id: element.id,
          description: element.description,
          label: this.translate(element.label),
          modelProperty: element.modelProperty
        }));
      } else if (element.type === 'textField') {
        entries.push(EntryFactory.textField({
          id: element.id,
          label: this.translate(element.label),
          modelProperty: element.modelProperty
        }));
      } else if (element.type === 'label') {
        entries.push(EntryFactory.label({
          id: element.id,
          labelText: this.translate(element.labelText),
          modelProperty: element.modelProperty
        }));
      }
    });
    return entries;
  }
}
