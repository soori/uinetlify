import { AppRouteComponent } from './../../admin/route/route.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [

  {
    path: 'home', component: HomeComponent, pathMatch: 'full',
    data: { breadcrumb: 'Home' }
  },
  {
    path: 'new', component: CreateComponent, pathMatch: 'full',
    data: { breadcrumb: 'Create' }
  },
  {
    path: 'edit/:id', component: EditComponent, pathMatch: 'full',
    data: { breadcrumb: 'Modify' }
  },
  {
    path: 'view/:id', component: ViewComponent, pathMatch: 'full',
    data: { breadcrumb: 'View' }
  },

  { path: '**', component: AppRouteComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BpmnRoutingModule { }
