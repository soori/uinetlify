import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class ProcessBuilderService {

    private bpmnTask = new BehaviorSubject({});
    currentTask = this.bpmnTask.asObservable();

    constructor(private http: HttpClient) { }


    saveProcess(page: any) {
        return this.http.post(`${environment.apiEndpoint}/processbuilder`, page);
    }
    getAllProcesses() {
        return this.http.get(`${environment.apiEndpoint}/processbuilder`);
    }
    getProcess(id: string) {
        return this.http.get(`${environment.apiEndpoint}/processbuilder/${id}`);
    }
    updateProcess(page: any, id: string | null) {
        return this.http.put(`${environment.apiEndpoint}/processbuilder/${id}`, page);
    }
    getCreateTemplate() {
        return this.http.get('./assets/bpmn/initial.bpmn');
    }
    changedTask(data: any) {
        this.bpmnTask.next(data);
    }
}
