import { map } from 'rxjs/operators';
import { ProcessBuilderService } from './../bpmn.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  showDefaultFilter: Boolean = true;
  dataSource: MatTableDataSource<any>;
  displayedColumns: any = ['name', 'description', 'isExecutable'];
  headers = [{ columnName: 'name', display_name: 'Name' },
  { columnName: 'description', display_name: 'Description' },
  { columnName: 'isExecutable', display_name: 'is Executable' }

  ];
  configItems: any;
  noData: any;
  isLoading: Boolean = false;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  processList: any = {};

  constructor(private service: ProcessBuilderService, protected router: Router, protected route: ActivatedRoute,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit(): void {
    this.processList = {};
    this.getProcessList();
  }


  applyFilter(event: any) {
    let filterValue = event.target.value;
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  selectedRowItem(selectedRow: any) {
    localStorage.setItem('selectedProcess', JSON.stringify(selectedRow));
    this.router.navigate(['./../view/' + selectedRow.name], { relativeTo: this.route });
  }

  getSelectedItems() {
    this.isLoading = true;
    this.service.getAllProcesses().subscribe(data => {
      this.configItems = data;
      this.isLoading = false;
      console.log(data);
      for (const row of this.configItems) {
        row.applicationName = row.applicationType.display_name;
        row.userName = row.userType.display_name;
        row.tenantName = row.tenant.display_name;
      }
      this.dataSource = new MatTableDataSource(this.configItems);
      this.noData = this.dataSource.connect().pipe(map(dataa => dataa.length === 0));
      if (this.configItems.length > 0) {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    },
      err => {
        this.isLoading = false;
      });
  }
  getProcessList() {
    this.service.getAllProcesses().subscribe(data => {
      this.processList = data;
      console.log(data);
      for (const process of this.processList) {
        process.name = process._id;
        if (process && process.content) {
          const content = process.content;
          const pcDef = content['BPMN:DEFINITIONS'];
          if (pcDef) {
            const processData: any[] = pcDef['BPMN:PROCESS'];
            if (processData && processData.length > 0) {
              const prcInfo = processData[0];
              if (prcInfo) {
                const prDetails = prcInfo['$'];
                process.description = prDetails.ID;
                process.isExecutable = prDetails.ISEXECUTABLE;
              }
            }
          }
        }
      }

      this.dataSource = new MatTableDataSource(this.processList);
      this.noData = this.dataSource.connect().pipe(map(dataa => dataa.length === 0));
      if (this.processList.length > 0) {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  create() {
    this.router.navigate(['./../new'], { relativeTo: this.route });
  }


}
