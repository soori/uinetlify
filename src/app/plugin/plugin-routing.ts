import { WithSideNavRoutingComponent } from './../theme/routingpage/withsidenav.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRouteComponent } from '../admin/route/route.component';

const routes: Routes =

    [{
        path: '', component: WithSideNavRoutingComponent,
        children: [
            { path: 'bpmn', loadChildren: './bpmn/bpmn.module#BpmnModule' },
            { path: '**', component: AppRouteComponent }
        ]
    }
    ]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PluginRoutingModule { }
