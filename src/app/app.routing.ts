import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRouteComponent } from './admin/route/route.component';
import { NotFoundComponent } from './pages/errors/not-found/not-found.component';


export const routes: Routes = [
    { path: '', redirectTo: 'authenticate', pathMatch: 'full' },
    { path: 'authenticate', loadChildren: './authentication/firebaseui/login.module#LoginModule' },
    { path: 'plugin', loadChildren: './plugin/plugin.module#PluginModule' },
    { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
    { path: 'portal', loadChildren: './pages/pages.module#UserPagesModule' },
    { path: 'custom', component: AppRouteComponent },
    { path: '**', component: AppRouteComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        //preloadingStrategy: PreloadAllModules,  // <- comment this line for activate lazy load
        useHash: false
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
