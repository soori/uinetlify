import { Component, OnInit, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FirebaseUISignInFailure, FirebaseUISignInSuccessWithAuthResult } from 'firebaseui-angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Settings } from '../../app.settings.model';
import { AppSettings } from '../../app.settings';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public form: FormGroup;
  public settings: Settings;
  constructor(public appSettings: AppSettings, public afs: AngularFirestore,
    public router: Router, public afAuth: AngularFireAuth, private ngZone: NgZone) {
    this.settings = this.appSettings.settings;
  }
  ngOnInit(): void {
    this.afAuth.authState.subscribe(
      data => {
        if (data) {
          if (data.phoneNumber) {
            const firebaseUser = {
              uid: data.uid,
              name: data.phoneNumber,
              // tslint:disable-next-line: max-line-length
              imageUrl: 'https://lh4.googleusercontent.com/-Im2HBZSawsU/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3reLTvWEja0ifnTeleua2Q2rk2_-hg/photo.jpg',
              email: 'help@sudheeksha.co',
            };
            localStorage.setItem('LoggedInUser', JSON.stringify(firebaseUser));
          }
          this.ngZone.run(() => {
            this.router.navigate(['./admin']);
          });
          this.router.navigate(['./admin']);
        } else {
          this.settings.loadingSpinner = false;
        }
      });
  }
  //https://www.youtube.com/watch?v=TRe-2NK4Mhg
  // TODO:: following call back was not called when phone authentication is used
  public successCallback(data: FirebaseUISignInSuccessWithAuthResult) {
    const user = data.authResult.user;
    if (user) {
      const firebaseUser = {
        uid: user.uid,
        name: user.displayName,
        imageUrl: user.photoURL,
        email: user.email,
      };
      this.afs.collection('gLoginUsers').doc(firebaseUser.uid).set(firebaseUser, { merge: true });
      localStorage.setItem('LoggedInUser', JSON.stringify(firebaseUser));
    }
  }
  public errorCallback(data: FirebaseUISignInFailure) {
  }
}
