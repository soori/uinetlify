import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhoneComponent } from './phone.component';
import { SharedModule } from 'src/app/shared/shared.module';

export const routes = [
  { path: '', component: PhoneComponent, pathMatch: 'full' }
];

@NgModule({
  declarations: [PhoneComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule

  ]
})
export class PhoneModule { }
