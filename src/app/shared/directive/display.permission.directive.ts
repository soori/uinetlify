import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Directive({
    selector: '[appDisplayPermission]'
})
export class DisplayPermissionsDirective implements OnInit {
    @Input() appDisplayPermission: string;
    getState: Observable<any>;

    constructor(private el: ElementRef) { }

    ngOnInit() {
        const permissions = JSON.parse(localStorage.getItem('userPermissions') || 'null');
        if (permissions) {
            if (!permissions.includes(this.appDisplayPermission)) {
                this.el.nativeElement.style.display = 'none';
            }
        }
    }
}
