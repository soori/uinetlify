import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root',
})
export class MessageService {
    verticalPos: MatSnackBarVerticalPosition = 'bottom';
    horizonatalPos: MatSnackBarHorizontalPosition = 'start';

    constructor(private snackBar: MatSnackBar) {
    }

    info(message: string, action: string = 'Close') {
        this.snackBar.open(message, action, {
            panelClass: ['md-snack-info'],
            duration: 2000,
            verticalPosition: this.verticalPos,
            horizontalPosition: this.horizonatalPos,
        });
    }

    warn(message: string, action: string = 'Close') {
        this.snackBar.open(message, action, {
            panelClass: ['md-snack-warn'],
            verticalPosition: this.verticalPos,
            horizontalPosition: this.horizonatalPos,
        });
    }

    message(message: string, action: string = 'Close') {
        this.snackBar.open(message, action, {
            duration: 2000,
            verticalPosition: this.verticalPos,
            horizontalPosition: this.horizonatalPos,
        });
    }
}
