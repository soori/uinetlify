import { AgmCoreModule } from '@agm/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguCarouselModule } from '@ngu/carousel';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { SharedModule } from '../shared/shared.module';
import { ThemeModule } from '../theme/theme.module';
import { AdminRoutingModule } from './admin-routing.module';



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ThemeModule,
    SharedModule,
    AgmCoreModule,
    ScrollToModule.forRoot(),
    NguCarouselModule,
    AdminRoutingModule,
    DragDropModule,
    FormsModule,
  ],
  declarations: [
  ],
  providers: [
  ],
  entryComponents: []
})
export class AdminModule { }
