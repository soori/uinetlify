import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private location: Location, private router: Router, private http: HttpClient) { }

  getMenuData() {
    return this.http.get(`${environment.apiEndpoint}/menu`);
  }

  getPermissions(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiEndpoint}/config/global/preauth/landingpage/1/1/usertypeCodes`);
  }

  saveTemplate(template: any) {
    return this.http.post(`${environment.apiEndpoint}/formtemplate`, template);
  }

  updateTemplate(template: any, id: any) {
    return this.http.put(`${environment.apiEndpoint}/formbuilder/${id}`, template);
  }
  deleteTemplate(id: any) {
    return this.http.delete(`${environment.apiEndpoint}/formbuilder/${id}`);
  }
  getSavedTemplate() {
    return this.http.get(`${environment.apiEndpoint}/formbuilder`);
  }


}
