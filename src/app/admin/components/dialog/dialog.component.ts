import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { DialogService } from './dialog.service';
import { DialogViewComponent } from './view/view.component';


@Component({
  selector: 'app-template-dialog-cmp',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {

  @Input() config: any;
  isLoading: Boolean = false;
  pageAttr: any;
  dialogFormGroup: FormArray;
  dialogTodoItems: any[] = [];
  dialogDoneItems: any[] = [];
  showView: Boolean = false;
  templateId: string | null;

  constructor(private service: DialogService, private fb: FormBuilder, public dialog: MatDialog,
    protected router: Router, protected route: ActivatedRoute, protected translate: TranslateService,
    private _messageService: MessageService, protected store: Store<UserLanguageState>, private location: Location) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    const template = JSON.parse(localStorage.getItem('template') || 'null');
    if (template) {
      this.dialogDoneItems = template.attributes;
    }
    this.templateId = this.route.snapshot.paramMap.get('id');
    this.dialogFormGroup = this.fb.array([]);
    this.getPageItems();
  }
  getPageItems() {
    this.isLoading = true;
    this.service.getSavedTemplate().subscribe(data => {
      this.isLoading = false;
      this.pageAttr = data;
      this.pageAttr.forEach((element: any) => {
        if (element.details.type.id === 1 || element.details.type.id === 5) {
          element.hasMultipleForms = true;
          const form = this.initFormGroup(element._id);
          this.dialogTodoItems.push(element);
          // TO-DO Uncomment below line and fix error
          // this.dialogFormGroup.push(form);
        }
      });
    });
  }
  initFormGroup(name: any): FormGroup {
    name = this.fb.group({
    });
    return name;
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      const doneCopy = this.createCopy(this.dialogDoneItems[event.currentIndex]);
      this.dialogDoneItems[event.currentIndex] = doneCopy;
      this.dialogDoneItems.forEach((item, i) => {
        item.position = i;
      });
    } else if (event.previousIndex !== event.currentIndex) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
    }
  }

  createCopy(orig: any) {
    return JSON.parse(JSON.stringify(orig));
  }

  resetDoneItems() {
    this.dialogDoneItems = [];
  }

  onViewClick() {
    const dialogCardRef = this.dialog.open(DialogViewComponent, {
      width: '100vw',
      height: '90vh',
      data: this.dialogDoneItems,
      disableClose: true
    });
  }
  submit() {
    this.isLoading = true;
    const dialogLayout = {
      details: this.config,
      attributes: this.dialogDoneItems
    };
    this.service.saveTemplate(dialogLayout).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Dialog saved successfully');
      this.router.navigate(['./../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, Try again');
        this.isLoading = false;
      });
  }

  update() {
    this.isLoading = true;
    const dialogLayout = {
      details: this.config,
      attributes: this.dialogDoneItems
    };
    this.service.updateTemplate(dialogLayout, this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Dialog updated successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, Try again');
        this.isLoading = false;
      });
  }
  goBack() {
    this.location.back();
  }
  removeItem(i: number) {
    this.dialogDoneItems.splice(i, 1);
  }
  getMultipleFormValue(event: any) {
  }
  getFormValue(event: any) {
  }
  getFormValid(event: any) {
  }
  openEditDialog(attr: any, index: number) {
  }
  delete() {
    this.isLoading = true;
    this.service.deleteTemplate(this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Template deleted successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this.isLoading = false;
        this._messageService.warn('Error occures, Try again');
      });
  }
  
}
