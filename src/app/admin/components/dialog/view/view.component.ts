import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-template-dialog-view-cmp',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class DialogViewComponent implements OnInit {
  dialogItems: any;
  dialogFormGroup: FormArray;
  constructor(public dialogRef: MatDialogRef<DialogViewComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder, protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.dialogFormGroup = this.fb.array([]);
    this.dialogItems = this.data;
    this.dialogItems.forEach((element: any) => {
      element.hasMultipleForms = true;
      const form = this.initFormGroup(element._id);
      this.dialogFormGroup.push(form);
    });
  }
  initFormGroup(name: any): FormGroup {
    name = this.fb.group({
    });
    return name;
  }
  onNoClick() {
    this.dialogRef.close();
  }
  getMultipleFormValue(event: any) {
  }
  getFormValue(event: any) {
  }
  getFormValid(event: any) {
  }
}
