import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { ButtonModule } from '../button/button.module';
import { FormFieldsModule } from '../form/form.module';
import { DialogComponent } from './dialog.component';
import { DialogViewComponent } from './view/view.component';

@NgModule({
  declarations: [DialogComponent, DialogViewComponent],
  imports: [
    CommonModule,
    DragDropModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    FormFieldsModule,
    ButtonModule
  ],
  exports: [DialogComponent, DialogViewComponent],
  entryComponents: [DialogViewComponent]
})
export class DialogModule { }
