import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { ImageRoutingModule } from './banner-routing.module';
import { DialogImageEditComponent } from './config/dialog-image-edit.component';
import { ViewConfigurationDialogComponent } from './config/view/view-configuration.component';
import { CreateImageComponent } from './create/create-image.component';
import { ViewImageComponent } from './view/view-image.component';

@NgModule({
  declarations: [
    CreateImageComponent, ViewImageComponent, DialogImageEditComponent, ViewConfigurationDialogComponent],
  imports: [
    CommonModule,
    ImageRoutingModule, DragDropModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [CreateImageComponent, ViewImageComponent],
  entryComponents: [DialogImageEditComponent]
})
export class BannerModule { }
