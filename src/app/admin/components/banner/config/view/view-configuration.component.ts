import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-view-configuration-dialog',
  templateUrl: './view-configuration.component.html',
  styleUrls: ['./view-configuration.component.scss']
})
export class ViewConfigurationDialogComponent implements OnInit {

  selectedTemplate: any = {};

  formFieldStyles: any[] = [
    { id: 0, name: 'legacy', display_name: 'Legacy' },
    { id: 1, name: 'outline', display_name: 'Outline' },
    { id: 2, name: 'standard', display_name: 'Standard' },
    { id: 3, name: 'fill', display_name: 'Fill' }
  ];

  directions: any[] = [
    { id: 1, name: 'row', display_name: 'Row' },
    { id: 2, name: 'column', display_name: 'Column' }
  ];

  constructor(public dialogRef: MatDialogRef<ViewConfigurationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.selectedTemplate = this.data;
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
  saveFormField() {
    this.dialogRef.close(this.selectedTemplate);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
