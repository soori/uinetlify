import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-dialog-image-edit',
  templateUrl: './dialog-image-edit.component.html',
  styleUrls: ['./dialog-image-edit.component.scss']
})
export class DialogImageEditComponent implements OnInit {

  item: any = {};
  selectedType: any = {};
  selectedImageType: any = {};
  imageView: any[] = [
    { id: 0, value: 50, View: 'Circle' },
    { id: 1, value: 0, View: 'Normal' }
  ];

  imageType: any[] = [
    { id: 0, name: 'URL', dispalName: 'URL', value: '' },
    { id: 1, name: 'IMAGE', dispalName: 'Avatar', value: '' },
    { id: 2, name: 'BROWSER', dispalName: 'Local', value: '' },
  ];

  selected = -1;
  public imagePath: any;
  public message: string;
  avtharImages: any[] = [
    { id: 0, name: 'male', value: './assets/img/users/user.jpg' },
    { id: 1, name: 'female', value: './assets/img/profile/ashley.jpg' },
  ];
  selectType: any = {};
  imgType: any;
  fileData: any;
  previewUrl: any = null;

  constructor(public dialogRef: MatDialogRef<DialogImageEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.item = this.data.value;
    this.selectedType = this.item;
    this.selectedImageType = this.item.source;
  }
  saveFormField() {
    this.dialogRef.close(this.item);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
  onChange(event: any, type: any, i: any) {
    this.selected = i;
    this.imgType.value = type.value;
    this.item.source = this.imgType;
    return this.selected;
  }

  imageChange(type: any) {
    this.imgType = type.value;
    this.item.source = this.imgType;
  }

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

  preview() {
    const mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
      this.item.source.value = reader.result;
    };
  }

}
