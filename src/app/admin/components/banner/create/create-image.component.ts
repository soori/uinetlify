import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { DialogImageEditComponent } from '../config/dialog-image-edit.component';
import { ViewConfigurationDialogComponent } from '../config/view/view-configuration.component';
import { BannerService } from './../banner.service';

@Component({
  selector: 'app-template-banner-create-cmp',
  templateUrl: './create-image.component.html',
  styleUrls: ['./create-image.component.scss']
})
export class CreateImageComponent implements OnInit {

  @Input() config: any = {};

  isLoading: Boolean = false;

  todoimages = [
    {
      id: 1,
      title: 'Image',
      icon: 'photo',
      source: {
        id: 0, name: 'url', dispalName: 'URL',
        value: 'https://images.pexels.com/photos/256559/pexels-photo-256559.jpeg?auto=compress&cs=tinysrgb&h=350',
      },
      imgHeight: 100,
      imgWidth: 100,
      imageRadius: 50,
      flexProperties: {
        alignment: 'start center',
        flex: '20%',
        layout: { id: 0, name: 'row' }
      }
    },
    {
      id: 2,
      title: 'text',
      icon: 'sort_by_alpha',
      name: 'name',
      key: 'text',
      label: 'firstname',
    },

  ];

  doneimages: any = [];
  donetext: any = [];

  templateConfig: any = {
    type: { id: 12, name: 'Banner' },
    name: '',
    layout: { id: 1, name: 'row', display_name: 'Row' },
    layoutColumn: { id: 0, flex: '20', flexGap: '5px' },
    fieldStyle: { id: 1, name: 'outline', display_name: 'Outline' },
  };

  showCreate: Boolean = true;
  templateId: string | null;
  @Input() cmpLayoutItems: any = [];
  @Input() layoutConfig: any = [];

  constructor(public dialog: MatDialog, private router: Router, private route: ActivatedRoute,
    private service: BannerService, private _messageService: MessageService, private location: Location,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.templateId = this.route.snapshot.paramMap.get('id');
    if (this.cmpLayoutItems?.textAttributes?.length > 0 && this.cmpLayoutItems?.textAttributes?.length > 0) {
      this.donetext = this.cmpLayoutItems.textAttributes;
      this.doneimages = this.cmpLayoutItems.imageAttributes;
      this.templateConfig = this.config;
    }
  }


  openEditDialog(item: any, i: number) {
    const dialogRef = this.dialog.open(DialogImageEditComponent, {
      width: '100vw',
      data: { value: JSON.parse(JSON.stringify(item)) },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        item = result;
        if (item.id === 1) {
          this.doneimages[i] = item;
        } else if (item.id === 2) {
          this.donetext[i] = item;
        }
      }
    });
  }
  openEditFlex(selectedTemplate: any, i: number) {
    selectedTemplate.config = i;
    const dialogRef = this.dialog.open(ViewConfigurationDialogComponent, {
      width: '100vw',
      data: selectedTemplate,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result = this.templateConfig;
      }
    });
  }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    } else {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
  }
  removeImg(i: number) {
    this.doneimages.splice(i, 1);
  }
  removeText(i: number) {
    this.donetext.splice(i, 1);
  }
  onViewClick() {
    this.showCreate = false;
    localStorage.removeItem('template');
    const imageTextData = {
      attributes: {
        imageAttributes: this.doneimages,
        textAttributes: this.donetext,
      },
      layout: {
        name: this.templateConfig.layout.name,
        flex: this.templateConfig.layoutColumn.flex,
      },
      details: this.templateConfig,
      // isTemplateView: false;

    };
    localStorage.setItem('template', JSON.stringify(imageTextData));
  }
  submitimage() {
    this.isLoading = true;
    this.templateConfig.name = this.config.name;
    const imageTextData = {
      attributes: {
        imageAttributes: this.doneimages,
        textAttributes: this.donetext,
      },
      layout: {
        name: this.templateConfig.layout.name,
        flex: this.templateConfig.layoutColumn.flex,
      },
      details: this.templateConfig,

    };
    this.service.saveTemplate(imageTextData).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('image & text feild saved successfully');
      this.router.navigate(['./../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.info('Error occured, Try again !');
        this.isLoading = false;
      });
  }
  update() {
    this.isLoading = true;
    this.templateConfig.name = this.config.name;
    const imageTextData = {
      attributes: {
        imageAttributes: this.doneimages,
        textAttributes: this.donetext,
      },
      layout: {
        name: this.templateConfig.layout.name,
        flex: this.templateConfig.layoutColumn.flex,
      },
      details: this.templateConfig,
    };
    this.service.updateTemplate(imageTextData, this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('image & text feild updated successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        // alert('error');
        this._messageService.warn('Error occured, Try again !');
        this.isLoading = false;
      });
  }

  delete() {
    this.isLoading = true;
    this.service.deleteTemplate(this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Template deleted successfully');
      this.location.back();
    },
      err => {
        this.isLoading = false;
        this._messageService.warn('Error occures, Try again');
      });
  }
  goBack() {
    this.location.back();
  }
  updateView(event: any) {
    this.showCreate = true;
  }
}
