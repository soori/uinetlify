import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewImageComponent } from './view/view-image.component';


const routes: Routes = [
  { path: 'view', component: ViewImageComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImageRoutingModule { }
