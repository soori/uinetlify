import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-template-banner-view-cmp',
  templateUrl: './view-image.component.html',
  styleUrls: ['./view-image.component.scss']
})
export class ViewImageComponent implements OnInit {

  imagedata: any;
  @Input() banner: any;
  @Output() viewMode = new EventEmitter<boolean>();
  @Output() emitClick = new EventEmitter<any>();


  constructor(private route: ActivatedRoute, protected translate: TranslateService,
    protected store: Store<UserLanguageState>, private location: Location) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  ngOnInit() {
    this.imagedata = this.banner ? this.banner : JSON.parse(localStorage.getItem('template') || 'null');
  }
  back() {
    this.location.back();
  }
  updateMode() {
    this.viewMode.emit(false);
  }
  submit(item: any) {
    this.emitClick.emit(item);
  }
  navigation(item: any) {
    this.emitClick.emit(item);
  }
  reset(item: any) {
    this.emitClick.emit(item);
  }
}
