import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-chart-create-cmp',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})

export class ChartCreateComponent implements OnInit {
  inputTypes: any = [
    { id: 1, name: 'Form', icon: 'assignment' },
    { id: 2, name: 'Card', icon: 'web_asset' },
    { id: 3, name: 'Tab', icon: 'tab' },
    { id: 4, name: 'Grid', icon: 'view_module' },
    { id: 5, name: 'Button', icon: 'keyboard' },
    { id: 7, name: 'Stepper', icon: 'format_list_numbered' },
    { id: 8, name: 'Bottom sheet', icon: 'call_to_action' },
    { id: 9, name: 'Dialog', icon: 'crop_16_9' },
    { id: 10, name: 'Tree', icon: 'account_tree' },
    { id: 11, name: 'Charts', icon: 'show_chart' },
    { id: 12, name: 'Banner', icon: 'photo' }

  ];

  basicTypes: any = [
    { id: 1, name: 'Pie', icon: 'pie_chart' },
    { id: 2, name: 'bar', icon: 'bar_chart' },
    { id: 3, name: 'Scatter Plot', icon: 'scatter_plot' },
    { id: 4, name: 'Map', icon: 'map' },
    { id: 5, name: 'Line', icon: 'timeline' },
    { id: 6, name: 'Area', icon: 'insert_photo' },
    { id: 7, name: 'Tree', icon: 'account_tree' },
    { id: 8, name: 'heat', icon: 'grain' },
    { id: 9, name: 'Guage', icon: 'speed' },
    { id: 10, name: 'Network', icon: 'ac_unit' },
    { id: 11, name: 'Bubble', icon: 'bubble_chart' },

  ];

  libraryTypes: any = [
    { id: 1, name: 'HighCharts', image: 'assets/img/charts/highcharts.png' },
    { id: 2, name: 'Matplotlib', image: 'assets/img/charts/matplotlib.png' }
  ];
  isClicked: Boolean = false;
  enableNext: Boolean = false;
  preventSingleClick: Boolean = false;
  timer: any;
  delay: Number;
  constructor(protected router: Router, protected route: ActivatedRoute) { }

  ngOnInit() {
  }

  onCardClick(card: any) {
    alert(card);
    this.enableNext = true;
    this.basicTypes.forEach((item: any) => {
      item.isClicked = false;
    });
    this.libraryTypes.forEach((item: any) => {
      item.isClicked = false;
    });
    card.isClicked = !card.isClicked;
    localStorage.setItem('selectedTemplate', JSON.stringify(card));
  }
  onCardDblClick(card: any) {
    alert(card);
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    this.basicTypes.forEach((item: any) => {
      item.isClicked = false;
    });
    this.libraryTypes.forEach((item: any) => {
      item.isClicked = false;
    });
    card.isClicked = !card.isClicked;
    this.router.navigate(['/admin/builder/template/new']);
    localStorage.setItem('selectedTemplate', JSON.stringify(card));
  }
  onNext() {
    this.timer = 0;
    this.preventSingleClick = false;
    const delay = 200;
    this.timer = setTimeout(() => {
      if (!this.preventSingleClick) {
        this.router.navigate(['/admin/builder/template/new']);
      }
    }, delay);
  }
}
