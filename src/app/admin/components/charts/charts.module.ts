import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { ButtonModule } from '../button/button.module';
import { ChartCreateComponent } from './create/create.component';


@NgModule({
  declarations: [ChartCreateComponent],
  imports: [
    CommonModule,
    DragDropModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    ButtonModule
  ],
  exports: [
    ChartCreateComponent
  ],
  entryComponents: []
})
export class ChartModule { }
