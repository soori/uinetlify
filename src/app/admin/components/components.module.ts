import { ChartModule } from './charts/charts.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { BannerModule } from './banner/banner.module';
import { BottomSheetModule } from './bottom-sheet/bottom-sheet.module';
import { ButtonModule } from './button/button.module';
import { CardModule } from './card/card.module';
import { DialogModule } from './dialog/dialog.module';
import { FormFieldsModule } from './form/form.module';
import { GridModule } from './grid/grid.module';
import { StepperModule } from './stepper/stepper.module';
import { TabModule } from './tab/tab.module';
import { TreeModule } from './tree/tree.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    CardModule,
    ButtonModule,
    StepperModule,
    DragDropModule,
    FormFieldsModule,
    TabModule,
    BannerModule,
    BottomSheetModule,
    DialogModule,
    GridModule,
    TreeModule,
    ChartModule,
    AngularEditorModule
  ],
  exports: [
    FormsModule,
    CardModule,
    ButtonModule,
    StepperModule,
    DragDropModule,
    FormFieldsModule,
    TabModule,
    BannerModule,
    BottomSheetModule,
    DialogModule,
    GridModule,
    TreeModule,
    ChartModule
  ],
  entryComponents: []
})
export class AdminComponentsModule { }
