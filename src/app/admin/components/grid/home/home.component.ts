import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-template-grid-home-cmp',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class GridHomeComponent implements OnInit {

  @Input() config: any;

  constructor() { }

  ngOnInit() {
  }

}
