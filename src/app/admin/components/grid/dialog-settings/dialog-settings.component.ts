import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-settings',
  templateUrl: './dialog-settings.component.html',
  styleUrls: ['./dialog-settings.component.scss']
})
export class DialogSettingsComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DialogSettingsComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }
  grid: any = {
    dataEndpoint: '',
    columnEndpoint: '',
    hasRowSelection: false,
    hasPagination: true
  };
  ngOnInit() {
    this.grid = this.data.grid ? this.data.grid : this.grid;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  onOkClick() {
    this.dialogRef.close(this.grid);
  }
}
