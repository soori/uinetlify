import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { GridCreateComponent } from './create/create.component';
import { DialogSettingsComponent } from './dialog-settings/dialog-settings.component';
import { GridRoutingModule } from './grid-routing.module';
import { GridHomeComponent } from './home/home.component';
import { GridViewComponent } from './view/view.component';


@NgModule({
  declarations: [GridCreateComponent, GridHomeComponent, GridViewComponent, DialogSettingsComponent],
  imports: [
    CommonModule,
    GridRoutingModule,
    DragDropModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [GridHomeComponent, GridCreateComponent],
  entryComponents: [DialogSettingsComponent]
})
export class GridModule { }
