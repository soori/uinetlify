import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
    providedIn: 'root'
})
export class GridService {
    constructor(private http: HttpClient) { }
    getGridData(endpoint: string) {
        return this.http.get(`${endpoint}`);
    }
}
