import { SelectionModel } from '@angular/cdk/collections';
import { Location } from '@angular/common';
import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { StepperService } from '../../stepper/stepper.service';
import { DialogSettingsComponent } from '../dialog-settings/dialog-settings.component';
import { GridService } from '../grid.service';


@Component({
  selector: 'app-template-grid-create-cmp',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})

export class GridCreateComponent implements OnInit, AfterViewInit {
  config: any = {};
  @Input() item: any;
  selection = new SelectionModel<any>(false, []);
  isLoading = false;
  grid: any = {};
  initColumns: any[] = [
    // {
    //   name: 'name',
    //   display_name: 'Name',
    //   isVisible: true,
    //   isSortable: false,
    //   isSearchable: false,
    //   hasRowClick: false,
    //   type: '',
    //   dateFormat: ''
    // },
    // {
    //   name: 'description',
    //   display_name: 'Description',
    //   isVisible: true,
    //   isSortable: false,
    //   isSearchable: true,
    //   hasRowClick: false,
    //   type: 'string',
    //   dateFormat: ''
    // },
    // {
    //   name: 'created_by',
    //   display_name: 'Created by',
    //   isVisible: true,
    //   isSortable: false,
    //   isSearchable: true,
    //   hasRowClick: false,
    //   type: 'string',
    //   dateFormat: ''
    // }
    // ,
    // {
    //   name: 'lastName',
    //   display_name: 'Last name',
    //   isVisible: true,
    //   isSortable: true,
    //   isSearchable: true,
    //   hasRowClick: false,
    //   type: 'string',
    //   dateFormat: ''
    // },
    // {
    //   name: 'age',
    //   display_name: 'Age',
    //   isVisible: true,
    //   isSortable: true,
    //   isSearchable: true,
    //   hasRowClick: false,
    //   type: 'number',
    //   dateFormat: ''
    // },
    // {
    //   name: 'dob',
    //   display_name: 'D.O.B',
    //   isVisible: false,
    //   isSortable: true,
    //   isSearchable: true,
    //   hasRowClick: false,
    //   type: 'date',
    //   dateFormat: 'dd/mm/yyyy'
    // },
    // {
    //   name: 'phone',
    //   display_name: 'Contact',
    //   isVisible: true,
    //   isSortable: true,
    //   isSearchable: false,
    //   hasRowClick: false,
    //   type: 'number',
    //   dateFormat: ''
    // }

  ];
  tableData: any[] = [
    // { firstName: 'Bruce', lastName: 'Wayne', age: 30, dob: '06/09/1990', phone: 911 },
    // { firstName: 'John', lastName: 'Wick', age: 25, dob: '01/01/1994', phone: 100 },
    // { firstName: 'James', lastName: 'Bond', age: 26, dob: '21/01/1993', phone: 101 },
    // { firstName: 'Howard', lastName: 'Stark', age: 27, dob: '31/01/1990', phone: 102 },
    // { firstName: 'Bruce', lastName: 'Banner', age: 100, dob: '01/04/1894', phone: 107 },
    // { firstName: 'Peter', lastName: 'Parker', age: 20, dob: '04/05/1994', phone: 108 },
    // { firstName: 'Steve', lastName: 'Rogers', age: 23, dob: '07/05/1812', phone: 123 },
  ];
  displayedColumns: any[] = [];
  dataSource: MatTableDataSource<any>;
  filteredTableData: any[] = [];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  i = 0;
  placeholderForFilter = '';
  gridSettings: any = {
    dataEndpoint: '',
    columnEndpoint: '',
    hasRowSelection: true,
    hasPagination: true
  };
  gridView: any = {};
  showView = false;
  templateId: string | null;
  @Input() cmpLayoutItems: any = [];
  constructor(public dialog: MatDialog, private stepperService: StepperService, private router: Router,
    private route: ActivatedRoute, private gridService: GridService, private location: Location) { }

  ngOnInit() {

    this.gridView = JSON.parse(localStorage.getItem('template') || 'null');
    if (this.item && this.item.isPageBuilderView) {
      this.showView = true;
      const gridAttr = this.item.attributes;
      this.displayedColumns = gridAttr.displayedColumns;
      this.gridSettings = gridAttr.settings;
      this.initColumns = gridAttr.data;
    } else if (this.gridView && this.gridView.isTemplateView && this.gridView.details.type.id === 4) {
      this.showView = true;
      const gridAttr = this.gridView.attributes;
      this.displayedColumns = gridAttr.displayedColumns;
      this.gridSettings = gridAttr.settings;
      this.initColumns = gridAttr.data;
    }
    this.processData();
    this.templateId = this.route.snapshot.paramMap.get('id');
  }

  processData() {
    // check for duplicates here
    for (const item of this.initColumns) {
      if (item.name && item.isVisible) {
        this.filteredTableData.push(item);
      }
    }
    // duplicates in displayedColumns here too
    this.displayedColumns = this.filteredTableData.map(col => col.name);
    this.dataSource = new MatTableDataSource(this.tableData);
    this.filterColumns();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  selectedRowItem(selectedRow: any) {
    this.selection.toggle(selectedRow);
  }

  getData() {
    this.isLoading = true;
  }

  applyFilter(event: any) {
    const filterValue = event.target.value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  submit() {
    this.isLoading = true;
    const girdDetails = {
      attributes: {
        displayedColumns: this.displayedColumns,
        data: this.initColumns,
        settings: this.gridSettings
      },
      details: {
        name: this.item.name,
        type: this.item.type
      }
    };
    this.stepperService.savedTemplate(girdDetails).subscribe(data => {
      this.isLoading = false;
      this.router.navigate(['./../home'], { relativeTo: this.route });
    },
      err => {
        alert('error');
        this.isLoading = false;
      });
  }
  onCheckboxChange(item: any, event: any, id: number) {
    if (event.checked) {
      if (id === 0) {
        item.isVisible = true;
        this.filteredTableData.push(item);
      } else if (id === 1) {
        item.isSortable = true;
      } else if (id === 2) {
        item.isSearchable = true;
        this.displayedColumns.push(item.name);
      }
    } else {
      if (id === 0) {
        item.isVisible = false;
        const i = this.filteredTableData.indexOf(item);
        this.filteredTableData.splice(i, 1);
      } else if (id === 1) {
        item.isSortable = false;
      } else if (id === 2) {
        item.isSearchable = false;
        const i = this.displayedColumns.indexOf(item.name);
        this.displayedColumns.splice(i, 1);
      }
    }
    this.displayedColumns = this.filteredTableData.map(col => col.name);
    this.filterColumns();
  }

  filterColumns() {
    const noSearch = this.initColumns.filter(item => item.isSearchable);
    let filterColumns = JSON.parse(JSON.stringify(this.displayedColumns));
    filterColumns = this.displayedColumns.filter(o1 => noSearch.some(o2 => o1 === o2.name));
    this.dataSource.filterPredicate = (data, filter) => {
      let filterQuery = false;
      for (const column of filterColumns) {
        if (column in data) {
          filterQuery = (filterQuery || data[column].toString().trim().toLowerCase().indexOf(filter.trim().toLowerCase()) !== -1);
        }
      }
      return filterQuery;
    };
    this.placeholderForFilter = 'Filter by';
    for (const header of filterColumns) {
      if (this.i === 0) {
        this.placeholderForFilter = this.placeholderForFilter + '  ' + this.camelCase(header.toLowerCase());
      } else {
        this.placeholderForFilter = this.placeholderForFilter + ',  ' + this.camelCase(header.toLowerCase());
      }
      this.i++;
    }
  }

  camelCase(str: any) {
    const reserve: string = str
      .replace(/\s(.)/g, function (a: any) {
        return a.toUpperCase();
      })
      .replace(/^(.)/, function (b: any) {
        return b.toLowerCase();
      });
    return reserve.charAt(0).toUpperCase() + reserve.slice(1);
  }
  openSettings() {
    const dialogSetRef = this.dialog.open(DialogSettingsComponent, {
      disableClose: true,
      data: { grid: this.gridSettings ? JSON.parse(JSON.stringify(this.gridSettings)) : null }
    });
    dialogSetRef.afterClosed().subscribe(result => {
      if (result) {
        this.gridSettings = result;
        if (this.gridSettings.dataEndpoint) {
          this.getGridData(this.gridSettings.dataEndpoint, true);
        }
        if (this.gridSettings.columnEndpoint) {
          this.getGridData(this.gridSettings.columnEndpoint, false);
        }
        if (!this.gridSettings.hasRowSelection) {
          this.displayedColumns.splice(0, 1);
        } else {
          if (this.initColumns && (this.displayedColumns[0] !== this.initColumns[0].name)) {
            this.displayedColumns.splice(0, 0, this.initColumns[0].name);
          }
        }
      }
    });
  }
  getGridData(endpoint: any, isGridData: any) {
    this.gridService.getGridData(endpoint).subscribe(data => {
      if (data) {
        if (isGridData) {
          this.tableData = [];
          const gridData: any = data;
          this.tableData = gridData;
        } else {
          this.initColumns = [];
          const columns: any = data;
          this.initColumns = columns;
        }
        this.processData();
      }
    });
  }
  goBack() {
    this.location.back();
  }
  onViewClick() {

  }
}
