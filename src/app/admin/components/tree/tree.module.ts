import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { TreeCreateComponent } from './create/create.component';


@NgModule({
  declarations: [TreeCreateComponent],
  imports: [
    CommonModule,
    DragDropModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule
  ],
  exports: [TreeCreateComponent],
  entryComponents: []
})
export class TreeModule { }
