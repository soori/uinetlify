import { SelectionModel } from '@angular/cdk/collections';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { TreeService } from '../tree.service';

interface TreeNode {
  display_name: string;
  id: string;
  parent: string;
  code: string;
  children?: TreeNode[];
}

@Component({
  selector: 'app-template-tree-create-cmp',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class TreeCreateComponent implements OnInit, OnChanges {
  selectedList: any[] = [];
  nodesFetched: Object;
  isSelectable = false;
  treeSettings: any = {};
  isLoading = false;
  treeView: any;
  showView = false;
  @Input() treeSelection: any[] = [];
  treeData: TreeNode[];
  @Input()
  config: any;
  @Output() selection: EventEmitter<any> = new EventEmitter();
  @Input() permissions: any;
  permissionView = false;
  @Input() isDisabled = false;
  templateId: string | null;
  @Input() parentProperty: any = 'code';
  @Input() childProperty: any = 'parent';
  @Input() displayLabel: any = 'display_name';
  @Input() isItemSelectable = true;
  @Input() hasMultipleSelection = true;
  @Input() hasIcon = false;
  treeControl = new NestedTreeControl<TreeNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<TreeNode>();
  checklistSelection = new SelectionModel<TreeNode>(this.hasMultipleSelection /* multiple */);
  getChildren = (node: TreeNode): TreeNode[] | undefined => node.children;

  constructor(private service: TreeService, private location: Location,
    private router: Router, private route: ActivatedRoute, private _messageService: MessageService,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.checklistSelection = new SelectionModel<TreeNode>(this.hasMultipleSelection);
    this.treeView = JSON.parse(localStorage.getItem('template') || 'null');
    if (this.permissions) {
      this.permissionView = true;
      this.treeSettings.isSelectable = true;
      this.processTreeData(this.permissions);
    } else if (this.config && this.config.isPageBuilderView) {
      this.showView = true;
      this.treeSettings = this.config.settings;
      this.getTreeData(this.config.settings.endPoint);
    } else if (this.treeView && this.treeView.isTemplateView && this.treeView.details.type.id === 10) {
      // this.showView = true;
      this.treeSettings = this.treeView.settings;
      this.getTreeData(this.treeSettings.endPoint);
    }
    this.templateId = this.route.snapshot.paramMap.get('id');
    if (!this.isItemSelectable) {
      this.treeSettings.isSelectable = this.isItemSelectable;
    }
  }

  ngOnChanges() {
  }

  prepareinitialSelectedList(node: TreeNode, list: any[]) {
    if (this.treeSelection.includes(node.code) || this.treeSelection.includes(node.id)) {
      list.push(node);
      this.checklistSelection = new SelectionModel<any>(this.hasMultipleSelection, list, true);
      this.checklistSelection.select(node);
    }
    if (node.children && node.children.length > 0) {
      for (const childeNode of node.children) {
        this.prepareinitialSelectedList(childeNode, list);
      }
    }
  }

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: TreeNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: TreeNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: TreeNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (this.checklistSelection.selected) {
      this.emitSelection();
    }
    // this.checkAllParentsSelection(node);
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: TreeNode): void {
    this.checklistSelection.toggle(node);
    if (this.checklistSelection.selected) {
      this.emitSelection();
    }
    // this.checkAllParentsSelection(node);
  }
  hasChild = (_: number, node: TreeNode) => !!node.children && node.children.length > 0;

  list_to_tree(list: any) {
    const map: any[] = [];
    let node: any;
    let i: number;
    const roots: any[] = [];
    for (i = 0; i < list.length; i += 1) {
      map[list[i][this.parentProperty]] = i; // initialize the map
      list[i].children = []; // initialize the children
    }
    for (i = 0; i < list.length; i += 1) {
      node = list[i];
      if (node) {
        if (node[this.childProperty]) {
          if (!list[map[node[this.childProperty]]].children) {
            list[map[node[this.childProperty]]].children = [];
          }
          list[map[node[this.childProperty]]].children.push(node);
        } else {
          roots.push(node);
        }
      }
    }
    return roots;
  }
  getTreeData(endpoint: string) {
    this.service.getData(endpoint).subscribe(data => {
      if (data) {
        this.nodesFetched = data;
        this.processTreeData(this.nodesFetched);
      }
    });
  }
  processTreeData(data: any) {
    this.treeData = this.list_to_tree(data);
    this.dataSource.data = this.treeData;
    const initiallySectedList: TreeNode[] = [];
    if (this.treeSelection) {
      for (const parentNode of this.treeData) {
        this.prepareinitialSelectedList(parentNode, initiallySectedList);
      }
    }
  }

  submit() {
    this.isLoading = true;
    const treeLayout = {
      details: this.config,
      attributes: this.dataSource.data,
      settings: this.treeSettings
    };
    this.service.savedTemplate(treeLayout).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Tree saved successfully');
      this.router.navigate(['./../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.info('Error occured, Try again !');
        this.isLoading = false;
      });
  }
  emitSelection() {
    let emitSelection = {};
    let selectedId: Array<string> = [];
    if (this.hasMultipleSelection) {
      selectedId = !this.permissionView ?
        this.checklistSelection.selected.map(item => item.id) : this.checklistSelection.selected.map(item => item.code);
      emitSelection = {
        id: !this.permissionView ? this.config._id : null,
        outputName: this.treeSettings.outputName,
        selectedNodes: this.checklistSelection.selected,
        selectedIds: selectedId
      };
    } else {
      selectedId = this.checklistSelection.selected.map(item => item.id);
      emitSelection = {
        selectedNodes: this.checklistSelection.selected,
        selectedIds: selectedId
      };
    }
    this.selection.emit(emitSelection);
  }
  onViewClick() {
    this.showView = true;
  }
  update() {
    this.isLoading = true;
    const treeLayout = {
      details: this.config,
      attributes: this.dataSource.data,
      settings: this.treeSettings
    };
    this.service.updateTemplate(treeLayout, this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Tree updated successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.info('Error occured, Try again !');
        this.isLoading = false;
      });
  }
  goBack() {
    this.location.back();
  }
  delete() {
    this.isLoading = true;
    this.service.deleteTemplate(this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Template deleted successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this.isLoading = false;
        this._messageService.warn('Error occures, Try again');
      });
  }

}
