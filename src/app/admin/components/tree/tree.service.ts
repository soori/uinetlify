import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class TreeService {

  constructor(private location: Location, private router: Router, private http: HttpClient) { }

  savedTemplate(template: any) {
    return this.http.post(`${environment.apiEndpoint}/formbuilder`, template);
  }
  updateTemplate(template: any, id: any) {
    return this.http.put(`${environment.apiEndpoint}/formbuilder/${id}`, template);
  }
  deleteTemplate(id: any) {
    return this.http.delete(`${environment.apiEndpoint}/formbuilder/${id}`);
  }
  getData(endpoint: any) {
    return this.http.get(`${endpoint}`);
  }

}
