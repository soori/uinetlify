import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { StepperService } from '../stepper/stepper.service';
import { DialogTabEditComponent } from './config/dialog-tab-edit.component';
import { TabService } from './tab.service';

@Component({
  selector: 'app-template-tab-cmp',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() config: any;
  isLoading: Boolean = false;
  todoList: any;
  emptyArray: any[] = [];
  tabs = [
    {
      id: 1,
      name: 'Tab',
      backgroundColor: { id: 1, name: 'basic', display_name: 'Basic' },
      color: { id: 1, name: '', display_name: '' },
      headerPosition: { id: 1, name: 'above' },
      align: { id: 1, name: 'start', displayName: 'Start' },
      tab: [
        {
          label: 'label 1',
          content: this.emptyArray,
          icon: '',
          isDisabled: false
        },
        {
          label: 'label 2',
          content: [],
          icon: '',
          isDisabled: false
        },
        {
          label: 'label 3',
          content: [],
          icon: '',
          isDisabled: false
        },
      ],
    },
  ];
  selectedTabContent: any = [];
  tabTodoItems: any = [];
  tabItems: any = [];
  tabFormGroup: FormArray;
  selectedTab = 0;

  showView: Boolean = false;
  tabTemplateView: any = [];
  @Output() tabIndexChange = new EventEmitter<number>();
  selectedNodes: any[] = [];
  @Output() treeSelection: EventEmitter<any> = new EventEmitter();
  selectedNodeIds: any;
  templateId: string | null;
  @Input() cmpLayoutItems: any = [];
  cleanTabs: any = [];

  constructor(private router: Router, private route: ActivatedRoute, private stepperService: StepperService,
    private service: TabService, private fb: FormBuilder, public dialog: MatDialog, private _messageService: MessageService,
    protected translate: TranslateService, protected store: Store<UserLanguageState>, private location: Location) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.tabFormGroup = this.fb.array([]);
    this.selectedTabContent = this.tabs[0].tab[this.selectedTab].content;
    this.tabTemplateView = JSON.parse(localStorage.getItem('template') || 'null');
    this.cleanTabs = JSON.parse(JSON.stringify(this.tabs));
    // Page builder
    if (this.config && this.config.isPageBuilderView) {
      this.showView = true;
      this.tabs = this.config.attributes;
      this.selectedTabContent = this.config.attributes[0].tab[this.selectedTab].content;
    } else if (this.tabTemplateView && this.tabTemplateView.isTemplateView && this.tabTemplateView.details.type.id === 3) {
      // Grid select
      // this.showView = true;
      this.tabs = this.tabTemplateView.attributes;
      this.selectedTabContent = this.tabTemplateView.attributes[0].tab[this.selectedTab].content;
      this.getPageItems();
    } else {
      // create
      this.getPageItems();
    }
    this.templateId = this.route.snapshot.paramMap.get('id');
  }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      this.selectedTabContent = this.tabs[0].tab[this.selectedTab].content;
      if (this.tabs[0].tab.length > 0) {
        const doneCopy = this.createCopy(this.selectedTabContent[event.currentIndex]);
        this.selectedTabContent[event.currentIndex] = doneCopy;
        this.selectedTabContent.forEach((item: any, i: any) => {
          item.position = i;
          if (item.details.type.id === 7 || item.details.type.id === 10) {
            item.isPageBuilderView = true;
          }
        });
      }
    } else if (event.previousIndex !== event.currentIndex) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
    }
  }

  createCopy(orig: any) {
    return JSON.parse(JSON.stringify(orig));
  }

  submit() {
    this.isLoading = true;
    const tabDetails = {
      attributes: this.tabs,
      details: {
        name: this.config.name,
        type: this.config.type
      }
    };
    this.service.savedTemplate(tabDetails).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Tab saved successfully');
      this.router.navigate(['./../home'], { relativeTo: this.route });
    },
      err => {
        alert('error');
        this._messageService.warn('Error occured, Try again !');
        this.isLoading = false;
      });
  }
  getPageItems() {
    this.isLoading = true;
    this.stepperService.getComponentTemplates().subscribe(data => {
      this.isLoading = false;
      this.tabItems = data;
      this.tabItems.forEach((element: any) => {
        if (element.details.type.id !== 3) {
          element.hasMultipleForms = true;
          const form = this.initFormGroup(element._id);
          this.tabFormGroup.push(form);
          this.tabTodoItems.push(element);
        }
      });
    });
  }
  initFormGroup(name: any): FormGroup {
    name = this.fb.group({
    });
    return name;
  }
  removeTabItem(itemIndex: number) {
    this.selectedTabContent.splice(itemIndex, 1);
  }
  addTab() {
    const length = this.tabs[0].tab.length + 1;
    this.tabs[0].tab.push({
      label: 'label' + ' ' + length,
      content: ['faafafaf' + ' ' + length],
      icon: '',
      isDisabled: false
    });
  }
  removeTab(tabIndex: number) {
    this.tabs[0].tab.splice(tabIndex, 1);
  }
  editTab(tab: any, tabIndex: number, isTab: Boolean) {
    const tabData = {
      tab: tab,
      isTabEdit: isTab
    };
    const dialogRef = this.dialog.open(DialogTabEditComponent, {
      data: JSON.parse(JSON.stringify(tabData))
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.isTabEdit) {
          this.tabs[0].tab[tabIndex] = result.tabData;
        } else {
          this.tabs[0] = result.tabData;
        }
      }
    });
  }
  tabChange(event: any) {
    this.selectedTab = event;
    this.tabIndexChange.emit(event);
    if (this.tabTemplateView && this.tabTemplateView.isTemplateView) {
      this.selectedTabContent = this.tabTemplateView.attributes[0].tab[this.selectedTab].content;
    } else if (this.config && this.config.isPageBuilderView) {
      this.selectedTabContent = this.config.attributes[0].tab[this.selectedTab].content;
    } else if (this.tabs[0].tab.length > 0) {
      this.selectedTabContent = this.tabs[0].tab[this.selectedTab].content;
    }
    // if (this.selectedTabContent.details.type.id === 10) {
    const index = this.selectedNodes.findIndex(item => item.id === this.selectedTabContent[0]._id);
    if (index === -1) {
      this.selectedNodeIds = [];
    } else if (index >= 0) {
      const configSaved = this.selectedNodes[index];
      this.selectedNodeIds = configSaved.selectedIds;
    }
    // }
  }
  resetTabGroup() {
    this.selectedTabContent = [] //running all tab will be reset
    this.tabs = this.cleanTabs;
  }
  onViewClick() {
    this.showView = true;
  }
  routeBack() {
    this.router.navigate(['./../../home'], { relativeTo: this.route });
  }
  getSelection(selectedConfig: any) {
    if (selectedConfig.selectedNodes) {
      const index = this.selectedNodes.findIndex(item => item.id === selectedConfig.id);
      if (index === -1) {
        this.selectedNodes.push(selectedConfig);
      } else if (index >= 0) {
        this.selectedNodes[index] = selectedConfig;
      }
      this.treeSelection.emit(this.selectedNodes);
    }
  }
  update() {
    this.isLoading = true;
    const tabDetails = {
      attributes: this.tabs,
      details: {
        name: this.config.name,
        type: this.config.type
      }
    };
    this.service.updateTemplate(tabDetails, this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Tab updated successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        alert('error');
        this._messageService.warn('Error occured, Try again !');
        this.isLoading = false;
      });
  }
  goBack() {
    this.location.back();
  }
  getFormValid(event: any) {
  }
  getFormValue(event: any) {
  }
  getMultipleFormValue(event: any) {
  }
  delete() {
    this.isLoading = true;
    this.service.deleteTemplate(this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Template deleted successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this.isLoading = false;
        this._messageService.warn('Error occures, Try again');
      });
  }
  
}
