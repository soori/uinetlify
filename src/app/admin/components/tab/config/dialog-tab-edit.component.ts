import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-dialog-tab-edit',
  templateUrl: './dialog-tab-edit.component.html',
  styleUrls: ['./dialog-tab-edit.component.scss']
})
export class DialogTabEditComponent implements OnInit {

  tabData: any = {};
  tab: any = {};
  headerPositions: any = [
    { id: 1, name: 'above' },
    { id: 2, name: 'below' }
  ];
  colors: any = [
    { id: 1, name: 'basic', display_name: 'Basic' },
    { id: 2, name: 'primary', display_name: 'Primary' },
    { id: 1, name: 'accent', display_name: 'accent' },
    { id: 2, name: 'warn', display_name: 'warn' }
  ];
  tabAlign: any = [
    { id: 1, name: 'start', displayName: 'Start' },
    { id: 2, name: 'center', displayName: 'Center' },
    { id: 3, name: 'end', displayName: 'End' },
  ];
  isTabEdit: Boolean = false;
  constructor(public dialogRef: MatDialogRef<DialogTabEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.tabData = this.data.tab;
    this.isTabEdit = this.data.isTabEdit;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  saveTabData() {
    this.dialogRef.close({ tabData: this.tabData, isTabEdit: this.isTabEdit });
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
