import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { ButtonModule } from '../button/button.module';
import { CardModule } from '../card/card.module';
import { FormFieldsModule } from '../form/form.module';
import { StepperModule } from '../stepper/stepper.module';
import { TreeModule } from '../tree/tree.module';
import { DialogTabEditComponent } from './config/dialog-tab-edit.component';
import { TabComponent } from './tab.component';

@NgModule({
  declarations: [TabComponent, DialogTabEditComponent],
  imports: [
    CommonModule,
    DragDropModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    FormFieldsModule,
    CardModule,
    ButtonModule,
    StepperModule,
    TreeModule
  ],
  exports: [TabComponent],
  entryComponents: [DialogTabEditComponent]
})
export class TabModule { }
