import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { ButtonModule } from '../button/button.module';
import { CardRoutingModule } from './card-routing.module';
import { ActionsComponent } from './config/actions/actions.component';
import { ContentComponent } from './config/content/content.component';
import { FooterComponent } from './config/footer/footer.component';
import { HeaderComponent } from './config/header/header.component';
import { CardCreateComponent } from './create/create.component';
import { CardViewComponent } from './view/view.component';
import { ImageComponent } from './config/image/image.component';


@NgModule({
  declarations: [CardCreateComponent, CardViewComponent,
    HeaderComponent, ContentComponent, ActionsComponent, FooterComponent, ImageComponent],
  imports: [
    CommonModule,
    CardRoutingModule,
    DragDropModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    ButtonModule
  ],
  exports: [CardCreateComponent, CardViewComponent],
  entryComponents: []
})
export class CardModule { }
