import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { ButtonConfigComponent } from '../../button/config/button/button-config.component';
import { ButtonViewConfigurationDialogComponent } from '../../button/config/view/view-configuration.component';
import { CardService } from '../card.service';
import { ActionsComponent } from '../config/actions/actions.component';
import { ContentComponent } from '../config/content/content.component';
import { FooterComponent } from '../config/footer/footer.component';
import { HeaderComponent } from '../config/header/header.component';
import { ImageComponent } from '../config/image/image.component';


@Component({
  selector: 'app-template-card-create-cmp',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CardCreateComponent implements OnInit {
  isLoading: Boolean = false;
  @Input() config: any;
  @Output() viewMode = new EventEmitter<boolean>();
  // card var starts
  cards: any[] = [
    {
      id: 1,
      name: 'header',
      icon: 'title',
      modelName: '',
      header: {
        title: 'Header',
        subtitle: 'Sub Header',
      },
      properties: {
        layout: { id: 2, name: 'column', display_name: 'Column' },
        alignment: 'center center',
      }
    },
    {
      id: 2,
      name: 'content',
      icon: 'description',
      modelName: '',
      content: {
        // id: 1,
        title: '',
        subtitle: '',
        text: 'Content asdfghgfdsdfgh',
        // tslint:disable-next-line:max-line-length
        imageUrl: 'https://images.pexels.com/photos/159866/books-book-pages-read-literature-159866.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
        imgHeight: 100,
        imgWidth: 100,
        imageRadius: 50,
        hasImage: true,
      },
      properties: {
        layout: { id: 1, name: 'row', display_name: 'Row' },
        alignment: 'center center',
        flexOrder: { id: 1, value: 0, display_name: 'Left' },
        // flex: 85

      }
    },
    {
      id: 3,
      name: 'image',
      icon: 'image',
      modelName: '',
      image: {
        // tslint:disable-next-line:max-line-length
        imageUrl: './assets/img/users/user.jpg',
        imgHeight: 180,
        imgWidth: 170,
        imageRadius: 50,
      },
    },
    {
      id: 4,
      name: 'footer',
      icon: 'call_to_action',
      modelName: '',
      Footer: {
        text: 'offer',
        iconName: 'home'
      },
      properties: {
        alignment: 'space-between end',
        flex: '1 1 100%',
      }
    },
  ];
  cardsDone: any[] = [
  ];
  cardProp: any;
  cardArray: any[] = [];
  cardLayout: any;
  @Input() cmpLayoutItems: any = [];
  templateId: string | null;
  showCreate: Boolean = true;
  dialogCardRef: any;

  // card var ends

  buttons = [
    {
      id: 1,
      style: 'Basic Button',
      color: 'primary',
      label: 'Basic',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
    {
      id: 2,
      style: 'Raised Button',
      color: 'basic',
      label: 'Raised',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
    {
      id: 3,
      style: 'Stroked Button',
      color: 'accent',
      label: 'Stroked',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
    {
      id: 4,
      style: 'Flat Button',
      color: 'warn',
      label: 'Flat',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
  ];
  buttonsDone: any = [];
  buttonConfig: any = {
    name: 'row',
    alignment: 'start start'
  };

  constructor(protected router: Router, protected route: ActivatedRoute, private service: CardService,
    protected translate: TranslateService, protected store: Store<UserLanguageState>, public dialog: MatDialog,
    private _messageService: MessageService, private location: Location) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.templateId = this.route.snapshot.paramMap.get('id');
    if (this.cmpLayoutItems.length > 0) {
      this.cardsDone = this.cmpLayoutItems;
    }
  }
  // cards content starts

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      const doneCopy = this.createCopy(this.cardsDone[event.currentIndex]);
      this.cardsDone[event.currentIndex] = doneCopy;
      this.cardsDone.forEach((item: any, i: any) => {
        item.position = i;
      });
    } else if (event.previousIndex !== event.currentIndex) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
    }
  }
  removeCard(j: number) {
    this.cardsDone.splice(j, 1);
  }
  EditHeader(item: any, id: number) {
    if (item.id === 1) {
      this.dialogCardRef = this.dialog.open(HeaderComponent, {
        width: '100vw',
        data: { value: item },
        disableClose: true,
      });
    } else if (item.id === 2) {
      this.dialogCardRef = this.dialog.open(ContentComponent, {
        width: '100vw',
        data: { value: item },
        disableClose: true,
      });
    } else if (item.id === 3) {
      this.dialogCardRef = this.dialog.open(ImageComponent, {
        width: '100vw',
        data: { value: item },
        disableClose: true,
      });
    } else if (item.id === 4) {
      this.dialogCardRef = this.dialog.open(FooterComponent, {
        width: '100vw',
        data: { value: item },
        disableClose: true,
      });
    }
    this.dialogCardRef.afterClosed().subscribe((result: any) => {
      this.cardsDone[item.position] = result;
      if (result) {
        if (result.attribute === 'header') {

          result = {
            id: 1,
            alignment: result.hAlign.name + ' ' + result.vAlign.name,
            flex: result.flex ? result.flex : '100%',
            attribute: result.attribute,
            header: result.header.title,
            layout: result.layout,
          };
        } else if (result.attribute === 'content') {
          result.properties = {
            id: 2,
            alignment: result.hAlign.name + ' ' + result.vAlign.name,
            flex: result.flex ? result.flex : '100%',
            content: result.content,
            imgHeight: result.imgHeight,
            imgWidth: result.imgWidth,
            attribute: result.attribute,
            layout: result.layout
          };
        } else if (result.attribute === 'footer') {
          result.properties = {
            id: 4,
            alignment: result.hAlign.name + ' ' + result.vAlign.name,
            flex: result.flex ? result.flex : '100%',
            attribute: result.attribute,
            footerContent: result.footerContent
          };
        } else if (result.attribute === 'image') {
          result.properties = {
            id: 3,
            alignment: result.hAlign.name + ' ' + result.vAlign.name,
            content: result.content,
            imgHeight: result.imgHeight,
            imgWidth: result.imgWidth,
            layout: result.layout,
            imageRadius: result.imageRadius
          };
        }
        this.cardsDone.forEach(element => {
          if (result.id === element.id) {
            element = result;
          }
        });
      }
    });
  }
  processCards() {
    this.cardLayout = {
      details: this.config,
      attributes: this.cardsDone,
      isTemplateView: false
    };
    return this.cardLayout;
  }

  submitCards() {
    this.isLoading = true;
    const cardLayout = this.processCards();
    this.service.saveTemplate(cardLayout).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Card saved successfully');
      this.router.navigate(['./../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, Try again !');
        this.isLoading = false;
      });
  }

  onViewClick() {
    this.showCreate = false;
    localStorage.removeItem('template');
    const cardData = this.processCards();
    const buttonData = {
      attributes: this.buttonsDone,
      layout: this.buttonConfig
    };
    // localStorage.setItem('template', JSON.stringify({ cardData: cardData, buttonData: buttonData }));
    localStorage.setItem('template', JSON.stringify(cardData));
  }

  updateCards() {
    this.isLoading = true;
    const cardLayout = this.processCards();
    const buttonData = {
      attributes: this.buttonsDone,
      layout: this.buttonConfig
    };
    this.service.updateTemplate(cardLayout, this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Card updated successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, Try again !');
        this.isLoading = false;
      });
  }
  goBack() {
    this.location.back();
  }
  dropButton(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      const doneCopy = this.createCopy(this.buttonsDone[event.currentIndex]);
      this.buttonsDone[event.currentIndex] = doneCopy;
      this.buttonsDone.forEach((item: any, i: any) => {
        item.position = i;
      });
    } else if (event.previousIndex !== event.currentIndex) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
    }
  }
  openButtonConfig(config: any) {
    const selectedTemplate: any = { layout: {} };
    selectedTemplate.layout = config;
    const dialogRef = this.dialog.open(ButtonViewConfigurationDialogComponent, {
      width: '100vw',
      data: JSON.parse(JSON.stringify(selectedTemplate)),
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.buttonConfig = result;
      }
    });
  }
  openButtonEditDialog(item: any) {
    const dialogRef = this.dialog.open(ButtonConfigComponent, {
      width: '100vw',
      data: { value: JSON.parse(JSON.stringify(item)) },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        item = result;
        this.buttonsDone[item.position] = item;
      }
    });
  }
  createCopy(orig: any) {
    return JSON.parse(JSON.stringify(orig));
  }
  openCardConfig() {
    const dialogRef = this.dialog.open(ButtonConfigComponent, {
      width: '100vw',
      // data: { value: JSON.parse(JSON.stringify()) },
    });
  }
  removeButton(j: number) {
    this.buttonsDone.splice(j, 1);
  }
  delete() {
    this.isLoading = true;
    this.service.deleteTemplate(this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Template deleted successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this.isLoading = false;
        this._messageService.warn('Error occures, Try again');
      });
  }

}
