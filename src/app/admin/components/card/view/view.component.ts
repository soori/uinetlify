import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-template-card-view-cmp',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class CardViewComponent implements OnInit {
  @Input() item: any;
  cardData: any;
  cardAttributes: any;
  templateId: string | null;
  buttonData: any;

  constructor(private route: ActivatedRoute, protected translate: TranslateService,
    protected store: Store<UserLanguageState>, private _messageService: MessageService) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    const cardTemplate: any = this.item ? this.item : JSON.parse(localStorage.getItem('template') || 'null');
    this.cardAttributes = cardTemplate.attributes;
    this.buttonData = cardTemplate.buttonData;
    this.templateId = this.route.snapshot.paramMap.get('id');
  }
}
