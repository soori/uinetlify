import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements OnInit {

  cardItem: any;
  item: any = {};
  hrzntlAlignments: any[] = [
    { id: 0, name: 'none', display_name: 'none' },
    { id: 1, name: 'start', display_name: 'Start' },
    { id: 2, name: 'center', display_name: 'Center' },
    { id: 3, name: 'end', display_name: 'End' },
    { id: 4, name: 'space-around', display_name: 'Space around' },
    { id: 5, name: 'space-between', display_name: 'Space between' },
    { id: 6, name: 'space-evenly', display_name: 'Space evenly' },
  ];
  verticalAlignments: any[] = [
    { id: 0, name: 'none', display_name: 'none' },
    { id: 1, name: 'start', display_name: 'Start' },
    { id: 2, name: 'center', display_name: 'Center' },
    { id: 3, name: 'end', display_name: 'End' },
    { id: 4, name: 'stretch', display_name: 'Stretch' },
  ];
  constructor(public dialogRef: MatDialogRef<ActionsComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  ngOnInit(): void {
    this.cardItem = this.data.value;
    if (this.cardItem) {
      this.item = this.cardItem;
      // alignment start
      const alignment = this.cardItem.properties.alignment.split(' ');
      const sleep = alignment[0];
      const stand = alignment[1];
      this.item.properties.hAlign = this.hrzntlAlignments.filter(hitem => {
        if (hitem.name === sleep) {
          return hitem;
        }
      });
      this.item.properties.vAlign = this.verticalAlignments.filter(vitem => {
        if (vitem.name === stand) {
          return vitem;
        }
      });
      this.item.properties.hAlign = this.item.properties.hAlign[0];
      this.item.properties.vAlign = this.item.properties.vAlign[0];
      // alignment end

    }
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  onNoClick() {
    this.dialogRef.close();
  }
  saveCardAttr() {
    this.item.properties.alignment = this.item.properties.hAlign.name + ' ' + this.item.properties.vAlign.name;
    this.dialogRef.close(this.item);
  }

}
