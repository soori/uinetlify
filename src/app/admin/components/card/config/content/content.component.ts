import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  cardItem: any;
  item: any = {};
  hrzntlAlignments: any[] = [
    { id: 0, name: 'none', display_name: 'none' },
    { id: 1, name: 'start', display_name: 'Start' },
    { id: 2, name: 'center', display_name: 'Center' },
    { id: 3, name: 'end', display_name: 'End' },
    { id: 4, name: 'space-around', display_name: 'Space around' },
    { id: 5, name: 'space-between', display_name: 'Space between' },
    { id: 6, name: 'space-evenly', display_name: 'Space evenly' },
  ];
  verticalAlignments: any[] = [
    { id: 0, name: 'none', display_name: 'none' },
    { id: 1, name: 'start', display_name: 'Start' },
    { id: 2, name: 'center', display_name: 'Center' },
    { id: 3, name: 'end', display_name: 'End' },
    { id: 4, name: 'stretch', display_name: 'Stretch' },
  ];

  imageView: any[] = [
    { id: 0, value: 50, View: 'Circle' },
    { id: 1, value: 0, View: 'Normal' }
  ];

  directions: any[] = [
    { id: 1, name: 'row', display_name: 'Row' },
    { id: 2, name: 'column', display_name: 'Column' }
  ];

  positions: any[] = [
    { id: 1, value: 0, display_name: 'Left' },
    { id: 2, value: 2, display_name: 'Right' }
  ];
  axix: any[] = [
    { id: 1, value: 0, display_name: 'top' },
    { id: 2, value: 2, display_name: 'left' }
  ];
  constructor(public dialogRef: MatDialogRef<ContentComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  ngOnInit(): void {
    this.item = this.data.value;
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  onNoClick() {
    this.dialogRef.close();
  }
  saveCardAttr() {
    this.dialogRef.close(this.item);
  }
}
