import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {


  imgItem: any;
  item: any = {};
  imageView: any[] = [
    { id: 0, value: 50, View: 'Circle' },
    { id: 1, value: 0, View: 'Normal' }
  ];
  constructor(public dialogRef: MatDialogRef<ImageComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  ngOnInit(): void {
    this.imgItem = this.data.value;

  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
  onNoClick() {
    this.dialogRef.close();
  }
  saveCardAttr() {
    // this.item.properties.alignment = this.item.properties.hAlign.name + ' ' + this.item.properties.vAlign.name;
    this.dialogRef.close(this.imgItem);
  }
}
