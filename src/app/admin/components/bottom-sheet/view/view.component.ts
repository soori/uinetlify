import { AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { PageEvent } from '@angular/material/paginator';

interface Buttongroup {
  label: string;
  navLink: string;
  subLabel: string;
}


@Component({
  selector: 'app-template-bottom-sheet-view-cmp',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class BottomSheetViewComponent implements OnInit, AfterViewInit {
  btnItems: Array<Buttongroup> = [] ;
  pgeEdtrItems: any = {};
  toolTipPos = 'left';
  lowValue = 0;
  highValue = 20;
  paginatorEvent: PageEvent;
  lastPage: number;
  @ViewChild('page', { static: true }) input: any;
  constructor(private _bottomSheetRef: MatBottomSheetRef<BottomSheetViewComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) { }

  ngOnInit() {
    this.btnItems = this.data.btnGrp.buttons;
    this.pgeEdtrItems = this.data.paraGrp.paragraphs;
  }

  ngAfterViewInit() {
    this.getPaginatorData(this.input);
  }

  openLink(event: MouseEvent, url: any): void {
    this._bottomSheetRef.dismiss(url);
    event.preventDefault();
  }
  public getPaginatorData(event: PageEvent): PageEvent {
    this.paginatorEvent = event;
    this.lastPage = Math.ceil(this.paginatorEvent.length / this.paginatorEvent.pageSize);
    this.lowValue = event.pageIndex * event.pageSize;
    this.highValue = this.lowValue + event.pageSize;
    return event;
  }
}
