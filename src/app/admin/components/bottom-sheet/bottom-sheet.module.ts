import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { FormFieldsModule } from '../form/form.module';
import { BottomSheetCreateComponent } from './create/create.component';
import { BottomSheetViewComponent } from './view/view.component';


@NgModule({
  declarations: [BottomSheetCreateComponent, BottomSheetViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    DragDropModule,
    FormFieldsModule,
    AngularEditorModule
  ],
  exports: [BottomSheetCreateComponent, BottomSheetViewComponent],
  entryComponents: [BottomSheetViewComponent],
  providers: [
    { provide: MatBottomSheetRef, useValue: {} },
    { provide: MAT_BOTTOM_SHEET_DATA, useValue: [] },
  ]
})
export class BottomSheetModule { }
