import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { BottomSheetViewComponent } from '../view/view.component';
import { BottomSheetService } from '../bottom-sheet.service';


@Component({
  selector: 'app-template-bottom-sheet-create-cmp',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class BottomSheetCreateComponent implements OnInit {
  item: any;
  @Input() config: any;
  toDoItems: any[] = [
    {
      id: 1,
      name: 'Button',
      label: 'Button label',
      subLabel: 'Button sub label',
      icon: 'keyboard',
      navLink: '../',
      content: '',
      hasActions: false,
      actions: []
    },
    {
      id: 2,
      name: 'Paragraph',
      label: 'Para label',
      subLabel: 'Para sub label',
      icon: 'line_style',
      navLink: '../',
      content: 'Paragraph content',
      hasActions: true,
      actions: []
    }
  ];
  doneItems: any[] = [];

  btnGroup: FormGroup;
  paraEditr: FormGroup;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };
  isLoading: Boolean = false;
  templateId: string | null;

  constructor(private _bottomSheet: MatBottomSheet, private formBuilder: FormBuilder, private service: BottomSheetService,
    private router: Router, private route: ActivatedRoute, private _messageService: MessageService, private location: Location) { }

  ngOnInit() {
    this.btnGroup = this.formBuilder.group({
      buttons: this.formBuilder.array([
        // this.initOption(),
      ])
    });
    this.templateId = this.route.snapshot.paramMap.get('id');
    const template = JSON.parse(localStorage.getItem('template') || 'null');
    if (template) {
      this.doneItems = template.attributes;
      this.setExistingoptions(template);
    }
    this.paraEditr = this.formBuilder.group({
      paragraphs: this.formBuilder.array([
        // this.initOption(),
      ])
    });
  }

  setExistingoptions(template: any) {
    const control = <FormArray>this.btnGroup.controls['buttons'];
    template.attributes[0].buttons.forEach((element: any) => {
      control.push(this.pushOption(element));
    });
  }

  pushOption(element: any) {
    return this.formBuilder.group({
      label: [element.label],
      navLink: [element.navLink],
      subLabel: [element.subLabel]
    });
  }

  initItems(id: number) {
    return this.formBuilder.group(
      id === 1 ? { label: [''], subLabel: [''], navLink: [''] } : { content: [''] }
    );
  }
  addItems(id: number) {
    const itemControl = id === 1 ? <FormArray>this.btnGroup.get('buttons') : <FormArray>this.paraEditr.get('paragraphs');
    itemControl.push(this.initItems(id));
  }

  // initParagraph() {
  //   return this.formBuilder.group({
  //     label: [''],
  //     subLabel: [''],
  //     navLink: [''],
  //     content: ['']
  //   });
  // }

  drop(event: CdkDragDrop<any[]>, item: any) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      if (event.container.data[event.currentIndex].id) {
        this.addItems(event.container.data[event.currentIndex].id);
      }
      const doneCopy = this.createCopy(this.doneItems[event.currentIndex]);
      this.doneItems[event.currentIndex] = doneCopy;
      this.doneItems.forEach((itemm, i) => {
        itemm.position = i;
      });
    } else if (event.previousIndex !== event.currentIndex) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
    }
  }
  createCopy(orig: any) {
    return JSON.parse(JSON.stringify(orig));
  }
  removeItem(i: number) {
    const control = <FormArray>this.btnGroup.get('buttons');
    control.removeAt(i);
  }
  openBottomSheet(): void {
    const bottomSheetRef = this._bottomSheet.open(BottomSheetViewComponent, {
      data: { btnGrp: this.btnGroup.value, paraGrp: this.paraEditr.value },
      hasBackdrop: true,
      panelClass: 'custom-bottom-sheet-width'
    });
  }

  submit() {
    this.isLoading = true;
    const attributes: any[] = [];
    attributes.push(this.btnGroup.value, this.paraEditr.value);
    const btmShtLayout = {
      details: this.config,
      attributes: attributes,
      isTemplateView: false
    };
    this.service.savedTemplate(btmShtLayout).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Bottom Sheet saved successfully');
      this.router.navigate(['./../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.info('Error occured, Try again !');
        this.isLoading = false;
      });
  }
  update() {
    this.isLoading = true;
    const attributes: any[] = [];
    attributes.push(this.btnGroup.value, this.paraEditr.value);
    const btmShtLayout = {
      details: this.config,
      attributes: attributes,
      isTemplateView: false
    };
    this.service.updateTemplate(btmShtLayout, this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Bottom sheet updated successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, Try again !');
      });
  }
  goBack() {
    this.location.back();
  }
  delete() {
    this.isLoading = true;
    this.service.deleteTemplate(this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Template deleted successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this.isLoading = false;
        this._messageService.warn('Error occures, Try again');
      });
  }
}
