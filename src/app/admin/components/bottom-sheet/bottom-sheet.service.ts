import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class BottomSheetService {

  constructor(private location: Location, private router: Router, private http: HttpClient) { }

  getMenuData() {
    return this.http.get(`${environment.apiEndpoint}/menu`);
  }

  getPermissions(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiEndpoint}/config/global/preauth/landingpage/1/1/usertypeCodes`);
  }

  saveTemplate(template: any) {
    return this.http.post(`${environment.apiEndpoint}/formtemplate`, template);
  }
  savedTemplate(template: any) {
    return this.http.post(`${environment.apiEndpoint}/formbuilder`, template);
  }
  getTemplate() {
    return this.http.get(`${environment.apiEndpoint}/formtemplate`);
  }
  updateTemplate(template: any, id: any) {
    return this.http.put(`${environment.apiEndpoint}/formbuilder/${id}`, template);
  }
  deleteTemplate(id: any) {
    return this.http.delete(`${environment.apiEndpoint}/formbuilder/${id}`);
  }
  getSavedTemplate() {
    return this.http.get(`${environment.apiEndpoint}/formbuilder`);
  }
  getActivityTypes() {
    return this.http.get('http://192.168.1.4:8080/lms-services/metadata/activitytype');
  }
  getData(endpoint: any) {
    return this.http.get(`${endpoint}`);
  }

}
