import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { ButtonModule } from '../button/button.module';
import { CardModule } from '../card/card.module';
import { FormFieldsModule } from '../form/form.module';
import { StepperCreateComponent } from './create/create.component';
import { StepperHomeComponent } from './home/home.component';
import { StepperRoutingModule } from './stepper-routing.module';
import { StepperViewComponent } from './view/view.component';
@NgModule({
  declarations: [StepperHomeComponent, StepperCreateComponent, StepperViewComponent],
  imports: [
    CommonModule,
    StepperRoutingModule,
    DragDropModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    FormFieldsModule,
    CardModule,
    ButtonModule
  ],
  exports: [StepperHomeComponent, StepperCreateComponent],
  entryComponents: []
})
export class StepperModule { }
