import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class StepperService {
    constructor(private http: HttpClient) { }

    getComponentTemplates() {
        return this.http.get(`${environment.apiEndpoint}/formbuilder`);
    }
    savedTemplate(template: any) {
        return this.http.post(`${environment.apiEndpoint}/formbuilder`, template);
    }
    updateTemplate(template: any, id: any) {
        return this.http.put(`${environment.apiEndpoint}/formbuilder/${id}`, template);
    }
    deleteTemplate(id: any) {
        return this.http.delete(`${environment.apiEndpoint}/formbuilder/${id}`);
    }
    getSavedTemplate() {
        return this.http.get(`${environment.apiEndpoint}/formbuilder`);
    }
}
