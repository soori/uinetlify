import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-template-stepper-view-cmp',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class StepperViewComponent implements OnInit {

  @Input() attributes: any = {};

  constructor() { }

  ngOnInit() {
  }

}
