import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-template-stepper-home-cmp',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class StepperHomeComponent implements OnInit {

  @Input() config: any = {};

  constructor() { }

  ngOnInit() {
  }

}
