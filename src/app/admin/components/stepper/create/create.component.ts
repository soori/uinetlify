import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { Location } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { FormViewComponent } from '../../form/view/view.component';
import { StepperService } from '../stepper.service';

@Component({
  selector: 'app-template-stepper-create-cmp',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class StepperCreateComponent implements OnInit {
  @Input() config: any = {};
  isLinear = false;
  isEditable = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isLoading = false;
  isFormValid = false;
  showView = false;
  stepperView: any = {};
  todoList: any;
  openConfig: any;
  templateConfig: any;
  stepperLayout: {
    details: any; stepperAttributes: any; isLinear: boolean;
    isEditable: boolean; isTemplateView: boolean; stepperView: any
  };
  formValue: any;
  formValues: any = [];
  multipleFormValue: any[] = [];
  setFormValues: any[] = [];
  stepperDoneItems: any[] = [];
  stepperTodoItems: any = [];
  templateId: string | null;
  @Input() cmpLayoutItems: any = [];
  constructor(private _formBuilder: FormBuilder, private service: StepperService,
    private router: Router, private route: ActivatedRoute, private fb: FormBuilder, private _messageService: MessageService,
    protected translate: TranslateService, protected store: Store<UserLanguageState>, private location: Location) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  steppers: any = [
    { id: 1, name: 'Horizontal' },
    { id: 2, name: 'Vertical' }
  ];
  selectedStepper: any = { id: 1, name: 'Horizontal' };
  stepperAttributes: any = [];
  formValidation: any = {};
  stepperFormValue: any = {
    hasMultipleForms: true,
    formValue: {}
  };
  @ViewChild(FormViewComponent, { static: false })
  private formViewCMp: FormViewComponent;
  stepperFormGroup: FormArray;
  ngOnInit() {
    this.stepperFormGroup = this.fb.array([]);
    this.stepperView = JSON.parse(localStorage.getItem('template') || 'null');
    this.stepperAttributes = [];
    // Page Builder view
    if (this.config.isPageBuilderView) {
      this.showView = true;
      this.stepperDoneItems = this.config.attributes;
      this.stepperDoneItems.forEach(item => {
        if (item.details.type.id !== 7) {
          const form = this.initFormGroup(item._id);
          this.stepperFormGroup.push(form);
        }
      });
      this.isLinear = this.config.isLinear;
      this.isEditable = this.config.isEditable;
      this.selectedStepper = this.config.stepperView;
      this.setFormValues = this.config.formValues;
    } else if (this.stepperView && this.stepperView.isTemplateView && this.stepperView.details.type.id === 7) {
      // on view click
      // this.showView = true;
      this.stepperDoneItems = this.stepperView.attributes;
      this.stepperDoneItems.forEach(item => {
        if (item.details.type.id !== 7) {
          const form = this.initFormGroup(item._id);
          this.stepperFormGroup.push(form);
        }
      });
      // TO-DO  refactor(merge)  duplicate code from pageBuilder and template view
      // this.config = this.stepperView.details;
      this.isLinear = this.stepperView.isLinear;
      this.isEditable = this.stepperView.isEditable;
      this.selectedStepper = this.stepperView.stepperView;
      this.setFormValues = this.stepperView.formValues;
      this.getPageItems();
    } else {
      // create view
      this.getPageItems();
    }
    this.templateId = this.route.snapshot.paramMap.get('id');
  }

  getPageItems() {
    this.isLoading = true;
    this.service.getSavedTemplate().subscribe(data => {
      this.isLoading = false;
      this.stepperAttributes = data;
      this.stepperAttributes.forEach((element: any) => {
        if (element.details.type.id !== 7) {
          element.hasMultipleForms = true;
          const form = this.initFormGroup(element._id);
          this.stepperFormGroup.push(form);
          this.stepperTodoItems.push(element);
        }
      });
    });
  }

  initFormGroup(name: any): FormGroup {
    name = this.fb.group({
    });
    return name;
  }
  onStepperNext(index: number) {
    this.formViewCMp.getFormValid();
  }

  getFormValue(value: any) {
    this.formValue = value;
  }
  getMultipleFormValue(value: any[]) {
    this.multipleFormValue = value;
  }
  getFormValid(isInvalid: Boolean) {
    this.isFormValid = !isInvalid;
  }

  checkFormValid() {
    return this.isFormValid;
  }

  submit() {
    this.formViewCMp.emitMultipleFormValue();
    this.isLoading = true;
    const stepperLayout = {
      details: this.config,
      attributes: this.stepperDoneItems,
      isLinear: this.isLinear,
      isEditable: this.isEditable,
      isTemplateView: false,
      stepperView: this.selectedStepper,
      formValues: this.multipleFormValue
    };
    this.service.savedTemplate(stepperLayout).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Stepper saved successfully');
      this.router.navigate(['./../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, Try again !');
        this.isLoading = false;
      });
  }
  onViewClick() {
    this.showView = true;
    this.stepperLayout = {
      details: this.config,
      stepperAttributes: this.stepperAttributes,
      isLinear: this.isLinear,
      isEditable: this.isEditable,
      isTemplateView: false,
      stepperView: this.selectedStepper
    };
  }


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      const doneCopy = this.createCopy(this.stepperDoneItems[event.currentIndex]);
      this.stepperDoneItems[event.currentIndex] = doneCopy;
      this.stepperDoneItems.forEach((item, i) => {
        item.position = i;
      });
    } else if (event.previousIndex !== event.currentIndex) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
    }
  }

  createCopy(orig: any) {
    return JSON.parse(JSON.stringify(orig));
  }
  resetDoneItems() {
    this.stepperDoneItems = [];
  }
  removeStep(stepIndex: number) {
    this.stepperDoneItems.splice(stepIndex, 1);
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
  update() {
    this.formViewCMp.emitMultipleFormValue();
    this.isLoading = true;
    const stepperLayout = {
      details: this.config,
      attributes: this.stepperDoneItems,
      isLinear: this.isLinear,
      isEditable: this.isEditable,
      isTemplateView: false,
      stepperView: this.selectedStepper,
      formValues: this.multipleFormValue
    };
    this.service.updateTemplate(stepperLayout, this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Stepper updated successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.info('Error occured, Try again !');
        this.isLoading = false;
      });
  }
  goBack() {
    this.location.back();
  }
  delete() {
    this.isLoading = true;
    this.service.deleteTemplate(this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Template deleted successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this.isLoading = false;
        this._messageService.warn('Error occures, Try again');
      });
  }
  
}
