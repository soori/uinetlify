import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { AppRouteService } from 'src/app/admin/route/route.service';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-template-pageaction-view-cmp',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class PageactionViewComponent implements OnInit {
  @Input() item: any;
  @Output() emitClick = new EventEmitter<any>();
  buttonData: any;
  butnTemplate: any;
  formStatus = false;

  constructor(protected translate: TranslateService, protected store: Store<UserLanguageState>,
    private service: AppRouteService) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.service.updateFormStatus.subscribe(data => {
      const status: string = data;
      if (status === 'INVALID') {
        this.formStatus = false;
      } else if (status === 'VALID') {
        this.formStatus = true;
      }
    });
    this.butnTemplate = this.item ? this.item : JSON.parse(localStorage.getItem('template') || 'null');
  }

  submit(item: any) {
    this.emitClick.emit(item);
  }
  navigation(item: any) {
    this.emitClick.emit(item);
  }

  reset(item: any) {
    this.emitClick.emit(item);
  }
}
