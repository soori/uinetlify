import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { ButtonRoutingModule } from './button-routing.module';
import { ButtonConfigComponent } from './config/button/button-config.component';
import { ButtonViewConfigurationDialogComponent } from './config/view/view-configuration.component';
import { PageactionCreateComponent } from './create/create.component';
import { PageactionViewComponent } from './view/view.component';

@NgModule({
  declarations: [PageactionCreateComponent, PageactionViewComponent, ButtonConfigComponent, ButtonViewConfigurationDialogComponent],
  imports: [
    CommonModule,
    ButtonRoutingModule,
    DragDropModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [PageactionCreateComponent, PageactionViewComponent],
  entryComponents: []
})
export class ButtonModule { }
