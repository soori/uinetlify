import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-button-config',
  templateUrl: './button-config.component.html',
  styleUrls: ['./button-config.component.scss']
})
export class ButtonConfigComponent implements OnInit {

  item: any;

  colors: any = [
    { id: 1, name: 'basic', display_name: 'Basic' },
    { id: 2, name: 'primary', display_name: 'Primary' },
    { id: 3, name: 'accent', display_name: 'Accent' },
    { id: 4, name: 'warn', display_name: 'Warn' }
  ];
  actions: any = [
    { id: 1, name: 'submit', display_name: 'Submit' },
    { id: 2, name: 'reset', display_name: 'Reset' },
    { id: 3, name: 'navigation', display_name: 'Navigation' },
  ];
  httpCalls: any[] = [
    { id: 1, name: 'get', display_name: 'Get' },
    { id: 2, name: 'post', display_name: 'Post' },
    { id: 3, name: 'put', display_name: 'Put' },
    { id: 4, name: 'delete', display_name: 'Delete' }
  ];

  constructor(public dialogRef: MatDialogRef<ButtonConfigComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.item = this.data.value;
  }
  saveFormField() {
    this.dialogRef.close(this.data.value);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
