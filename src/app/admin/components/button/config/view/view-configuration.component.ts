import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-view-configuration-dialog',
  templateUrl: './view-configuration.component.html',
  styleUrls: ['./view-configuration.component.scss']
})
export class ButtonViewConfigurationDialogComponent implements OnInit {

  selectedTemplate: any = {};

  mainAxis: any[] = [
    { id: 1, name: 'start', display_name: 'Start' },
    { id: 2, name: 'center', display_name: 'Center' },
    { id: 3, name: 'end', display_name: 'End' },
  ];
  crossAxis: any[] = [
    { id: 1, name: 'start', display_name: 'Start' },
    { id: 2, name: 'center', display_name: 'Center' },
    { id: 3, name: 'end', display_name: 'End' },
  ];

  directions: any[] = [
    { id: 1, name: 'row', display_name: 'Row' },
    { id: 2, name: 'column', display_name: 'Column' }
  ];

  options: any = {
    direction: '',
    mAxis: '',
    cAxis: '',
  };

  constructor(public dialogRef: MatDialogRef<ButtonViewConfigurationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.selectedTemplate = this.data;
    const layoutName = this.selectedTemplate.layout.name;
    const alignment: any[] = this.selectedTemplate.layout.alignment.split(' ');
    const sleep = alignment[0];
    const stand = alignment[1];
    this.options.mAxis = this.mainAxis.filter(hitem => {
      if (hitem.name === sleep) {
        return hitem;
      }
    });
    this.options.cAxis = this.crossAxis.filter(vitem => {
      if (vitem.name === stand) {
        return vitem;
      }
    });
    this.options.direction = this.directions.filter(dir => {
      if (dir.name === layoutName) {
        return dir;
      }
    });
    this.options.mAxis = this.options.mAxis[0];
    this.options.cAxis = this.options.cAxis[0];
    this.options.direction = this.options.direction[0];
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
  saveFormField() {
    const buttonData = {
      name: this.options.direction.name,
      alignment: `${this.options.mAxis.name} ${this.options.cAxis.name}`
    };
    this.dialogRef.close(buttonData);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
