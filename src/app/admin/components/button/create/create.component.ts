import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { ButtonService } from '../button.service';
import { ButtonConfigComponent } from '../config/button/button-config.component';
import { ButtonViewConfigurationDialogComponent } from '../config/view/view-configuration.component';

@Component({
  selector: 'app-template-pageaction-create-cmp',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class PageactionCreateComponent implements OnInit {
  @Input() config: any = {
    name: '',
    type: { id: 5, name: 'Button' }
  };
  isLoading: Boolean = false;
  @Output() viewMode = new EventEmitter<boolean>();
  buttons = [
    {
      id: 1,
      style: 'Basic Button',
      color: 'primary',
      label: 'Basic',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
    {
      id: 2,
      style: 'Raised Button',
      color: 'basic',
      label: 'Raised',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
    {
      id: 3,
      style: 'Stroked Button',
      color: 'accent',
      label: 'Stroked',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
    {
      id: 4,
      style: 'Flat Button',
      color: 'warn',
      label: 'Flat',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
  ];
  buttonsDone: any = [];
  layout: any = {
    name: 'row',
    alignment: 'start start'
  };
  templateId: string | null;
  @Input() cmpLayoutItems: any = [];
  showCreate: Boolean = true;
  constructor(public dialog: MatDialog, private router: Router, private route: ActivatedRoute,
    private service: ButtonService, private _messageService: MessageService, private location: Location,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.templateId = this.route.snapshot.paramMap.get('id');
    if (this.cmpLayoutItems.length > 0) {
      this.buttonsDone = this.cmpLayoutItems;
      this.layout = this.config.layout;
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      const doneCopy = this.createCopy(this.buttonsDone[event.currentIndex]);
      this.buttonsDone[event.currentIndex] = doneCopy;
      this.buttonsDone.forEach((item: any, i: any) => {
        item.position = i;
      });
    } else if (event.previousIndex !== event.currentIndex) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
    }
  }

  createCopy(orig: any) {
    return JSON.parse(JSON.stringify(orig));
  }

  removeButton(j: number) {
    this.buttonsDone.splice(j, 1);
  }
  openEditDialog(item: any) {
    const dialogRef = this.dialog.open(ButtonConfigComponent, {
      width: '100vw',
      data: { value: JSON.parse(JSON.stringify(item)) },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        item = result;
        this.buttonsDone[item.position] = item;
      }
    });
  }

  openConfig(selectedTemplate: any) {
    selectedTemplate.layout = {
      name: this.layout.name,
      alignment: this.layout.alignment
    };
    const dialogRef = this.dialog.open(ButtonViewConfigurationDialogComponent, {
      width: '100vw',
      data: JSON.parse(JSON.stringify(selectedTemplate)),
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.layout = result;
      }
    });
  }

  submitButtons() {
    this.isLoading = true;
    this.config.layout = this.layout;
    const buttonData = {
      details: this.config,
      attributes: this.buttonsDone,
      layout: this.layout
    };
    this.service.saveTemplate(buttonData).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Buttons saved successfully');
      this.router.navigate(['./../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.info('Error occured, Try again !');
        this.isLoading = false;
      });
  }

  onViewClick() {
    this.showCreate = false;
    localStorage.removeItem('template');
    const buttonData = {
      details: this.config,
      attributes: this.buttonsDone,
      layout: this.layout
    };
    localStorage.setItem('template', JSON.stringify(buttonData));
  }

  // TO-DO write save and update into single call
  updateButtons() {
    this.isLoading = true;
    this.config.layout = this.layout;
    const buttonData = {
      details: this.config,
      attributes: this.buttonsDone,
      layout: this.layout
    };
    this.service.updateTemplate(buttonData, this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Buttons updated successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, Try again !');
        this.isLoading = false;
      });
  }
  goBack() {
    this.location.back();
  }
  delete() {
    this.isLoading = true;
    this.service.deleteTemplate(this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Template deleted successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this.isLoading = false;
        this._messageService.warn('Error occures, Try again');
      });
  }
  
}
