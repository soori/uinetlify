import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class ButtonService {
    constructor(private http: HttpClient) { }

    updateTemplate(template: any, id: any) {
        return this.http.put(`${environment.apiEndpoint}/formbuilder/${id}`, template);
    }

    saveTemplate(template: any) {
        return this.http.post(`${environment.apiEndpoint}/formbuilder`, template);
    }

    deleteTemplate(id: any) {
        return this.http.delete(`${environment.apiEndpoint}/formbuilder/${id}`);
    }


}
