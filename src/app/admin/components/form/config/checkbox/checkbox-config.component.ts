import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { MediaObserver } from '@angular/flex-layout';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { MatSelectChange } from '@angular/material/select';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-checkbox-config',
  templateUrl: './checkbox-config.component.html',
  styleUrls: ['./checkbox-config.component.scss']
})
export class CheckBoxConfigComponent implements OnInit {

  item: any;

  displayTypes: any[] = [
    { id: 1, name: 'Normal', display_name: 'Normal' },
    { id: 2, name: 'Readonly', display_name: 'Read Only' },
    { id: 3, name: 'Label', display_name: 'Label' },
  ];
  visibility: any[] = [
    { id: 0, type: 'true', display_name: 'Yes' },
    { id: 1, type: 'false', display_name: 'No' },
    { id: 2, type: 'Permission code', display_name: 'Permission code' },
  ];
  dataTypes: any[] = [
    { id: 1, type: 'text', display_name: 'String' },
    { id: 2, type: 'number', display_name: 'Number' },
  ];
  selections: any[] = [
    { id: 1, value: 'single selection', display_name: 'Is Single Selection' },
    { id: 2, value: 'multi selection', display_name: 'Is Multiple Selection' },
  ];
  endPoints: any[] = [
    { id: 1, value: 'manualdata', display_name: 'Manualdata' },
    { id: 2, value: 'api', display_name: 'API Data' },
  ];
  responsiveBreakPoints: any = [
    { id: 1, breakPoint: 'xs', mediaQuery: 'max-width: \'599px\'' },
    { id: 2, breakPoint: 'sm', mediaQuery: 'min-width: 600px and max-width: 959px' },
    { id: 3, breakPoint: 'md', mediaQuery: 'min-width: 960px and max-width: 1279px' },
    { id: 4, breakPoint: 'lg', mediaQuery: 'min-width: 1280px and max-width: 1919px' },
    { id: 5, breakPoint: 'xl', mediaQuery: 'min-width: 1920px and max-width: 5000px' },
    { id: 6, breakPoint: 'lt-sm', mediaQuery: 'max-width: 599px' },
    { id: 7, breakPoint: 'lt-md', mediaQuery: 'max-width: 959px' },
    { id: 8, breakPoint: 'lt-lg', mediaQuery: 'max-width: 1279px' },
    { id: 9, breakPoint: 'lt-xl', mediaQuery: 'max-width: 1919px' },
    { id: 10, breakPoint: 'gt-xs', mediaQuery: 'min-width: 600px' },
    { id: 11, breakPoint: 'gt-sm', mediaQuery: 'min-width: 960px' },
    { id: 12, breakPoint: 'gt-md', mediaQuery: 'min-width: 1280px' },
    { id: 13, breakPoint: 'gt-lg', mediaQuery: 'min-width: 1920px' }
  ];
  formField: FormGroup;
  hasOptions: Boolean = false;
  dataChange: any = {
    endPoint: { id: 1, value: 'manualdata', display_name: 'Manualdata' },
  };
  selectionCheckbox: any;
  fieldModelNames: any = [];

  constructor(public dialogRef: MatDialogRef<CheckBoxConfigComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder, protected translate: TranslateService, protected store: Store<UserLanguageState>,
    public media: MediaObserver) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit(): void {
    this.item = this.data.item;
    this.processModelNames(this.data.done);
    this.formField = this.formBuilder.group({
      options: this.formBuilder.array([
      ])
    });
    if (this.item.options) {
      this.setExistingoptions();
    }
    if (this.item) {
      this.hasOptions = true;
    } else {
      this.hasOptions = false;
    }
  }
  initOption() {
    return this.formBuilder.group({
      display_name: ['', Validators.required],
      id: ['', Validators.required]
    });
  }
  pushOption(element: any) {
    return this.formBuilder.group({
      display_name: [element.display_name],
      id: [element.id]
    });
  }
  setExistingoptions() {
    const control = <FormArray>this.formField.controls['options'];
    this.item.options.forEach(element => {
      control.push(this.pushOption(element));
    });
  }
  addOption() {
    const control = <FormArray>this.formField.controls['options'];
    control.push(this.initOption());
  }
  removeOption(i: number) {
    const control = <FormArray>this.formField.controls['options'];
    control.removeAt(i);
  }
  onNoClick() {
    this.dialogRef.close();
  }
  saveFormField() {
    if (this.formField.controls.options.value.length > 0) {
      this.item.options = this.formField.controls.options.value;
    }
    this.dialogRef.close(this.item);
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
  onVisibiltyChange(event: MatSelectChange) {
    const value = event.value;

    if (value.id === 1) {
      this.item.hasDeviceSpecificDisplay = false;
      this.item.isActiveAt = { id: 10, breakPoint: 'gt-xs', mediaQuery: 'min-width: 600px' };
    }
  }

  processModelNames(items: Array<any>) {
    items.forEach((item) => item.modelName && (item.position !== this.item.position) ?
      this.fieldModelNames.push({ id: item.position, modelName: item.modelName }) : null);
  }
  onDerviedFromChange(event: MatSelectChange) {
    const value = event.value;
    if (!value) {
      this.item.derivedFrom = {};
    }
  }
}
