import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';

@Component({
  selector: 'app-view-configuration-dialog',
  templateUrl: './view-configuration.component.html',
  styleUrls: ['./view-configuration.component.scss']
})
export class ViewConfigurationDialogComponent implements OnInit {

  selectedTemplate: any = {};
  yesNo: any[] = [
    { id: 0, name: 'Yes' },
    { id: 1, name: 'No' }
  ];
  displayErrorMessages: any[] = [
    { id: 0, name: 'Inline' },
    { id: 1, name: 'Snackbar' }
  ];
  formTemplateNature: any[] = [
    { id: 0, name: 'Static' },
    { id: 1, name: 'Dynamic' }
  ];
  layouts: any[] = [
    { id: 0, name: 'row wrap', dispalyName: 'Row' },
    { id: 1, name: 'column', dispalyName: 'Column' }
  ];
  layoutColumns: any[] = [
    { id: 0, name: 'One Column', flex: '1 1 100', flexGap: '10px' },
    { id: 1, name: 'Two Column', flex: '1 1 47', flexGap: '20px' },
    { id: 2, name: 'Three Column', flex: '1 1 29', flexGap: '21px' },
    { id: 3, name: 'Four Column', flex: '1 1 18', flexGap: '40px' },
  ];

  formFieldStyles: any[] = [
    { id: 0, name: 'legacy', display_name: 'Legacy' },
    { id: 1, name: 'outline', display_name: 'Outline' },
    { id: 2, name: 'standard', display_name: 'Standard' },
    { id: 3, name: 'fill', display_name: 'Fill' }
  ];

  mainAxis: any[] = [
    { id: 1, name: 'start', display_name: 'Start' },
    { id: 2, name: 'center', display_name: 'Center' },
    { id: 3, name: 'end', display_name: 'End' },
  ];
  crossAxis: any[] = [
    { id: 1, name: 'start', display_name: 'Start' },
    { id: 2, name: 'center', display_name: 'Center' },
    { id: 3, name: 'end', display_name: 'End' },
  ];

  directions: any[] = [
    { id: 1, name: 'row', display_name: 'Row' },
    { id: 2, name: 'column', display_name: 'Column' }
  ];
  formAlign: any[] = [
    { id: 1, order: 0, display_name: 'Top' },
    { id: 2, order: 2, display_name: 'Bottom' }
  ];
  options: any = {
    direction: '',
    mAxis: '',
    cAxis: '',
  };

  constructor(public dialogRef: MatDialogRef<ViewConfigurationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.selectedTemplate = this.data;
    if (this.selectedTemplate.config === 3) {
      const layoutName = this.selectedTemplate.layout.name;
      const alignment: any[] = this.selectedTemplate.layout.alignment.split(' ');
      const sleep = alignment[0];
      const stand = alignment[1];
      this.options.mAxis = this.mainAxis.filter(hitem => {
        if (hitem.name === sleep) {
          return hitem;
        }
      });
      this.options.cAxis = this.crossAxis.filter(vitem => {
        if (vitem.name === stand) {
          return vitem;
        }
      });
      this.options.direction = this.directions.filter(dir => {
        if (dir.name === layoutName) {
          return dir;
        }
      });
      this.options.mAxis = this.options.mAxis[0];
      this.options.cAxis = this.options.cAxis[0];
      this.options.direction = this.options.direction[0];
    }
  }

  onResponsiveChange() {
    if (this.selectedTemplate.isResponsive.id === 0) {
      this.selectedTemplate.layout = { id: 0, name: 'row wrap', dispalyName: 'Row' };
      this.selectedTemplate.layoutColumn = { id: 0, name: 'One Column', flex: '1 1 auto', flexGap: '10px' };
    }
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
  saveFormField() {
    if (this.selectedTemplate.type.id === 5) {
      const buttonData = {
        name: this.options.direction.name,
        alignment: `${this.options.mAxis.name} ${this.options.cAxis.name}`
      };
      this.dialogRef.close(buttonData);
    } else {
      this.dialogRef.close(this.selectedTemplate);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
