import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormService } from '../form.service';
import { FieldBase } from '../model/base';

@Component({
  selector: 'app-template-form-view-cmp',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class FormViewComponent implements OnInit {
  @Input() item: any;
  @Input() mobileLayout: any;
  @Input() multipleForm: any = [];
  @Input() formIndex: number;
  @Input() pageFormValue: any = {};
  @Output() formValue = new EventEmitter<any>();
  @Output() isFormInvalid = new EventEmitter<boolean>();
  @Output() multipleForms: EventEmitter<any[]> = new EventEmitter<any[]>();
  formData: any;
  fields: FieldBase<any>[] = [];
  basicForm: FormGroup;
  basicFields: FieldBase<any>[] = [];
  layout: any;
  template: any[] = [];
  multipleFormValues: any[] = [];
  buttonData: any;
  constructor(private formService: FormService, private fb: FormBuilder, public media: MediaObserver) { }

  ngOnInit() {
    this.basicForm = this.fb.group({
    });

    if (this.item) {
      this.formData = this.item;
    } else {
      const formTemplate: any = JSON.parse(localStorage.getItem('template') || 'null');
      this.formData = formTemplate.formData;
      this.buttonData = formTemplate.buttonData;
    }

    // form  groups & controls,...
    //  pass input app-formfield-display-cmp
    if (this.formData) {
      if (this.formData.layout) {
        this.layout = this.formData.layout;
      }
      this.fields = this.formData.attributes;
      // to remove duplicates
      const filteredFields = this.fields.reduce((arr: any[], item: any) => {
        const exists = !!arr.find((x: any) => x.modelName === item.modelName);
        if (!exists) {
          arr.push(item);
        }
        return arr;
      }, []);
      this.formData.attributes = filteredFields;
      this.template = this.formData.details;
      for (const field of filteredFields) {
        if ((field.modelName || field.id === 7) && field.securityAttrributes.visibility.id !== 1) {
          field.pathSegments = field.modelName.split('.');
          field.isSetValue = false;
          if (this.multipleForm) {
            // to set value to fields(which is used when viewed to disaply value)
            field.isSetValue = true;
          }
          if (this.pageFormValue.length > 0) {
            // When page viewed
            field.isSetValue = true;
            this.pageFormValue.forEach((item: any) => {
              if (item.modelName === field.modelName) {
                field.value = item.value;
              }
            });
          } else {
            if (field.value) {
              // show deafult value in field
              field.isSetValue = true;
            } else {
              field.isSetValue = false;
              field.value = null;
            }
          }
          this.basicFields.push(field);
        }
        // For multiple forms used in Tab, stepper, etc..
        if (this.multipleForm) {
          this.multipleForm[this.formIndex] = this.formService.toFormGroup(this.basicFields, this.basicForm);
        } else {
          this.basicForm = this.formService.toFormGroup(this.basicFields, this.basicForm);
        }
      }
    }

    if (this.mobileLayout && this.mobileLayout.deviceType &&
      (this.mobileLayout.deviceType.id === 3 || this.mobileLayout.deviceType.id === 4) && this.layout) {
      this.layout.name = 'column';
      this.layout.flexGap = '0';
    }
  }

  emitMultipleFormValue() {
    this.multipleForm.forEach((item: any, i: any) => {
      this.multipleFormValues.push(item.value);

    });
    this.multipleForms.emit(this.multipleFormValues);
  }
  getFormValue() {
    this.formValue.emit(this.basicForm.value);
  }
  getFormValid() {
    this.isFormInvalid.emit(this.multipleForm[this.formIndex].invalid);
  }
  resetFormValue() {
    this.basicForm.reset();
  }
}
