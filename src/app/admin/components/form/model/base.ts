export class FieldBase<T> {
    value: T;
    key: string;
    label: string;
    display_name: string;
    required: boolean;
    pattern: string;
    order: number;
    dataType: { id: any, type: any, display_name: string };
    controlType: string;
    isActiveAt: any;
    minlength: number | null;
    maxlength: number | null;
    options: { display_name: any, id: string, isChecked: boolean }[];
    errorMessages: {
        required:string,
        minlength: string,
        maxlength: string,
        invalid: string
    };
    modelName: string;
    minDate: any;
    maxDate: any;
    pathSegments: any[];
    hasFormGroup: boolean;
    groupName: string;
    isSetValue: boolean;
    securityAttrributes: {
        displayType: { id: any, name: string, display_name: string },

    };
    isDerived: boolean;
    derivedFrom: { id: string, modelName: string };
    showHideAction: boolean;

    constructor(options: {
        value?: any,
        key?: string,
        label?: string,
        display_name? : string;
        dataType?: { id: any, type: any, display_name: string };
        required?: boolean,
        order?: number,
        minlength?: number,
        maxlength?: number,
        pattern?: string,
        controlType?: string,
        isActiveAt?: any,
        options?: { display_name: any, id: string, isChecked: boolean }[],
        errorMessages?: {
            required: string,
            minlength: string,
            maxlength: string,
            invalid: string
        },
        modelName?: string,
        minDate?: any;
        maxDate?: any;
        pathSegments?: any[],
        hasFormGroup?: boolean,
        groupName?: string,
        isSetValue?: boolean,
        securityAttrributes?: {
            displayType: { id: any, name: string, display_name: string },

        },
        isDerived?: boolean,
        derivedFrom?: { id: string, modelName: string },
        showHideAction?: boolean
    } = {}) {
        this.value = options.value || undefined;
        this.key = options.key || '';
        this.label = options.label || '';
        this.dataType=options.dataType||{ id: '', type: '', display_name: ''};
        this.display_name = options.display_name || '';
        this.required = !!options.required;
        this.pattern = options.pattern || '';
        this.order = options.order === undefined ? 1 : options.order;
        this.minlength = options.minlength || null;
        this.maxlength = options.maxlength || null;
        this.controlType = options.controlType || '';
        this.isActiveAt = options.isActiveAt || '';
        this.options = options.options || [];
        this.errorMessages = options.errorMessages || {
            required: '',
            minlength: '',
            maxlength: '',
            invalid: ''
        };
        this.modelName = options.modelName || '';
        this.minDate = options.minDate || '';
        this.maxDate = options.maxDate || '';
        this.pathSegments = options.pathSegments || [];
        this.hasFormGroup = !!options.hasFormGroup;
        this.groupName = options.groupName || '';
        this.securityAttrributes = options.securityAttrributes || {
                    displayType: { id:'', name: '', display_name: '' },

        };
        this.isSetValue = !!options.isSetValue;
        this.isDerived = !!options.isDerived;
        this.derivedFrom = options.derivedFrom || { id: '', modelName: '' };
        this.showHideAction = !!options.showHideAction;
    }
}
