import { SelectConfigComponent } from './../config/select/select-config.component';
import { TextAreaConfigComponent } from './../config/textarea/textarea-config.component';
import { LableConfigComponent } from './../config/lable/lable-config.component';
import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { DatePickerConfigComponent } from '../config/datepicker/datepicker-config.component';
import { TextBoxConfigComponent } from '../config/textbox/textbox-config.component';
import { ViewConfigurationDialogComponent } from '../config/view/view-configuration.component';
import { FormService } from '../form.service';
import { ButtonViewConfigurationDialogComponent } from '../../button/config/view/view-configuration.component';
import { ButtonConfigComponent } from '../../button/config/button/button-config.component';
import { CheckBoxConfigComponent } from '../config/checkbox/checkbox-config.component';
import { RadioButtonConfigComponent } from '../config/radiobutton/radiobutton-config.component';

@Component({
  selector: 'app-template-form-create-cmp',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})

export class FormCreateComponent implements OnInit {
  @Input() config: any = {};
  @Input() cmpLayoutItems: any = [];
  isLoading: Boolean = false;
  formControlsTODO: any[] = [
    {
      id: 1,
      key: 'text',
      label: 'Placeholder',
      icon: 'sort_by_alpha',
      options: [],
      required: true,
      order: 3,
      value: null,
      pattern: null,
      minlength: '',
      maxlength: '',
      errorMessages: {
        required: 'Text is required',
        minlength: 'Text minlength error',
        maxlength: 'Text maxlength error',
        invalid: 'Invalid text'
      },
      dataType: { id: 1, type: 'text', display_name: 'String' },
      controlType: 'textbox',
      modelName: '',
      pathSegments: [],
      hasFormGroup: false,
      groupName: '',
      securityAttrributes: {
        visibility: { id: 0, type: 'true', display_name: 'Yes' },
        displayType: { id: 1, name: 'Normal', display_name: 'Normal' },
        allowOverride: false,
        encryptData: true
      },
      isActiveAt: { id: 10, breakPoint: 'gt-xs', mediaQuery: 'min-width: 600px' },
      showHideAction: false
    },
    {
      id: 6,
      key: 'Date',
      label: 'Date',
      value: '',
      required: true,
      minDate: '',
      maxDate: '',
      pattern: null,
      icon: 'date_range',
      errorMessages: {
        required: 'Date required',
        minlength: 'Minimum Date error message Required ',
        maxlength: 'Maximum Date error message Required',
        invalid: 'Invalid Date'
      },
      minlength: null,
      maxlength: '9999',
      order: 6,
      controlType: 'datepicker',
      modelName: '',
      pathSegments: [],
      hasFormGroup: false,
      dataType: { id: 1, type: 'text', display_name: 'String' },
      groupName: '',
      securityAttrributes: {
        visibility: { id: 0, type: 'true', display_name: 'Yes' },
        displayType: { id: 1, name: 'Normal', display_name: 'Normal' },
        allowOverride: false,
        encryptData: true
      },
      isActiveAt: { id: 10, breakPoint: 'gt-xs', mediaQuery: 'min-width: 600px' },
    },
    {
      id: 2,
      key: 'textArea',
      label: 'Placeholder',
      icon: 'insert_comment',
      value: '',
      options: [],
      required: false,
      pattern: null,
      minlength: null,
      maxlength: 9999999,
      order: 100,
      errorMessages: {},
      controlType: 'textarea',
      modelName: '',
      pathSegments: [],
      hasFormGroup: false,
      dataType: { id: 1, type: 'text', display_name: 'String' },
      groupName: '',
      securityAttrributes: {
        visibility: '',
        displayType: '',
        allowOverride: false,
        encryptData: true
      },
      isActiveAt: { id: 10, breakPoint: 'gt-xs', mediaQuery: 'min-width: 600px' },
      showHideAction: false
    },
    {
      id: 3,
      key: 'checkbox',
      controlType: 'checkbox',
      label: 'Checkbox label',
      icon: 'check_circle',
      options: [
        {
          display_name: 'Option 1',
          id: 'option-1',
          isChecked: false
        },
        {
          display_name: 'Option 2',
          id: 'option-2',
          isChecked: false
        }, {
          display_name: 'Option 3',
          id: 'option-3',
          isChecked: false
        },
        {
          display_name: 'Option 4',
          id: 'option-4',
          isChecked: false
        }
      ],
      endPoint: '',
      value: '',
      required: true,
      pattern: null,
      errorMessages: {
        required: 'Value is required',
        minlength: '',
        maxlength: '',
        invalid: ''
      },
      minlength: null,
      maxlength: null,
      order: 10,
      modelName: '',
      pathSegments: [],
      hasFormGroup: false,
      dataType: { id: 1, type: 'text', display_name: 'String' },
      groupName: '',
      securityAttrributes: {
        visibility: { id: 0, type: 'true', display_name: 'Yes' },
        displayType: { id: 1, name: 'Normal', display_name: 'Normal' },
        allowOverride: false,
        encryptData: true
      },
      isActiveAt: { id: 10, breakPoint: 'gt-xs', mediaQuery: 'min-width: 600px' },
      showHideAction: false,
      selectionType: 1,
    },
    {
      id: 4,
      controlType: 'radiobutton',
      key: 'radioButton',
      label: 'Radio button label',
      icon: 'radio_button_checked',
      options: [
        {
          display_name: 'Option 1',
          id: 'option-1',
          isChecked: false
        },
        {
          display_name: 'Option 2',
          id: 'option-2',
          isChecked: false
        }
      ],
      endPoint: '',
      value: '',
      required: false,
      pattern: null,
      errorMessages: {
        required: 'Value is required',
      },
      minlength: null,
      maxlength: null,
      order: 5,
      modelName: '',
      pathSegments: [],
      hasFormGroup: false,
      dataType: { id: 1, type: 'text', display_name: 'String' },
      groupName: '',
      securityAttrributes: {
        visibility: { id: 0, type: 'true', display_name: 'Yes' },
        displayType: { id: 1, name: 'Normal', display_name: 'Normal' },
        allowOverride: false,
        encryptData: true
      },
      isActiveAt: { id: 10, breakPoint: 'gt-xs', mediaQuery: 'min-width: 600px' },
      showHideAction: false
    },
    {
      id: 5,
      controlType: 'dropdown',
      key: 'dropDown',
      label: 'Drop down label',
      icon: 'arrow_drop_down_circle',
      options: [
        {
          display_name: 'Option 1',
          id: 'option-1',
          isChecked: false
        },
        {
          display_name: 'Option 2',
          id: 'option-2',
          isChecked: false
        }, {
          display_name: 'Option 3',
          id: 'option-3',
          isChecked: false
        },
        {
          display_name: 'Option 4',
          id: 'option-4',
          isChecked: false
        }
      ],
      endPoint: '',
      required: true,
      order: 3,
      pattern: null,
      minlength: null,
      maxlength: 9999,
      errorMessages: {
        required: 'Value is required',
        minlength: '',
        maxlength: ''
      },
      value: '',
      modelName: '',
      pathSegments: [],
      hasFormGroup: false,
      dataType: { id: 1, type: 'text', display_name: 'String' },
      groupName: '',
      securityAttrributes: {
        visibility: { id: 0, type: 'true', display_name: 'Yes' },
        displayType: { id: 1, name: 'Normal', display_name: 'Normal' },
        allowOverride: false,
        encryptData: true
      },
      isActiveAt: { id: 10, breakPoint: 'gt-xs', mediaQuery: 'min-width: 600px' },
      showHideAction: false,
      selectionType: 1,
    },
    {
      id: 7,
      key: 'label',
      label: 'Enter Label value',
      icon: 'sort_by_alpha',
      options: [],
      required: true,
      order: 3,
      value: null,
      pattern: null,
      minlength: 3,
      maxlength: 9,
      errorMessages: {
        required: 'Text is required',
        minlength: 'Text minlength error',
        maxlength: 'Text maxlength error',
        invalid: 'Invalid text'
      },
      dataType: { id: 1, type: 'text', display_name: 'String' },
      controlType: 'label',
      modelName: '',
      pathSegments: [],
      hasFormGroup: false,
      groupName: '',
      securityAttrributes: {
        visibility: { id: 0, type: 'true', display_name: 'Yes' },
        displayType: { id: 1, name: 'Normal', display_name: 'Normal' },
        allowOverride: false,
        encryptData: true
      },
      isActiveAt: { id: 10, breakPoint: 'gt-xs', mediaQuery: 'min-width: 600px' },
      fontStyle: {
        fontSize: 14,
        fontWeight: 'initial',
      }
    }
  ];
  done: any[] = [];
  // form var ends
  isSubmitted: Boolean = false;
  templateConfig: any = {
    type: { id: 1, name: 'Form' },
    name: '',
    templateNature: { id: 0, name: 'Static' },
    fieldStyle: { id: 1, name: 'outline', display_name: 'Outline' },
    errorMessages: { id: 0, name: 'Inline' },
    isResponsive: { id: 1, name: 'No' },
    layout: { id: 0, name: 'row wrap', dispalyName: 'Row' },
    layoutColumn: { id: 0, name: 'One Column', flex: '1 1 100', flexGap: '10px' },
    flexOrder: { id: 1, order: 0, display_name: 'Top' }
  };
  endPoint: String = '';
  endpointData: Object;
  showCreateView: Boolean = false;
  templateId: string | null;
  dialogRef: any;


  buttons = [
    {
      id: 1,
      style: 'Basic Button',
      color: 'primary',
      label: 'Basic',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
    {
      id: 2,
      style: 'Raised Button',
      color: 'basic',
      label: 'Raised',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
    {
      id: 3,
      style: 'Stroked Button',
      color: 'accent',
      label: 'Stroked',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
    {
      id: 4,
      style: 'Flat Button',
      color: 'warn',
      label: 'Flat',
      type: '',
      url: '',
      endPoint: '',
      httpCall: {},
      permissionCode: ''
    },
  ];
  buttonsDone: any = [];
  buttonConfig: any = {
    name: 'row',
    alignment: 'start start'
  };
  selection: any;
  selected: any = -1;
  constructor(public dialog: MatDialog, private router: Router, private route: ActivatedRoute,
    private service: FormService, private _messageService: MessageService, private location: Location,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  ngOnInit() {
    this.templateId = this.route.snapshot.paramMap.get('id');
    if (this.cmpLayoutItems.length > 0) {
      this.done = this.cmpLayoutItems;
      this.templateConfig = this.config;
    }
  }

  onViewClick() {
    this.showCreateView = true;
    localStorage.removeItem('template');
    const formData = {
      attributes: this.done,
      layout: {
        name: this.templateConfig.layout.name,
        flex: this.templateConfig.layoutColumn.flex,
        flexGap: this.templateConfig.layoutColumn.flexGap,
        flexAlign: this.templateConfig.layout.id === 0 ? 'start start' : 'stretch',
        column: this.templateConfig.layoutColumn.name,
        flexOrder: this.templateConfig.flexOrder
      },
      details: this.templateConfig,
    };
    const buttonData = {
      attributes: this.buttonsDone,
      layout: this.buttonConfig
    };
    localStorage.setItem('template', JSON.stringify({ formData: formData, buttonData: buttonData }));
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      const doneCopy = this.createCopy(this.done[event.currentIndex]);
      this.done[event.currentIndex] = doneCopy;
      if (doneCopy.controlType === 'dropdown') {
        if (doneCopy.endPoint) {
          doneCopy.options = this.getData(doneCopy.endPoint);
        }
      }
      this.done.forEach((item, i) => {
        item.position = i;
      });
    } else if (event.previousIndex !== event.currentIndex) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
    }
  }

  createCopy(orig: any) {
    return JSON.parse(JSON.stringify(orig));
  }
  removeItem(i: number) {
    this.done.splice(i, 1);
  }

  openConfig(selectedTemplate: any, i: number) {
    selectedTemplate.config = i;
    const dialogRef = this.dialog.open(ViewConfigurationDialogComponent, {
      width: '100vw',
      data: JSON.parse(JSON.stringify(selectedTemplate)),
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.templateConfig = result;
      }
    });
  }
  openEditDialog(item: any, i: number) {
    item = JSON.parse(JSON.stringify(item));
    const done = JSON.parse(JSON.stringify(this.done));
    if (item.id === 1) {
      this.dialogRef = this.dialog.open(TextBoxConfigComponent, {
        height: '85vh',
        width: '100vw',
        data: { item: item, done: done },
        disableClose: true
      });
    } else if (item.id === 2) {
      this.dialogRef = this.dialog.open(TextAreaConfigComponent, {
        height: '85vh',
        width: '100vw',
        data: { item: item, done: done },
        disableClose: true
      });
    } else if (item.id === 3) {
      this.dialogRef = this.dialog.open(CheckBoxConfigComponent, {
        height: '85vh',
        width: '100vw',
        data: { item: item, done: done },
        disableClose: true
      });
    } else if (item.id === 4) {
      this.dialogRef = this.dialog.open(RadioButtonConfigComponent, {
        height: '85vh',
        width: '100vw',
        data: { item: item, done: done },
        disableClose: true
      });
    } else if (item.id === 5) {
      this.dialogRef = this.dialog.open(SelectConfigComponent, {
        height: '85vh',
        width: '100vw',
        data: { item: item, done: done },
        disableClose: true
      });
    } else if (item.id === 6) {
      this.dialogRef = this.dialog.open(DatePickerConfigComponent, {
        height: '85vh',
        width: '100vw',
        data: { item: item, done: done },
        disableClose: true
      });
    } else if (item.id === 7) {
      this.dialogRef = this.dialog.open(LableConfigComponent, {
        height: '85vh',
        width: '100vw',
        data: { item: item, done: done },
        disableClose: true
      });
    }
    this.dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        item = result;
        if ((item.id === 5 && item.endPoint) || (item.id === 4 && item.endPoint) || (item.id === 3 && item.endPoint)) {
          this.service.getData(item.endPoint).subscribe(data => {
            if (Array.isArray(data)) {
              item.options = data;
            } else {
              this._messageService.warn('Error occured');
            }
          },
            error => {
              this._messageService.warn('Error occured');
            }
          );
        }
        this.done[i] = item;
      }
    });
  }
  submit() {
    const formDataToSave = this.processFormData();
    if (formDataToSave) {
      localStorage.setItem('dynamicFormFields', JSON.stringify(formDataToSave));
      this.service.saveTemplate(formDataToSave).subscribe(data => {
        this.isLoading = false;
        this._messageService.info('Form saved successfully');
        this.router.navigate(['./../home'], { relativeTo: this.route });
      },
        err => {
          this.isLoading = false;
          this._messageService.warn('Error occured, Try again');
        });
    } else {
      this._messageService.warn('Invalid Form Data');
    }
  }
  processFormData() {
    this.isLoading = true;
    localStorage.removeItem('dynamicFormFields');
    this.isSubmitted = !this.isSubmitted;
    this.templateConfig.name = this.config.name;
    const dynmaicFormFields = {
      attributes: this.done,
      layout: {
        name: this.templateConfig.layout.name,
        flex: this.templateConfig.layoutColumn.flex,
        flexGap: this.templateConfig.layoutColumn.flexGap,
        flexAlign: this.templateConfig.layout.id === 0 ? 'start start' : 'stretch',
        column: this.templateConfig.layoutColumn.name
      },
      details: this.templateConfig,
    };
    return dynmaicFormFields;
  }
  getData(endPoint) {
    this.service.getData(endPoint).subscribe(data => {
      this.endpointData = data;
    });
    return this.endpointData;
  }

  update() {
    const formDataToSave = this.processFormData();
    if (formDataToSave) {
      localStorage.setItem('dynamicFormFields', JSON.stringify(formDataToSave));
      this.service.updateTemplate(formDataToSave, this.templateId).subscribe(data => {
        this.isLoading = false;
        this._messageService.info('Form updated successfully');
        this.router.navigate(['./../../home'], { relativeTo: this.route });
      },
        err => {
          this._messageService.warn('Error occured, Try again !');
        });
    } else {
      this._messageService.warn('Invalid Form Data');
    }
  }
  goBack() {
    this.location.back();
  }
  delete() {
    this.isLoading = true;
    this.service.deleteTemplate(this.templateId).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Template deleted successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this.isLoading = false;
        this._messageService.warn('Error occures, Try again');
      });
  }
  dropButton(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      const doneCopy = this.createCopy(this.buttonsDone[event.currentIndex]);
      this.buttonsDone[event.currentIndex] = doneCopy;
      this.buttonsDone.forEach((item: any, i: any) => {
        item.position = i;
      });
    } else if (event.previousIndex !== event.currentIndex) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
    }
  }
  openButtonConfig(config: any) {
    const selectedTemplate: any = { layout: {} };
    selectedTemplate.layout = config;
    const dialogRef = this.dialog.open(ButtonViewConfigurationDialogComponent, {
      width: '100vw',
      data: JSON.parse(JSON.stringify(selectedTemplate)),
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.buttonConfig = result;
      }
    });
  }
  openButtonEditDialog(item: any) {
    const dialogRef = this.dialog.open(ButtonConfigComponent, {
      width: '100vw',
      data: { value: JSON.parse(JSON.stringify(item)) },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        item = result;
        this.buttonsDone[item.position] = item;
      }
    });
  }
  removeButton(j: number) {
    this.buttonsDone.splice(j, 1);
  }
  onCheckboxChange(option: any, event: any) {
    if (event.checked) {
      option.isChecked = true;
    } else {
      option.isChecked = false;
    }
  }
}
