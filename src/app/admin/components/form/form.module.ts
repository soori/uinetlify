import { SelectConfigComponent } from './config/select/select-config.component';
import { TextAreaConfigComponent } from './config/textarea/textarea-config.component';
import { LableConfigComponent } from './config/lable/lable-config.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { DatePickerConfigComponent } from './config/datepicker/datepicker-config.component';
import { TextBoxConfigComponent } from './config/textbox/textbox-config.component';
import { ViewConfigurationDialogComponent } from './config/view/view-configuration.component';
import { FormCreateComponent } from './create/create.component';
import { FormfieldRenderComponent } from './render/render.component';
import { FormViewComponent } from './view/view.component';
import { ButtonModule } from '../button/button.module';
import { CheckBoxConfigComponent } from './config/checkbox/checkbox-config.component';
import { RadioButtonConfigComponent } from './config/radiobutton/radiobutton-config.component';


@NgModule({
  declarations: [FormCreateComponent, FormViewComponent, FormfieldRenderComponent,
    TextBoxConfigComponent, DatePickerConfigComponent, ViewConfigurationDialogComponent,
    CheckBoxConfigComponent, RadioButtonConfigComponent, LableConfigComponent, SelectConfigComponent, TextAreaConfigComponent],
  imports: [
    CommonModule,
    DragDropModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    ButtonModule
  ],
  exports: [
    FormCreateComponent, FormViewComponent, FormfieldRenderComponent
  ],
  entryComponents: []
})
export class FormFieldsModule { }
