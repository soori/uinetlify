import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { AppRouteService } from 'src/app/admin/route/route.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { FieldBase } from '../model/base';

@Component({
    selector: 'app-formfield-render-cmp',
    templateUrl: './render.component.html',
    styleUrls: ['./render.component.scss']
})
export class FormfieldRenderComponent implements OnInit {

    @Input() basicField: FieldBase<any>;
    @Input() basicForm: FormGroup;
    @Input() template: any;
    @Input() pathSegments: any;
    checkedOptions: any[] = [];
    checkboxFormGroup: FormGroup;
    basicFieldOptions: { display_name: any; id: string; }[];
    basicFieldControl: any;
    hidePswrd = true;
    selectedCheckbox: any = {};
    constructor(
        protected translate: TranslateService, private formBuilder: FormBuilder,
        protected store: Store<UserLanguageState>, private routeService: AppRouteService) {
        this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
    }
    ngOnInit() {
        this.translate.get(this.basicField.label).subscribe((text: string) => {
            this.basicField.label = text;
        });
        // validation controls for level 2
        if (this.pathSegments.length === 2) {
            const basePathSegement = this.basicForm.get(this.pathSegments[0]);
            if (basePathSegement) {
                this.basicFieldControl = basePathSegement.get(this.pathSegments[1]);
            }
        }
        // TO-DO set fieldValue when viewed
        if (this.basicField.isSetValue) {
            this.setValue(this.basicField.value, this.pathSegments[this.pathSegments.length - 1], this.pathSegments);
            if (this.basicField.required) {
                this.basicForm.get(this.basicField.modelName)?.valueChanges.subscribe(val => {
                    if (!val) {
                        const formControlName = this.basicField.modelName;
                        this.basicForm.patchValue({ formControlName: this.basicField.value });
                    }
                });
            }
            // To update form status
            this.basicForm.statusChanges.subscribe((result) => {
                this.routeService.changeStatus(result);
            });
        }
        // Derived field config
        if (this.basicField.isDerived) {
            const derivedFrom: any = this.basicForm.get(this.basicField.derivedFrom.modelName);
            if (derivedFrom?.value) {
                this.basicForm.get(this.basicField.modelName)?.enable();
            } else {
                this.basicForm.get(this.basicField.modelName)?.disable();
            }
            this.basicForm.get(this.basicField.derivedFrom.modelName)?.valueChanges.subscribe(val => {
                if (!val) {
                    this.basicForm.get(this.basicField.modelName)?.disable();
                } else {
                    this.basicForm.get(this.basicField.modelName)?.enable();
                }
            });
        }
    }

    somethingChanged(event: any, formControlName: any, pathSegments: any) {
        let value = '';
        if (event.value) {
            if (event.source && event.source.checked) {
                event.value.isChecked = true;
            }
            value = event.value;
        } else if (event.target.value) {
            value = event.target.value;
        }
        if (this.basicField.isSetValue) {
            this.basicField.value = value;
        }
        this.setValue(value, formControlName, pathSegments);
    }

    onCheckboxChange(option: any, event: any, formControlName: any, selectionType: any) {
        const checkboxFormArray: FormArray = this.basicForm.get(formControlName) as FormArray;
        // Single select
        if (selectionType === 1) {
            this.selectedCheckbox = option;
            const fomcontrol = this.basicForm.get(formControlName);
            if (fomcontrol) {
                checkboxFormArray.removeAt(0);
                checkboxFormArray.push(new FormControl(option));
            }
        } else {
            // Multiple Select
            if (event.checked) {
                option.isChecked = true;
                checkboxFormArray.push(new FormControl(option));
            } else {
                for (let i = 0; i < this.basicField.options.length; i++) {
                    if (checkboxFormArray.value[i] === option) {
                        checkboxFormArray.removeAt(i);
                    }
                }
            }
        }
        if (this.basicField.isSetValue) {
            this.basicField.value = checkboxFormArray.value;
        }
        this.setValue(checkboxFormArray.value, formControlName, this.pathSegments);
    }
    setValue(value: any, formControlName: any, pathSegments: any) {
        if (pathSegments.length === 1) {
            this.basicForm.patchValue({ formControlName: value });
        } else if (pathSegments.length === 2) {
            const one = pathSegments[0];
            const two = pathSegments[1];
            this.basicForm.patchValue({ [one]: { [two]: value } });
        } else if (pathSegments.length === 3) {
            const one = pathSegments[0];
            const two = pathSegments[1];
            const three = pathSegments[2];
            this.basicForm.patchValue({ [one]: { [two]: { [three]: value } } });
        }
    }
    compareFn(c1: any, c2: any): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }
    showHidePswrd(field: any) {
        if (field.dataType.id === 1) {
            field.dataType = { id: 3, type: 'password', display_name: 'Password' };
        } else if (field.dataType.id === 3) {
            field.dataType = { id: 1, type: 'text', display_name: 'String' };
        }
        this.hidePswrd = !this.hidePswrd;
    }
}
