import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { FieldBase } from './model/base';


@Injectable({
    providedIn: 'root'
})
export class FormService {
    FormGroupCtrl: any;
    constructor(public fb: FormBuilder, private http: HttpClient) { }

    toFormGroup(fields: FieldBase<any>[], form: any) {
        const group: any = {};
        fields.forEach((field: any) => {
            const i = 0;
            // Doesn't push Label into formGroup
            if (field?.id !== 7) {
                form = this.dynamicFormGroup(i, form, group, field);
            }
        });
        return form;
    }

    dynamicFormGroup(i: number, form: any, group: any, field: any) {
        const pathSegments = field.pathSegments;
        if (i < pathSegments.length - 1) {
            // nested form groups
            if (i > 0 && form.get(pathSegments[i - 1])) {
                form.get(pathSegments[i - 1]).addControl(pathSegments[i], this.initGroup());
            } else {
                // control in single group
                (form.addControl(pathSegments[i], this.initGroup()));
            }
            i++;
            this.dynamicFormGroup(i, form, group, field);
        } else if (i === pathSegments.length - 1) {

            // lv-1
            if (pathSegments.length === 1) {
                this.FormGroupCtrl = form.controls;
            } else if (pathSegments.length === 2) {
                // lv-2
                this.FormGroupCtrl = form.get(pathSegments[i - 1]).controls;
            } else if (pathSegments.length === 3) {
                // lv-3
                const form1 = form.get(pathSegments[i - 2]);
                const form2 = form1.get(pathSegments[i - 1]);
                this.FormGroupCtrl = form.get(pathSegments[i - 2]).get(pathSegments[i - 1]).addControl();
            }

            // TO-DO loop above pathSegments
            // if (pathSegments.length) {
            //   for (let k in pathSegments) {
            //   }
            // }
            if (field.id === 3) {
                // Creating form Array for checkbox and Dropdown to allow muliptle selection
                this.FormGroupCtrl[pathSegments[pathSegments.length - 1]] = new FormArray([]);
            } else {
                this.FormGroupCtrl[pathSegments[pathSegments.length - 1]] = field.required ? new FormControl(field.value || '',
                    (field.dataType && field.dataType.id === 2) ? [Validators.required, Validators.pattern(field.pattern),
                    Validators.min(field.minlength ? field.minlength : -999999),
                    Validators.max(field.maxlength ? field.maxlength : 999999)] :
                        [Validators.required, Validators.pattern(field.pattern),
                        Validators.minLength(field.minlength ? field.minlength : 0),
                        Validators.maxLength(field.maxlength ? field.maxlength : 999999)]
                ) : new FormControl(field.value || '');
            }
        }
        return form;
    }

    initGroup(): FormGroup {
        const formGroup = this.fb.group({
        });
        return formGroup;
    }
    saveTemplate(template: any) {
        return this.http.post(`${environment.apiEndpoint}/formbuilder`, template);
    }
    updateTemplate(template: any, id: any) {
        return this.http.put(`${environment.apiEndpoint}/formbuilder/${id}`, template);
    }
    deleteTemplate(id: any) {
        return this.http.delete(`${environment.apiEndpoint}/formbuilder/${id}`);
    }
    getData(endpoint: any) {
        return this.http.get(`${endpoint}`);
    }
}
