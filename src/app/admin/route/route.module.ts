import { AgmCoreModule } from '@agm/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguCarouselModule } from '@ngu/carousel';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdminComponentsModule } from '../components/components.module';
import { AppRouteComponent } from './route.component';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    AgmCoreModule,
    ScrollToModule.forRoot(),
    NguCarouselModule,
    DragDropModule,
    FormsModule,
    AdminComponentsModule
  ],
  declarations: [
    AppRouteComponent,
  ],
  providers: [
  ],
  exports: [AppRouteComponent],
  entryComponents: [AppRouteComponent]
})
export class AppRouteModule { }
