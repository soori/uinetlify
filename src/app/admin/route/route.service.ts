import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class AppRouteService {
    private formStatus = new BehaviorSubject('');
    updateFormStatus = this.formStatus.asObservable();
    constructor(private http: HttpClient) { }

    // RK :: Why do we need this ??

    // Get data from enpoint on button Click
    getData(endpoint: any) {
        return this.http.get(`${endpoint}`);
    }
    // Put data from enpoint on button Click
    updateData(endpoint: any, data: any) {
    }
    // Delete data from enpoint on button Click
    deleteData(endpoint: any, id: number) {
        return this.http.delete(`${endpoint}/${id}`);
    }
    // Post data from enpoint on button Click
    postData(endpoint: any, data: any) {
        return this.http.post(`${endpoint}`, data);
    }

    getTemplate() {
        return this.http.get(`${environment.apiEndpoint}/formtemplate`);
    }
    getPageData(page: any) {
        return this.http.get(`${environment.apiEndpoint}/${page.endpoint}/${page.id}`);
    }
    changeStatus(status: string) {
        this.formStatus.next(status);
    }
}
