import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { AppService } from 'src/app/app.service';
import { FormViewComponent } from '../components/form/view/view.component';
import { AppRouteService } from './route.service';

@Component({
  selector: 'app-route',
  templateUrl: './route.component.html',
  styleUrls: ['./route.component.scss']
})
export class AppRouteComponent implements OnInit, OnChanges {

  @Output() showCreate = new EventEmitter<boolean>();
  @Input() appViewPageItems: any = {};
  @Input() layoutFlex: any = {};
  @ViewChild(FormViewComponent, { static: false })

  private formViewCMp: FormViewComponent;

  pageRouteItems: any[] = [];
  pageItems: any = [];
  formValue: any;
  selectedTabIndex: Number = 0;
  mobileFlex: any;
  treeSelction: any;
  isCreatePage: string | null;
  layoutItems: any;
  page: any = {};
  pages: any = [];
  isLoading: Boolean = false;
  permissionCodes: Array<string> = [];
  constructor(private route: ActivatedRoute, private router: Router, private service: AppRouteService,
    private location: Location, public appService: AppService) {
    this.permissionCodes = JSON.parse(localStorage.getItem('permissionCodes') || 'null');
    this.router.events
      .pipe(filter(e => e instanceof NavigationEnd))
      .subscribe((e: NavigationEnd) => {
        if (this.permissionCodes && this.permissionCodes.length > 0) {
          this.getPages(e.url);
        }
      });
  }

  ngOnInit() {

    this.route.queryParamMap.subscribe(params => {
      this.isCreatePage = params.get('isCreate');
    });
    localStorage.removeItem('template');
    this.pageItems = this.pageRouteItems ? this.pageRouteItems : this.appViewPageItems;
    this.service.getTemplate().subscribe(data => {
    });
    const pageValue = JSON.parse(localStorage.getItem('selectedConfig') || 'null');
    if (this.isCreatePage === '0') {
      this.formValue = [
        { modelName: 'role.name', value: pageValue.name, isCreatePage: this.isCreatePage },
        { modelName: 'role.description', value: pageValue.description, isCreatePage: this.isCreatePage }
      ];
    } else {
      this.formValue = [];
    }
    this.page.id = pageValue ? pageValue.id : null;
  }
  ngOnChanges(): void {
    if (this.appViewPageItems) {
      this.pageItems = this.appViewPageItems;
    }

  }
  gotoCreate() {
    this.showCreate.emit(false);
  }

  getFormValue(value: any) {
    console.log(value);
    this.formValue = value;
  }

  getPageAction(value: any) {
    if (value.type.id === 1) {
      this.formViewCMp.getFormValue();
      const postData = this.processSubmitData();
      this.submitAction(value, postData);
    } else if (value.type.id === 2) {
      this.formViewCMp.resetFormValue();
    } else if (value.type.id === 3) {
      const hasRouteId: Boolean = false;
      const url: any[] = value.url.split('/');
      const navRoute: any[] = [];
      // searchs for id and replaces it with tabIndex(nav from tab to inner stepper, works only for 'new/id'(level 1) )
      for (const index in url) {
        if (url[index] === 'id') {
          url[index] = this.selectedTabIndex;
          navRoute[index] = this.selectedTabIndex;
          // hasRouteId = true;
        } else {
          navRoute[index] = url[index];
        }
      }
      hasRouteId ? this.router.navigate(['/' + navRoute[0] + '/' + navRoute[1]], { relativeTo: this.route })
        : this.router.navigate(['./../' + value.url], { relativeTo: this.route });
    }
  }

  submitAction(item: any, formValue: any) {
    if (item.httpCall.id === 1) {
      this.service.getData(item.endPoint);
    } else if (item.httpCall.id === 2) {
      // this.service.postData(item.endPoint, formValue).subscribe(data => {
      // });
      console.log('post data call and redirect');
      // this.router.navigate(['./../' + item.url], { relativeTo: this.route });
    } else if (item.httpCall.id === 3) {
      this.service.updateData(item.endPoint, formValue);
    } else if (item.httpCall.id === 4) {
      this.service.deleteData(item.endPoint, formValue);
    }
  }
  onTabChange(tabIndex: number) {
    this.selectedTabIndex = tabIndex;
  }
  getTreeSelection(selection: any) {
    this.treeSelction = selection;
  }
  getPages(url: string) {
    this.isLoading = true;
    // url = url.substring(1, this.router.url.length);
    this.appService.getPageByUrl(url).subscribe((data: any) => {
      this.pageItems = data[0];
      const hasPermission = this.permissionCodes.includes(this.pageItems?.permission_code);
      if (this.pageItems && hasPermission) {
        this.layoutItems = this.pageItems.layoutItems;
        this.layoutItems.forEach((item: any, index: any) => {
          if (item.details && item.details.type.id === 5) {
            item.attributes.forEach((el: any) => {
              if (el.httpCall && el.httpCall.id === 2) {
                this.page.endpoint = el.endPoint;
              }
            });
          }
          // Permission only works for forms now
          if (item?.details?.type?.id === 1) {
            const formItems: Array<any> = [];
            item.attributes.forEach((el: any) => {
              if (el.securityAttrributes.visibility.id === 2) {
                if (this.permissionCodes.includes(el.permission)) {
                  formItems.push(el);
                }
              } else if (el.securityAttrributes.visibility.id === 0) {
                formItems.push(el);
              }
            });
            if (formItems) {
              this.pageItems.layoutItems[index].attributes = formItems;
            }
          }
        });
      }
      if (this.page.id && this.page.endpoint && (this.isCreatePage === '0')) {
        this.getPageValue(this.page);
      }
      this.isLoading = false;
    });
  }
  processSubmitData() {
    let role: any = {};
    let postData = {};
    if (this.treeSelction && !this.formValue) {
      postData = {
        treeSelction: this.treeSelction
      };
    } else if (!this.treeSelction && this.formValue) {
      postData = this.formValue.role;
    } else if (this.treeSelction && this.formValue) {
      role = this.formValue.role;
      role.addldata = {};
      for (const element of this.treeSelction) {
        if (element.outputName.includes('addldata')) {
          role.addldata[element.outputName] = element.selectedIds;
        } else {
          role[element.outputName] = element.selectedIds;
        }
      }
      postData = role;
    }
    return postData;
  }
  getPageValue(page: any) {
    this.service.getPageData(page).subscribe(data => {
    });
  }
}
