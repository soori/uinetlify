import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { MenuService } from './../menu.service';
import { AppService } from 'src/app/app.service';
import { MatSelectChange } from '@angular/material/select';
import { Menu } from 'src/app/theme/components/menu/menu.model';
import { MessageService } from 'src/app/shared/snackbar/message.service';

@Component({
  selector: 'app-menu-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuCreateComponent implements OnInit {
  menuItem: any = {};
  selectedIcon: any = [];
  iconsnames: any = [];
  iconCategories: any = [];
  listicons: any = [];
  iconList: any = [];
  yesRNo: any = [
    { id: true, display_name: 'True' },
    { id: false, display_name: 'False' }
  ];
  isLoading: Boolean = false;
  pages: any;
  pageDetails: any = {};
  isCreatePage: string | null;
  pageGroups: any = [
    { value: 'PG1', name: 'Page Group 1' },
    { value: 'PG2', name: 'Page Group 2' },
    { value: 'PG3', name: 'Page Group 3' }
  ];
  menuItems: any;
  parentItems: any = [];
  childPages: any = [];
  selectedMenuItem: any = {};
  dataloaded: boolean;
  menuModel: Menu[];
  parentsMenu: any;

  constructor(public service: MenuService, protected router: Router, protected route: ActivatedRoute, private appService: AppService,
    protected translate: TranslateService,
    private _messageService: MessageService, protected store: Store<UserLanguageState>, public dialog: MatDialog) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  ngOnInit() {
    this.parentItems = [];
    this.childPages = [];
    this.getIcons();
    this.getMenuList();
    this.getDynamicPagesList();
    this.route.queryParamMap.subscribe(params => {
      this.isCreatePage = params.get('isCreate');
    });
  }
  getMenuList() {
    this.dataloaded = false;
    this.service.getVerticalMenuItemsFromApi().subscribe(data => {
      this.menuModel = data;
      this.dataloaded = true;
      this.processMenuItems(this.menuModel);
      if (this.isCreatePage === '0') {
        this.checkEditData();
      } else {
        this.menuItem.has_sub_menu = false;
      }
    });
  }

  getDynamicPagesList() {
    this.service.getDynamicPages().subscribe(data => {
      this.childPages = this.childPages.concat(data);
    });

  }

  checkEditData() {
    const selectedMenu = JSON.parse(localStorage.getItem('selectedMenuItem') || 'null');
    this.selectedMenuItem = selectedMenu ? selectedMenu.selectedNodes[0] : null;
    if (this.selectedMenuItem) {
      this.menuItem = this.selectedMenuItem;
      this.isCreatePage = '0';
      for (const pagi of this.childPages) {
        if (pagi.router_link === this.menuItem.router_link) {
          this.pageDetails.page = pagi;
        }
      }
    }
  }


  processMenuItems(items: any) {

    items.forEach((element: any) => {
      if (element && element.parent_id === null) {
        this.parentItems.push(element);
      } else {
        this.childPages.push(element);
      }
    });
  }
  getIcons() {
    this.service.getIcons().subscribe((data: any) => {
      this.iconList = data.icons;
    });
  }

  submit() {
    this.service.saveMenu(this.menuItem).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Menu created successfully');
      this.router.navigate(['./..'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, while creating Role');
      });
  }

  update() {
    this.service.updateMenu(this.menuItem.id, this.menuItem).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Menu updated successfully');
      this.router.navigate(['./..'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, while updating Menu');
      });
  }

  pageChange(event: MatSelectChange) {
    this.menuItem.permission_code = event.value.permission_code;
    this.menuItem.router_link = event.value.router_link;
  }

  pagePermissionChange(event: any) {
    this.menuItem.permission_code = event.value;
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
  pageGroupChange(value: any, items: any, clearArray: Boolean) {

  }
}

