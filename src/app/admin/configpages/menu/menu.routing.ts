import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuCreateComponent } from './create/create.component';
import { MenuHomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: MenuHomeComponent, pathMatch: 'full', data: { breadcrumb: 'Menu Home' } },
  { path: 'new', component: MenuCreateComponent, pathMatch: 'full', data: { breadcrumb: 'Menu Create' } },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuRoutingModule { }
