import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { MenuService } from './../menu.service';
import { Menu } from 'src/app/theme/components/menu/menu.model';


@Component({
  selector: 'app-menu-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuHomeComponent implements OnInit {

  menuItems: any = {};
  selectedIcon: any = [];
  subMenu: any = [];
  results: any = [
    { id: 1, value: 'true', display_name: 'True' },
    { id: 0, value: 'false', display_name: 'False' }
  ];
  isLoading = false;
  tenants: any[];
  parentProp = 'id';
  childProp = 'parent_id';
  displayLabel = 'display_name';
  isItemSelectable = true;
  menuModel: any = {};
  isEditable = false;
  hasMenuIcon = true;
  hasMultipleSelection = false;
  tenant: any = {};
  dataloaded = false;
  parentsMenu: Menu[] = [];
  constructor(public service: MenuService, protected router: Router, protected route: ActivatedRoute,
    protected translate: TranslateService, protected store: Store<UserLanguageState>, public dialog: MatDialog) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
    this.getMenu();
  }

  private getMenu() {
    this.dataloaded = false;
    this.service.getVerticalMenuItemsFromApi().subscribe(data => {
      this.menuModel = data;
      this.dataloaded = true;
      this.parentsMenu = this.menuModel.filter((item: Menu) => item.has_sub_menu === true);
      localStorage.setItem('parentMenu', JSON.stringify(this.parentsMenu));
    });
  }

  ngOnInit() {
    this.getTenant();
    const savedMenu = JSON.parse(localStorage.getItem('savedMenu') || 'null');
  }

  getTenant() {
    this.isLoading = true;
    this.service.tenant().subscribe((data: any) => {
      this.isLoading = false;
      this.tenants = data;
    });
  }

  getSelection(event: any) {
    if (event.selectedNodes.length > 0) {
      this.isEditable = true;
    } else {
      this.isEditable = false;
    }
    localStorage.setItem('selectedMenuItem', JSON.stringify(event));
  }

}

