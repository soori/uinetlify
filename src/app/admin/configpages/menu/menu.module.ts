import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { MenuCreateComponent } from './create/create.component';
import { MenuHomeComponent } from './home/home.component';
import { MenuRoutingModule } from './menu.routing';
import { TreeModule } from '../../components/tree/tree.module';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PipesModule,
    MenuRoutingModule,
    TreeModule
  ],
  declarations: [
    MenuHomeComponent,
    MenuCreateComponent
  ],
  entryComponents: [],
  exports: [],
})
export class MenuModule { }
