import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { verticalMenuItems } from 'src/app/theme/components/menu/menu';
import { Menu } from 'src/app/theme/components/menu/menu.model';
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root',
})
export class MenuService {

    public getVerticalMenuItemsFromApi(): Observable<Menu[]> {
        return this.http.get<Menu[]>(`${environment.apiEndpoint}/menu`);
    }
    constructor(public http: HttpClient) { }

    getSubmenu(): Observable<any> {
        return this.http.get('./assets/data/company.json')
    }
    getMenu(): Observable<any> {
        return this.http.get('./assets/data/company.json')
    }

    getIcons(): Observable<any> {
        return this.http.get('./assets/data/icons.json')
    }
    tenant() {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/tenant`);
    }
    public getVerticalMenuItems(): Array<Menu> {
        return verticalMenuItems;
    }
    public getDynamicPages(): Observable<any> {
        return this.http.get(`${environment.apiEndpoint}/pagebuilder/basicinfo`);
    }

    saveMenu(config: any) {
        return this.http.post(`${environment.apiEndpoint}/menu`, config);
    }

    updateMenu(id: any, config: any) {
        return this.http.put(`${environment.apiEndpoint}/menu/${id}`, config);
    }

}
