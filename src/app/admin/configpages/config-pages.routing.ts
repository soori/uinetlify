import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRouteComponent } from '../route/route.component';

const routes: Routes = [
  { path: 'menu', loadChildren: './menu/menu.module#MenuModule' },
  { path: 'permission', loadChildren: './permission/permission.module#PermissionModule' },
  { path: 'preauth', loadChildren: './preauth/preauth.module#PreAuthConfigurationModule' },
  { path: 'usermanagement', loadChildren: './usermanagement/usermanagement.module#UserManagementModule' },
  { path: '**', component: AppRouteComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminConfigPagesRoutingModule { }
