import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { verticalMenuItems } from 'src/app/theme/components/menu/menu';
import { Menu } from 'src/app/theme/components/menu/menu.model';
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root',
})
export class PermissionService {

    getPermissionsLevels(): Observable<any[]> {
        return this.http.get<any[]>(`${environment.apiEndpoint}//metadata/global/permissionLevel`);
    }

    public getPermissionCodesFromApi(): Observable<Menu[]> {
        return this.http.get<Menu[]>(`${environment.apiEndpoint}/permissioncode`);
    }
    constructor(public http: HttpClient) { }


    tenant() {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/tenant`);
    }

    savePermission(config: any) {
        return this.http.post(`${environment.apiEndpoint}/permissioncode`, config);
    }

    updatePermission(id: any, config: any) {
        return this.http.put(`${environment.apiEndpoint}/permissioncode/${id}`, config);
    }

}
