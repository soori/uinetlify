import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionCreateComponent } from './create/create.component';
import { PermissionHomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: PermissionHomeComponent, pathMatch: 'full', data: { breadcrumb: 'Permission Home' } },
  { path: 'new', component: PermissionCreateComponent, pathMatch: 'full', data: { breadcrumb: 'Permission Create' } },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermissionRoutingModule { }
