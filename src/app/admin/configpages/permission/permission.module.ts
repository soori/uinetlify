import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { TreeModule } from '../../components/tree/tree.module';
import { PermissionCreateComponent } from './create/create.component';
import { PermissionHomeComponent } from './home/home.component';
import { PermissionRoutingModule } from './permission.routing';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PipesModule,
    PermissionRoutingModule,
    TreeModule
  ],
  declarations: [
    PermissionHomeComponent,
    PermissionCreateComponent
  ],
  entryComponents: [],
  exports: [],
})
export class PermissionModule { }
