import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from 'src/app/app.service';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { PermissionService } from './../permission.service';

@Component({
  selector: 'app-permission-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PermissionCreateComponent implements OnInit {
  menuItem: any = {};

  isLoading: Boolean = false;
  isCreatePage: string | null;
  parentItems: any = [];
  dataloaded: boolean;
  permissionLevels: any = [];
  totalPermissions: any[] = [];

  constructor(public service: PermissionService, protected router: Router, protected route: ActivatedRoute, private appService: AppService,
    protected translate: TranslateService,
    private _messageService: MessageService, protected store: Store<UserLanguageState>, public dialog: MatDialog) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }
  ngOnInit() {
    this.parentItems = [];
    this.totalPermissions = [];
    this.getPermissionsList();
    this.getPermissionLevels();
    this.route.queryParamMap.subscribe(params => {
      this.isCreatePage = params.get('isCreate');
    });


  }

  getPermissionLevels() {
    this.service.getPermissionsLevels().subscribe((data: any[]) => {
      this.permissionLevels = data;
    });
  }

  checkEditData() {
    const selectedPermission = JSON.parse(localStorage.getItem('selectedPermissionItem') || 'null');
    if (selectedPermission && selectedPermission.selectedNodes[0]) {
      this.menuItem = selectedPermission.selectedNodes[0];
      this.updateParents({ value: this.menuItem.pemission_level_id });
    }
  }

  getPermissionsList() {
    this.service.getPermissionCodesFromApi().subscribe((data: any[]) => {
      this.totalPermissions = data;
      if (this.isCreatePage === '0') {
        this.checkEditData();
      }
    });
  }

  updateParents($event: any) {
    this.parentItems = this.totalPermissions.filter((item: any) => item.pemission_level_id === ($event.value - 1));
  }




  submit() {
    const permissiontoSave = {
      code: (this.menuItem.parent_code ? this.menuItem.parent_code + "-" + this.menuItem.strip_code : this.menuItem.strip_code),
      display_name: this.menuItem.display_name,
      parent_code: this.menuItem.parent_code,
      pemission_level_id: this.menuItem.pemission_level_id
    }


    this.service.savePermission(permissiontoSave).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Permission created successfully');
      this.router.navigate(['./..'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, while creating Role');
      });
  }

  update() {
    const permissiontoUpdate = {
      code: (this.menuItem.parent_code ? this.menuItem.parent_code + "-" + this.menuItem.strip_code : this.menuItem.strip_code),
      display_name: this.menuItem.display_name,
      parent_code: this.menuItem.parent_code,
      pemission_level_id: this.menuItem.pemission_level_id,
      id: this.menuItem.id
    }
    this.service.updatePermission(this.menuItem.id, permissiontoUpdate).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Permission updated successfully');
      this.router.navigate(['./..'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, while updating Menu');
      });
  }

  pageChange(event: MatSelectChange) {
    this.menuItem.permission_code = event.value.permission_code;
    this.menuItem.router_link = event.value.router_link;
  }

  pagePermissionChange(event: any) {
    this.menuItem.permission_code = event.value;
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
  pageGroupChange(value: any, items: any, clearArray: Boolean) {

  }
}

