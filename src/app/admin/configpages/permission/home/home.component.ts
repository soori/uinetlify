import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { Menu } from 'src/app/theme/components/menu/menu.model';
import { PermissionService } from './../permission.service';


@Component({
  selector: 'app-permission-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PermissionHomeComponent implements OnInit {

  menuItems: any = {};
  selectedIcon: any = [];
  subMenu: any = [];
  results: any = [
    { id: 1, value: 'true', display_name: 'True' },
    { id: 0, value: 'false', display_name: 'False' }
  ];
  isLoading = false;
  tenants: any[];
  parentProp = 'code';
  childProp = 'parent';
  displayLabel = 'display_name';
  isItemSelectable = true;
  menuModel: any = {};
  isEditable = false;
  hasMenuIcon = true;
  hasMultipleSelection = false;
  tenant: any = {};
  dataloaded = false;
  parentsMenu: Menu[] = [];
  permissionCodes: Array<string> = [];
  constructor(public service: PermissionService, protected router: Router, protected route: ActivatedRoute,
    protected translate: TranslateService, protected store: Store<UserLanguageState>, public dialog: MatDialog) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
    this.getMenu();
  }

  private getMenu() {
    this.dataloaded = false;
    this.service.getPermissionCodesFromApi().subscribe(data => {
      this.menuModel = data;
      this.menuModel.forEach((element: any) => {
        this.permissionCodes.push(element.code);
      });
      localStorage.setItem('permissionCodes', JSON.stringify(this.permissionCodes));
      this.dataloaded = true;
    });
  }

  ngOnInit() {
    this.getTenant();
  }

  getTenant() {
    this.isLoading = true;
    this.service.tenant().subscribe((data: any) => {
      this.isLoading = false;
      this.tenants = data;
    });
  }

  getSelection(event: any) {
    if (event.selectedNodes.length > 0) {
      this.isEditable = true;
    } else {
      this.isEditable = false;
    }
    localStorage.setItem('selectedPermissionItem', JSON.stringify(event));
  }

}

