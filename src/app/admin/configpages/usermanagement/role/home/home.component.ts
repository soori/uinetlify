import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { UserManagementService } from '../../service/usermanagement.service';

@Component({
  selector: 'app-setup-usermanagement-role-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class RoleHomeComponent implements OnInit {
  showDefaultFilter: Boolean = true;
  dataSource: MatTableDataSource<any>;
  displayedColumns: any = ['name', 'description', 'created_by'];
  headers = [{ columnName: 'name', display_name: 'GRID.DISPLAY-NAME.ROLE-NAME' },
  { columnName: 'description', display_name: 'Description' },
  { columnName: 'created_by', display_name: 'Created by' },
  ];
  configItems: any;
  noData: any;
  isLoading: Boolean = false;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  roles: any = {};

  constructor(public service: UserManagementService, protected router: Router, protected route: ActivatedRoute,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    // this.getSelectedItems();
    this.getRoles();
  }
  applyFilter(event: any) {
    let filterValue = event.target.value;
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  selectedRowItem(selectedRow: any) {
    localStorage.setItem('selectedConfig', JSON.stringify(selectedRow));
    this.router.navigate(['./../edit/' + selectedRow.id], { relativeTo: this.route, queryParams: { isCreate: 0 } });
  }

  getSelectedItems() {
    this.isLoading = true;
    this.service.getAllitems().subscribe(data => {
      this.configItems = data;
      this.isLoading = false;
      for (const row of this.configItems) {
        row.applicationName = row.applicationType.display_name;
        row.userName = row.userType.display_name;
        row.tenantName = row.tenant.display_name;
      }
      this.dataSource = new MatTableDataSource(this.configItems);
      this.noData = this.dataSource.connect().pipe(map(dataa => dataa.length === 0));
      if (this.configItems.length > 0) {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    },
      err => {
        this.isLoading = false;
      });
  }
  getRoles() {
    this.service.getRoles().subscribe(data => {
      this.roles = data;
      this.dataSource = new MatTableDataSource(this.roles);
      this.noData = this.dataSource.connect().pipe(map(dataa => dataa.length === 0));
      if (this.roles.length > 0) {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  gotoRoleCreate() {
    this.router.navigate(['./../../rolss'], { relativeTo: this.route, queryParams: { isCreate: true } });
  }
}
