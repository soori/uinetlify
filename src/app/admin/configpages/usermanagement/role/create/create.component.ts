import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UserManagementService } from '../../service/usermanagement.service';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { TranslateService } from '@ngx-translate/core';
import { Menu } from 'src/app/theme/components/menu/menu.model';

@Component({
    selector: 'app-setup-usermanagement-role-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RoleCreateComponent implements OnInit {
    name: any;
    description: any;

    userTypes: any;
    userType: any = { id: null };

    tenants: any;
    tenant: any = { id: null };

    applicationTypes: any;
    applicationType: any = { id: null };
    selectedApplicationType: any;

    selectedConfig: any = {};
    codes: any[] = [];
    savedData: any;
    permissions: any;
    permissionType: any;
    isLoading: Boolean = false;



    parentProp = 'id';
    childProp = 'parent_id';
    displayLabel = 'display_name';
    isItemSelectable = true;
    menuModel: any = {};
    isEditable = false;
    hasMenuIcon = true;
    hasMultipleSelection = true;
    dataloaded = false;


    constructor(public service: UserManagementService, private _messageService: MessageService, protected translate: TranslateService,
        private router: Router, private route: ActivatedRoute) {

    }
    ngOnInit() {
        this.getMenu();
        this.getUserType();
        this.getApplicationType();
        this.getTenant();
        this.getPermission();
        this.getPermissionType();
    }

    getMenu() {
        this.dataloaded = false;
        this.service.getVerticalMenuItemsFromApi().subscribe(data => {
            this.menuModel = data;
            this.dataloaded = true;
        });
    }

    getUserType() {
        this.service.userType().subscribe(data => {
            this.userTypes = data;
        });
    }
    getApplicationType() {
        this.service.applicationType().subscribe(data => {
            this.applicationTypes = data;
        });
    }
    getTenant() {
        this.service.tenant().subscribe(data => {
            this.tenants = data;
        });
    }

    getSelection(selectedItems: any) {
        this.selectedConfig = selectedItems;
    }

    getPermission() {
        this.service.getPermission().subscribe((data: any[]) => {
            this.permissions = data[0];
        });
    }
    getPermissionType() {
        this.service.getPermissionType().subscribe(data => {
            this.permissionType = data;
        });
    }
    saveSelectedConfig() {
        this.isLoading = true;
        for (const nodee of this.selectedConfig.selectedNodes) {
            this.codes.push(nodee.id);
        }
        const selectedRoleConfig: any = {};
        selectedRoleConfig.name = this.name;
        selectedRoleConfig.description = this.description;
        selectedRoleConfig.applicationType = { id: this.selectedApplicationType };
        selectedRoleConfig.userType = this.userType;
        selectedRoleConfig.tenant = this.tenant;
        selectedRoleConfig.permissions = this.codes;
        this.service.saveRole(selectedRoleConfig).subscribe(data => {
            this.savedData = data;
            this.isLoading = false;
            this._messageService.info('Role configured successfully');
            this.router.navigate(['./../..'], { relativeTo: this.route });
        },
            err => {
                this._messageService.warn('Error occured, while configuring Role');
            });
    }




}
