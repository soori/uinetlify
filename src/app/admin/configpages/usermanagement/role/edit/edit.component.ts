import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserManagementService } from '../../service/usermanagement.service';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { TranslateService } from '@ngx-translate/core';
import { Menu } from 'src/app/theme/components/menu/menu.model';


@Component({
  selector: 'app-setup-usermanagement-role-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class RoleEditComponent implements OnInit {
  userTypes: any;
  tenants: any;
  applicationTypes: any;
  applicationType: any = { id: null };
  userType: any = { id: null };
  tenant: any = { id: null };
  selectedConfig: any;
  codes: any[] = [];
  savedData: any;
  selectedItem: any;
  permissionCodes: any;
  permissions: any;
  permissionType: any;
  isEditable: Boolean = false;
  isLoading: Boolean = false;

  parentProp = 'id';
  childProp = 'parent_id';
  displayLabel = 'display_name';
  isItemSelectable = true;
  menuModel: any = {};
  hasMenuIcon = true;
  hasMultipleSelection = true;
  selecetdList: any[] | null;
  selectedItems: any;
  dataLoaded = false;

  constructor(public service: UserManagementService, private _messageService: MessageService, protected translate: TranslateService,
    private router: Router, private route: ActivatedRoute) {

  }

  private getMenu() {
    this.dataLoaded = false;
    this.service.getVerticalMenuItemsFromApi().subscribe(data => {
      this.menuModel = data;
      this.dataLoaded = true;
    });
  }

  ngOnInit() {
    this.getUserType();
    this.getApplicationType();
    this.getTenant();
    this.getMenu();
    this.selectedItem = JSON.parse(localStorage.getItem('selectedConfig') || 'null');
    this.selecetdList = this.selectedItem.permissions;
  }
  getUserType() {
    this.service.userType().subscribe(data => {
      this.userTypes = data;
    });
  }
  getApplicationType() {
    this.service.applicationType().subscribe(data => {
      this.applicationTypes = data;
    });
  }
  getTenant() {
    this.service.tenant().subscribe(data => {
      this.tenants = data;
    });
  }
  getSelectionConfig(selectedConfig: any) {
    if (selectedConfig.selected && selectedConfig.selected.length > 0) {
      this.selectedConfig = selectedConfig.selected;
    }
  }
  getPermissionCodes() {
    this.service.getPermissionCodes(this.selectedItem.userType.id,
      this.selectedItem.applicationType.id, this.selectedItem.tenant.id).subscribe(data => {
        this.permissionCodes = data;
      });
  }

  getSelection(selectedItems: any) {
    this.selectedItems = selectedItems;
  }
  updateSelectedConfig() {
    this.isLoading = true;
    for (const nodee of this.selectedItems.selectedNodes) {
      this.codes.push(nodee.id);
    }
    this.selectedItem.permissions = this.codes;
    this.service.updateRole(this.selectedItem.id, this.selectedItem).subscribe(data => {
      this.savedData = data;
      this.isLoading = false;
      this._messageService.info('Role Updated successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, while updating Role');
      });
  }
  getPermission() {
    this.service.getPermission().subscribe((data: any[]) => {
      this.permissions = data[0];
    });
  }
  getPermissionType() {
    this.service.getPermissionType().subscribe(data => {
      this.permissionType = data;
    });
  }
}
