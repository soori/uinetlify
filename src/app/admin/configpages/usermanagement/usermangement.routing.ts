import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleHomeComponent } from './role/home/home.component';
import { RoleCreateComponent } from './role/create/create.component';
import { RoleEditComponent } from './role/edit/edit.component';
import { UsersComponent } from './users/users.component';
import { AppRouteComponent } from '../../route/route.component';



const routes: Routes = [
  { path: '', redirectTo: 'role/home', pathMatch: 'full' },
  { path: 'role/home', component: RoleHomeComponent, pathMatch: 'full', data: { breadcrumb: 'Roles Home' } },
  { path: 'role/new', component: RoleCreateComponent, pathMatch: 'full', data: { breadcrumb: 'New Role' } },
  { path: 'role/edit/:id', component: RoleEditComponent, pathMatch: 'full', data: { breadcrumb: 'Modify Role' } },
  { path: 'users/home', component: UsersComponent, pathMatch: 'full', data: { breadcrumb: 'Users Home' } },
  { path: '**', component: AppRouteComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
