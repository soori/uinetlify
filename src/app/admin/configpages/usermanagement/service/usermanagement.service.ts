import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { verticalMenuItems } from 'src/app/theme/components/menu/menu';
import { Menu } from 'src/app/theme/components/menu/menu.model';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class UserManagementService {


    public getVerticalMenuItemsFromApi(): Observable<Menu[]> {
        return this.http.get<Menu[]>(`${environment.apiEndpoint}/menu`);
    }
    getVerticalMenuItems(): Array<Menu> {
        return verticalMenuItems;
    }
    constructor(public http: HttpClient) { }

    userType() {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/userType`);
    }
    applicationType() {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/applicationType`);
    }
    tenant() {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/tenant`);
    }
    getPermission() {
        return this.http.get(`${environment.apiEndpoint}/config/global/permission`);
    }
    getPermissionType() {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/permissionType`);
    }
    saveSelectedConfig(config: any) {
        return this.http.post(`${environment.apiEndpoint}/config/global/role`, config);
    }
    getAllitems() {
        return this.http.get(`${environment.apiEndpoint}/config/global/role`);

    }
    updateSelectedConfig(id: any, config: any) {
        return this.http.put(`${environment.apiEndpoint}/config/global/role/${id}`, config);
    }
    getPermissionCodes(userTypeId: any, appTypeId: any, tenantId: any) {
        return this.http.get(`${environment.apiEndpoint}/config/global/role/${userTypeId}/${appTypeId}/${tenantId}/permCodes`);
    }
    getRoles() {
        return this.http.get(`${environment.apiEndpoint}/role`);
    }

    saveRole(config: any) {
        return this.http.post(`${environment.apiEndpoint}/role`, config);
    }

    updateRole(id: any, config: any) {
        return this.http.put(`${environment.apiEndpoint}/role/${id}`, config);
    }

}
