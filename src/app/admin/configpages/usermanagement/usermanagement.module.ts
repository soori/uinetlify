import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRouteModule } from 'src/app/admin/route/route.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { PermissionForRoleControlComponent } from './permission-control/permission-control.component';
import { RoleCreateComponent } from './role/create/create.component';
import { RoleEditComponent } from './role/edit/edit.component';
import { RoleHomeComponent } from './role/home/home.component';
import { UserRoutingModule } from './usermangement.routing';
import { UsersComponent } from './users/users.component';
import { TreeModule } from '../../components/tree/tree.module';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PipesModule,
    TreeModule,
    DragDropModule, UserRoutingModule, AppRouteModule
  ],
  declarations: [
    RoleCreateComponent,
    PermissionForRoleControlComponent,
    RoleHomeComponent,
    RoleEditComponent,
    UsersComponent,
    RoleEditComponent
  ],
  entryComponents: [],
  exports: [PermissionForRoleControlComponent]
})
export class UserManagementModule { }
