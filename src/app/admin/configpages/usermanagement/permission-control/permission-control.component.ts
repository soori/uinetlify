import { SelectionModel } from '@angular/cdk/collections';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { TranslateService } from '@ngx-translate/core';
interface PermissionNode {
  name: string;
  code: string;
  children?: PermissionNode[];
}
@Component({
  selector: 'app-permission-control-for-role',
  templateUrl: './permission-control.component.html',
  styleUrls: ['./permission-control.component.scss']
})
export class PermissionForRoleControlComponent implements OnInit, OnChanges {
  @Input() permissions: any;
  @Input() permissionType: any;
  @Input() selectedList: any[];
  @Input() isDisabled = false;
  @Output() selection: EventEmitter<any> = new EventEmitter();
  treeControl = new NestedTreeControl<PermissionNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<PermissionNode>();
  checklistSelection = new SelectionModel<PermissionNode>(true /* multiple */);
  getChildren = (node: PermissionNode): PermissionNode[] | undefined => node.children;
  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  constructor(protected translate: TranslateService) {
  }

  ngOnInit() {
  }
  ngOnChanges() {
    if (this.permissions) {
      const treeData: PermissionNode[] = this.permissions.children;
      this.dataSource.data = treeData;
      const initiallySectedList: PermissionNode[] = [];
      if (this.selectedList) {
        for (const parentNode of treeData) {
          this.prepareinitialSelectedList(parentNode, initiallySectedList);
        }
      }
    }
  }

  prepareinitialSelectedList(node: any, list: any) {
    if (this.selectedList.includes(node.code)) {
      list.push(node);
      this.checklistSelection = new SelectionModel<PermissionNode>(true, list, true);
      this.checklistSelection.select(node);
    }
    if (node.children && node.children.length > 0) {
      for (const childeNode of node.children) {
        this.prepareinitialSelectedList(childeNode, list);
      }
    }

  }

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: PermissionNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (this.checklistSelection.selected) {
      this.selection.emit(this.checklistSelection);
    }
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: PermissionNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    if (this.checklistSelection.selected) {
      this.selection.emit(this.checklistSelection);
    }
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: PermissionNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    this.selection.emit(this.checklistSelection);
    // this.checkAllParentsSelection(node);
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: PermissionNode): void {
    this.checklistSelection.toggle(node);
    this.selection.emit(this.checklistSelection);
    // this.checkAllParentsSelection(node);
  }
  hasChild = (_: number, node: PermissionNode) => !!node.children && node.children.length > 0;
}
