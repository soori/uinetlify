import { Component, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { PreAuthService } from '../../service/preauth.service';


@Component({
  selector: 'app-setup-preauth-homepage-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class PreAuthHomePageEditComponent implements OnInit {
  isLoading = false;
  selectedConfig: any;
  config: any = [];
  tenants: any;
  userTypes: any;
  parentPermissions: any;
  isEditable = false;
  constructor(public service: PreAuthService, private _messageService: MessageService, protected translate: TranslateService,
    private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.selectedConfig = JSON.parse(localStorage.getItem('selectedHomePageConfig') || 'null');
    this.getTenant();
    this.getUserType();
    this.getParentPermissions();
    this.config = this.selectedConfig.config;
    this.tabClick(this.config[0].userTypeId);
  }
  getTenant() {
    this.service.tenant().subscribe(data => {
      this.tenants = data;
    });
  }
  getUserType() {
    this.service.userType().subscribe(data => {
      this.userTypes = data;
    });
  }
  getParentPermissions() {
    this.service.getParentPermissions().subscribe(data => {
      this.parentPermissions = data;
    });
  }
  tenantChange() {
    this.isLoading = true;
    this.config = [];
    if (this.selectedConfig.tenant && this.selectedConfig.tenant.id) {
      this.service.getExisitngPermissions(this.selectedConfig.tenant.id).subscribe(data => {
        this.config = data;
        this.isLoading = false;
      });
    }
  }

  isSelected(userTypeId: number, permissionCode: number) {
    const isCheckboxselected = this.config.some((item: any) => (item.authParentPermissionCode.includes(permissionCode))
      && (item.userTypeId === userTypeId));
    if (isCheckboxselected) {
      return true;
    } else {
      return false;
    }
  }
  changeSelection(userTypeId: number, permissionCode: number, isChecked: MatCheckboxChange) {
    if (isChecked) {
      if (this.config.length > 0) {
        let foundEntry = false;
        this.config.forEach((element: any) => {
          if (element.userTypeId === userTypeId) {
            foundEntry = true;
            if (isChecked.checked) {
              element.authParentPermissionCode.push(permissionCode);
            } else {
              const index = element.authParentPermissionCode.findIndex((item: any) => item === permissionCode);
              element.authParentPermissionCode.splice(index, 1);
            }
          }
        });
        if (!foundEntry) {
          const checkedPermissions: number[] = [];
          checkedPermissions.push(permissionCode);
          this.config.push({ authParentPermissionCode: checkedPermissions, userTypeId: userTypeId });
        }
      } else {
        this.config = [];
        const checkedPermissions: number[] = [];
        checkedPermissions.push(permissionCode);
        this.config.push({ authParentPermissionCode: checkedPermissions, userTypeId: userTypeId });
      }
    }
  }
  saveSelectedConfig() {
    const saveConfig: any = {};
    saveConfig.tenant = this.selectedConfig.tenant;
    saveConfig.config = this.config;
    saveConfig.name = this.selectedConfig.name;
    this.service.updateHompageItems(this.selectedConfig.id, saveConfig).subscribe(data => {
      this._messageService.info('Homepage Updated successfully');
      this.router.navigate(['./../..'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, while updating Homepage configuration');
      });
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  tabClick(usrType: any) {
    this.selectedConfig.config.forEach((element: any) => {
      if (element.userTypeId === usrType.id) {
        element.authParentPermissionCode.forEach((code: any) => {
          this.isSelected(element.userTypeId, code);
        });
      }
    });
  }
}
