import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { PreAuthService } from '../../service/preauth.service';

@Component({
  selector: 'app-setup-preauth-homepage-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class PreAuthHomePageHomeComponent implements OnInit {
  showDefaultFilter = true;
  dataSource: MatTableDataSource<any>;
  displayedColumns: any = ['tenantName', 'name'];
  headers = [
    { columnName: 'tenantName', display_name: 'GRID.DISPLAY-NAME.TENANT' },
    { columnName: 'name', display_name: 'Name' },
  ];
  configItems: any;
  isLoading = false;
  noData: any;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  homepagePermissions: any;

  constructor(public service: PreAuthService, protected router: Router, protected route: ActivatedRoute,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.getHomepageItems();
  }
  applyFilter(event: any) {
    let filterValue = event.target.value;
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  selectedRowItem(selectedRow: any) {
    localStorage.setItem('selectedHomePageConfig', JSON.stringify(selectedRow));
    this.router.navigate(['./edit/' + selectedRow.id], { relativeTo: this.route });
  }

  getHomepageItems() {
    this.isLoading = true;
    this.service.getHomepagePermissions().subscribe(data => {
      this.homepagePermissions = data;
      this.isLoading = false;
      for (const row of this.homepagePermissions) {
        row.tenantName = row.tenant.display_name;
      }
      this.dataSource = new MatTableDataSource(this.homepagePermissions);
      this.noData = this.dataSource.connect().pipe(map(dataa => dataa.length === 0));
      if (this.homepagePermissions.length > 0) {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
}
