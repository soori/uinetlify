import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { PreAuthService } from '../../service/preauth.service';

@Component({
    selector: 'app-setup-preauth-homepage-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PreAuthHomePageCreateComponent implements OnInit {
    userTypes: any;
    tenants: any;
    tenant: any;
    isLoading = false;
    parentPermissions: any;
    config: any = [];
    configName: String = '';
    constructor(private _messageService: MessageService, protected translate: TranslateService,
        private router: Router, private route: ActivatedRoute, private service: PreAuthService,
        protected store: Store<UserLanguageState>) {
        this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
    }

    ngOnInit() {
        this.getUserType();
        this.getTenant();
        this.getParentPermissions();
    }
    getUserType() {
        this.isLoading = true;
        this.service.userType().subscribe(data => {
            this.isLoading = false;
            this.userTypes = data;
        });
    }

    getTenant() {
        this.isLoading = true;
        this.service.tenant().subscribe(data => {
            this.isLoading = false;
            this.tenants = data;
        });
    }

    getParentPermissions() {
        this.isLoading = true;
        this.service.getParentPermissions().subscribe(data => {
            this.isLoading = false;
            this.parentPermissions = data;
        });
    }

    tenantChange() {
        this.config = [];
        if (this.tenant && this.tenant.id) {
            this.service.getExisitngPermissions(this.tenant.id).subscribe(data => {
                this.config = data;
            });
        }
    }
    isSelected(userTypeId: number, permissionCode: number) {
        const isCheckboxselected = this.config.some((item: any) => (item.authParentPermissionCode.includes(permissionCode))
            && (item.userTypeId === userTypeId));
        if (isCheckboxselected) {
            return true;
        } else {
            return false;
        }
    }
    changeSelection(userTypeId: number, permissionCode: number, isChecked: MatCheckboxChange) {
        if (isChecked) {
            if (this.config.length > 0) {
                let foundEntry = false;
                this.config.forEach((element: any) => {
                    if (element.userTypeId === userTypeId) {
                        foundEntry = true;
                        if (isChecked.checked) {
                            element.authParentPermissionCode.push(permissionCode);
                        } else {
                            const index = element.authParentPermissionCode.findIndex((item: any) => item === permissionCode);
                            element.authParentPermissionCode.splice(index, 1);
                        }
                    }
                });
                if (!foundEntry) {
                    const checkedPermissions: number[] = [];
                    checkedPermissions.push(permissionCode);
                    this.config.push({ authParentPermissionCode: checkedPermissions, userTypeId: userTypeId });
                }
            } else {
                this.config = [];
                const checkedPermissions: number[] = [];
                checkedPermissions.push(permissionCode);
                this.config.push({ authParentPermissionCode: checkedPermissions, userTypeId: userTypeId });
            }
        }
    }
    saveSelectedConfig() {
        const saveConfig: any = {};
        saveConfig.tenant = this.tenant;
        saveConfig.config = this.config;
        saveConfig.name = this.configName;
        this.service.saveSelectedPermissions(saveConfig).subscribe(data => {
            this._messageService.info('Homepage configured successfully');
            this.router.navigate(['./..'], { relativeTo: this.route });
        },
            err => {
                this._messageService.info('Error occured, while configuring Homepage');
            });
    }
}
