import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { PreAuthHomePageCreateComponent } from './create/create.component';
import { PreAuthHomePageEditComponent } from './edit/edit.component';
import { PreAuthHomePageHomeComponent } from './home/home.component';
import { HomePageConfigRoutingModule } from './homepage.routing';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PipesModule,
    HomePageConfigRoutingModule
  ],
  declarations: [
    PreAuthHomePageHomeComponent,
    PreAuthHomePageCreateComponent,
    PreAuthHomePageEditComponent,
  ],
  entryComponents: []
})
export class HomepageConfigurationModule { }
