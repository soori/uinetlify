import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRouteComponent } from '../../../route/route.component';
import { PreAuthHomePageHomeComponent } from './home/home.component';
import { PreAuthHomePageCreateComponent } from './create/create.component';
import { PreAuthHomePageEditComponent } from './edit/edit.component';

const routes: Routes = [

  {
    path: '', component: PreAuthHomePageHomeComponent, pathMatch: 'full', data: { breadcrumb: 'Home' },
  },
  {
    path: 'new', component: PreAuthHomePageCreateComponent, pathMatch: 'full',
    data: { breadcrumb: 'Create' }
  },
  {
    path: 'edit/:id', component: PreAuthHomePageEditComponent, pathMatch: 'full',
    data: { breadcrumb: 'Modify' }
  },
  { path: '**', component: AppRouteComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageConfigRoutingModule { }
