import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PreAuthService } from '../../service/preauth.service';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-setup-preauth-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class PreAuthEditComponent implements OnInit {
  permissions: any;
  permissionType: any;
  appTypes: any;
  userTypes: any;
  tenants: any;
  preAuthConfigPermissions: any;
  selectedConfig: any;
  Tenant: any = { id: 1 };
  authCodes: any;
  selectedItems: any;
  codes: any[] = [];
  name: any;
  description: any;
  applicationType: any;
  savedData: Object;
  tenantId: number;
  isEditable: Boolean = false;
  isLoading: Boolean = false;
  constructor(public service: PreAuthService, private _messageService: MessageService, protected translate: TranslateService,
    private router: Router, private route: ActivatedRoute) {
    this.selectedConfig = JSON.parse(localStorage.getItem('selectedConfig') || 'null');
    this.authCodes = this.selectedConfig.codes;
  }

  ngOnInit() {
    this.getPermission();
    this.getPermissionType();
    this.getAppType();
    this.getTenants();
    this.getUserType();
    this.getPreAuthConfig();
  }

  getPermission() {
    this.service.getPermission().subscribe(data => {
      this.permissions = data[0];
    });
  }
  getPermissionType() {
    this.service.getGlobalPermissionType().subscribe(data => {
      this.permissionType = data;
    });
  }
  getAppType() {
    this.service.getAppType().subscribe(data => {
      this.appTypes = data;
    });
  }
  getUserType() {
    this.service.getUserType().subscribe(data => {
      this.userTypes = data;
    });
  }
  getTenants() {
    this.service.getTenants().subscribe(data => {
      this.tenants = data;
    });
  }
  getPreAuthConfig() {
    this.service.getPreAuthConfig().subscribe(data => {
      this.preAuthConfigPermissions = data;
    });
  }
  getSelection(selectedItems: any) {
    this.selectedItems = selectedItems;
  }
  saveSelecteditems() {
    this.isLoading = true;
    this.selectedConfig.codes = this.selectedItems.selectedIds;
    this.service.updateSelecteditems(this.selectedConfig.id, this.selectedConfig).subscribe(data => {
      this.savedData = data;
      this.isLoading = false;
      this._messageService.info('Pre-Auth configuration updated successfully');
      this.router.navigate(['./../..'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, while updating Pre-Auth config');
      });
  }
}
