import { Component, Inject, OnInit } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { ActivatedRoute, Router } from '@angular/router';
import { PreAuthService } from '../../service/preauth.service';

@Component({
  selector: 'app-setup-preauth-wizard-cmp',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class PreAuthWizardComponent implements OnInit {
  isClicked: Boolean = false;
  appTypes: any = [];
  appTypeMap = new Map<number, string>([
    [1, 'lni-chrome'],
    [2, 'lni-apple'],
    [3, 'lni-android'],
    [4, 'lni-windows'],
    [5, 'lni-website'],
    [6, 'lni-display']
  ]);
  userTypesMap = new Map<number, string>([
    [1, 'account_box'],
    [2, 'account_circle'],
    [3, 'assignment_ind'],
    [10, 'record_voice_over'],
    [5, 'sentiment_satisfied_alt'],
    [6, 'how_to_reg'],
    [7, 'directions_walk'],
    [8, 'directions_run'],
    [9, 'person_pin'],
    [4, 'adb'],
    [11, 'directions_car'],
    [12, 'people']
  ]);
  userTypes: any;
  showAuth: Boolean = true;
  isLoading: Boolean = false;
  enableNext: Boolean = false;
  preventSingleClick: Boolean = false;
  timer: any;
  delay: Number;
  constructor(protected router: Router, protected route: ActivatedRoute, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    public service: PreAuthService, public bottomSheetRef: MatBottomSheetRef<PreAuthWizardComponent>) { }

  ngOnInit() {
    this.appTypes = this.data.appTypes;
    this.userTypes = this.data.userTypes;
    const selectedAppIndex = this.appTypes.findIndex((type: any) => type.isClicked === true);

    this.enableNext = selectedAppIndex >= 0 ? true : false;
  }
  onAuthCardClick(appType: any) {
    this.enableNext = true;
    this.appTypes.forEach((element: any) => {
      element.isClicked = false;
    });
    appType.isClicked = !appType.isClicked;
    localStorage.setItem('appType', JSON.stringify(appType));
  }
  onAuthCardDbClick(appType: any) {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    this.enableNext = true;
    this.appTypes.forEach((element: any) => {
      element.isClicked = false;
    });
    appType.isClicked = !appType.isClicked;
    localStorage.setItem('appType', JSON.stringify(appType));
    const selectedUserIndex = this.userTypes.findIndex((type: any) => type.isClicked === true);
    this.enableNext = selectedUserIndex >= 0 ? true : false;
    this.showAuth = false;
  }
  onUserDbCardClick(userType: any) {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    this.enableNext = true;
    this.userTypes.forEach((element: any) => {
      element.isClicked = false;
    });
    userType.isClicked = !userType.isClicked;
    this.bottomSheetRef.dismiss();
    this.router.navigate(['/admin/pages/preauth/authtype/new']);
  }
  onUserCardClick(userType: any) {
    this.enableNext = true;
    this.userTypes.forEach((element: any) => {
      element.isClicked = false;
    });
    userType.isClicked = !userType.isClicked;
    localStorage.setItem('userType', JSON.stringify(userType));
  }
  onNext() {
    this.bottomSheetRef.dismiss();
    this.router.navigate(['/admin/builder/template/new'], { relativeTo: this.route });
  }
  onAppNext() {
    const selectedUserIndex = this.userTypes.findIndex((type: any) => type.isClicked === true);
    this.enableNext = selectedUserIndex >= 0 ? true : false;
    this.showAuth = false;
  }
  onUserNext() {
    this.bottomSheetRef.dismiss();
    this.router.navigate(['/admin/pages/preauth/authtype/new']);
  }
  onUserBack() {
    this.enableNext = true;
    this.showAuth = true;
  }

  getIconOfAppType(id: any): string {
    return this.appTypeMap.get(id) || 'lni-star';
  }
  getIconOfUserType(id: any): string {
    return this.userTypesMap.get(id) || 'account_circle';
  }
}
