import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from '../../../../../shared/snackbar/message.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { PreAuthService } from '../../service/preauth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-setup-preauth-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PreAuthCreateComponent implements OnInit {
    public page: any;
    public viewType = 'grid';
    isLinear = false;
    firstForm: FormGroup;
    secondForm: FormGroup;
    permissions: any;
    permissionType: any;

    userTypes: any;
    tenants: any;
    appTypes: any;
    selectedTenant: any;
    selectedApplicationType: any;
    selectedUserType: any;


    selectedItems: any;
    preAuthConfigPermissions: any;
    name: any;
    description: any;

    codes: any = [];
    savedData: any;
    isLoading: Boolean = false;
    selectedAppType: any;

    constructor(public service: PreAuthService, private _messageService: MessageService, protected translate: TranslateService,
        private router: Router, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.getPermission();
        this.getPermissionType();
        // this.getAppType();
        this.getTenants();
        // this.getUserType();
        this.getPreAuthConfig();
        this.selectedUserType = JSON.parse(localStorage.getItem('appType') || 'null');
        this.selectedAppType = JSON.parse(localStorage.getItem('userType') || 'null');
    }
    getPermission() {
        this.service.getPermission().subscribe(data => {
            this.permissions = data[0];
        });
    }
    getPermissionType() {
        this.service.getGlobalPermissionType().subscribe(data => {
            this.permissionType = data;
        });
    }
    getAppType() {
        this.service.getAppType().subscribe(data => {
            this.appTypes = data;
        });
    }
    getUserType() {
        this.service.getUserType().subscribe(data => {
            this.userTypes = data;
        });
    }
    getTenants() {
        this.service.getTenants().subscribe(data => {
            this.tenants = data;
        });
    }
    getPreAuthConfig() {
        this.service.getPreAuthConfig().subscribe(data => {
            this.preAuthConfigPermissions = data;
        });
    }
    getSelection(selectedItems: any) {
        this.selectedItems = selectedItems;
    }
    saveSelecteditems() {
        this.isLoading = true;
        const selectedConfiguration: any = {};
        selectedConfiguration.name = this.name;
        selectedConfiguration.description = this.description;
        // selectedConfiguration.applicationType =  this.selectedApplicationType ;
        // selectedConfiguration.userType =  this.selectedUserType ;
        selectedConfiguration.applicationType = this.selectedAppType;
        selectedConfiguration.userType = this.selectedUserType;
        selectedConfiguration.tenant = this.selectedTenant;
        selectedConfiguration.codes = this.selectedItems.selectedIds;
        this.service.saveSelecteditems(selectedConfiguration).subscribe(data => {
            this.savedData = data;
            this.isLoading = false;
            this._messageService.info('Pre-Auth configured successfully');
            this.router.navigate(['./..'], { relativeTo: this.route });
        },
            err => {
                this._messageService.warn('Error occured, while configuring Pre-Auth');
            }
        );
    }
}
