import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { TreeModule } from '../../../components/tree/tree.module';
import { PreAuthCreateComponent } from './create/create.component';
import { PreAuthEditComponent } from './edit/edit.component';
import { PreAuthHomeComponent } from './home/home.component';
import { PreAuthWizardComponent } from './wizard/wizard.component';
import { PermissionControlComponent } from './../permission-control/permission-control.component';
import { AuthTypeConfigRoutingModule } from './authtype.routing';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PipesModule,
    TreeModule,
    AuthTypeConfigRoutingModule
  ],
  declarations: [
    PreAuthCreateComponent,
    PermissionControlComponent,
    PreAuthHomeComponent,
    PreAuthEditComponent,
    PreAuthWizardComponent,
  ],
  entryComponents: [PreAuthWizardComponent]
})
export class PreAuthTypeConfigurationModule { }
