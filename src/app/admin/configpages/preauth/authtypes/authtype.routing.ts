import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRouteComponent } from '../../../route/route.component';
import { PreAuthCreateComponent } from './create/create.component';
import { PreAuthEditComponent } from './edit/edit.component';
import { PreAuthHomeComponent } from './home/home.component';

const routes: Routes = [

  { path: '', component: PreAuthHomeComponent, pathMatch: 'full', },
  { path: 'new', component: PreAuthCreateComponent, pathMatch: 'full', data: { breadcrumb: 'New' } },
  { path: 'edit/:id', component: PreAuthEditComponent, pathMatch: 'full', data: { breadcrumb: 'Modify' } },
  { path: '**', component: AppRouteComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthTypeConfigRoutingModule { }
