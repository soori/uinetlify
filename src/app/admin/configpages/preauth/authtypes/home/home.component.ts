import { Component, OnInit, ViewChild } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { PreAuthService } from '../../service/preauth.service';
import { PreAuthWizardComponent } from '../wizard/wizard.component';

@Component({
  selector: 'app-setup-preauth-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class PreAuthHomeComponent implements OnInit {
  showDefaultFilter: Boolean = true;
  dataSource: MatTableDataSource<any>;
  displayedColumns: any = ['tenantName', 'applicationName', 'userName', 'name'];
  headers = [{ columnName: 'applicationName', display_name: 'GRID.DISPLAY-NAME.APPLICATION-TYPE' },
  { columnName: 'userName', display_name: 'GRID.DISPLAY-NAME.USER-TYPE' },
  { columnName: 'tenantName', display_name: 'GRID.DISPLAY-NAME.TENANT' },
  { columnName: 'name', display_name: 'GRID.DISPLAY-NAME.CONFIGURATION-NAME' },
  ];
  configItems: any;
  isLoading: Boolean = false;
  noData: any;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  appTypes: any = [];
  userTypes: any = [];

  constructor(public service: PreAuthService, protected router: Router, protected route: ActivatedRoute,
    protected translate: TranslateService, protected store: Store<UserLanguageState>, private _bottomSheet: MatBottomSheet) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.getSelectedItems();
    this.getAppType();
    this.getUserType();
  }
  applyFilter(event: any) {
    let filterValue = event.target.value;
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  selectedRowItem(selectedRow: any) {
    localStorage.setItem('selectedConfig', JSON.stringify(selectedRow));
    this.router.navigate(['./edit/' + selectedRow.id], { relativeTo: this.route });
  }

  getSelectedItems() {
    this.isLoading = true;
    this.service.getSelecteditems().subscribe(data => {
      this.configItems = data;
      this.isLoading = false;
      for (const row of this.configItems) {
        row.applicationName = row.applicationType.display_name;
        row.userName = row.userType.display_name;
        row.tenantName = row.tenant.display_name;
      }
      this.dataSource = new MatTableDataSource(this.configItems);
      this.noData = this.dataSource.connect().pipe(map(dataa => dataa.length === 0));
      if (this.configItems.length > 0) {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    },
      err => {
        this.isLoading = false;
      });
  }

  goToNew() {
    this.openPreAuthWizard();
  }

  openPreAuthWizard(): void {
    const bottomSheetRef = this._bottomSheet.open(PreAuthWizardComponent, {
      hasBackdrop: true,
      data: { appTypes: this.appTypes, userTypes: this.userTypes },
      panelClass: 'custom-bottom-sheet-wizard-width'
    });
  }
  getAppType() {
    this.isLoading = true;
    this.service.getAppType().subscribe(data => {
      this.isLoading = false;
      this.appTypes = data;
    });
  }
  getUserType() {
    this.isLoading = true;
    this.service.getUserType().subscribe(data => {
      this.isLoading = false;
      this.userTypes = data;
    });
  }
}
