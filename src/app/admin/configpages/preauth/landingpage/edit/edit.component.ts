import { Component, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { PreAuthService } from '../../service/preauth.service';


@Component({
  selector: 'app-setup-preauth-landingpage-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class PreAuthLandingPageEditComponent implements OnInit {
  userTypes: any;
  tenants: any;
  applicationTypes: any;
  applicationType: any = { id: null };
  userType: any = { id: null };
  tenant: any;
  configuration: any = {
    applicationTypeId: null,
    userTypeId: null
  };
  config: any = [];
  isLoading = false;
  configName: String = '';
  isEditable = false;
  selectedLandingPageConfig: any;
  constructor(private _messageService: MessageService, protected translate: TranslateService,
    private router: Router, private route: ActivatedRoute, private _service: PreAuthService,
    private service: PreAuthService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.getUserType();
    this.getApplicationType();
    this.getTenant();
    this.selectedLandingPageConfig = JSON.parse(localStorage.getItem('selectedLandingPageConfig') || 'null');
    this.config = this.selectedLandingPageConfig.config;
    this.tabClick(this.config[0].applicationTypeId);
  }
  getUserType() {
    this.service.userType().subscribe(data => {
      this.userTypes = data;
      this.userTypes.optionsChecked = [];
    });
  }
  tenantChange() {
    this.isLoading = true;
    this.config = [];
    if (this.tenant && this.tenant.id) {
      this.service.getExisitngCheckbox(this.tenant.id).subscribe(data => {
        this.config = data;
        this.isLoading = false;
      });
    }
  }
  getApplicationType() {
    this.service.applicationType().subscribe(data => {
      this.applicationTypes = data;
    });
  }
  getTenant() {
    this.service.tenant().subscribe(data => {
      this.tenants = data;
    });
  }

  isSelected(applicationTypeId: number, userTypeId: number) {
    const hasIds = this.config.some((item: any) => (item.userIds.includes(userTypeId)) &&
      (item.applicationTypeId === applicationTypeId));
    if (hasIds) {
      return true;
    } else {
      return false;
    }
  }

  changeSelection(applicationTypeId: number, userTypeId: number, isChecked: MatCheckboxChange) {
    if (isChecked) {
      if (this.config.length > 0) {
        let foundEntry = false;
        this.config.forEach((element: any) => {
          if (element.applicationTypeId === applicationTypeId) {
            foundEntry = true;
            if (isChecked.checked) {
              element.userIds.push(userTypeId);
            } else {
              const index = element.userIds.findIndex((item: any) => item === userTypeId);
              element.userIds.splice(index, 1);
            }
          }
        });
        if (!foundEntry) {
          const checkedUserId: number[] = [];
          checkedUserId.push(userTypeId);
          this.config.push({ userIds: checkedUserId, applicationTypeId: applicationTypeId });
        }
      } else {
        this.config = [];
        const checkedUserId: number[] = [];
        checkedUserId.push(userTypeId);
        this.config.push({ userIds: checkedUserId, applicationTypeId: applicationTypeId });
      }
    }
  }

  saveSelectedConfig() {
    const saveConfig: any = {};
    saveConfig.tenant = this.selectedLandingPageConfig.tenant;
    saveConfig.config = this.config;
    saveConfig.name = this.selectedLandingPageConfig.name;
    this._service.updateLandingConfig(saveConfig, this.selectedLandingPageConfig.id).subscribe(data => {
      this._messageService.info('Landingpage configuration updated successfully');
      this.router.navigate(['./../../'], { relativeTo: this.route });
    },
      err => {
        this._messageService.warn('Error occured, while updating Landingpage configuration');
      });
  }
  tabClick(appType: any) {
    this.selectedLandingPageConfig.config.forEach((element: any) => {
      if (element.applicationTypeId === appType.id) {
        element.userIds.forEach((code: any) => {
          this.isSelected(element.applicationTypeId, code);
        });
      }
    });
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
