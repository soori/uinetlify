import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRouteComponent } from '../../../route/route.component';
import { PreAuthLandingPageCreateComponent } from './create/create.component';
import { PreAuthLandingPageEditComponent } from './edit/edit.component';
import { PreAuthLandingPageHomeComponent } from './home/home.component';

const routes: Routes = [

  {
    path: '', component: PreAuthLandingPageHomeComponent, pathMatch: 'full',
    data: { breadcrumb: 'Home' }
  },
  {
    path: 'new', component: PreAuthLandingPageCreateComponent, pathMatch: 'full',
    data: { breadcrumb: 'Create' }
  },
  {
    path: 'edit/:id', component: PreAuthLandingPageEditComponent, pathMatch: 'full',
    data: { breadcrumb: 'Modify' }
  },

  { path: '**', component: AppRouteComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingPageConfigRoutingModule { }
