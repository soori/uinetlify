import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { PreAuthService } from '../../service/preauth.service';

@Component({
    selector: 'app-setup-preauth-landingpage-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PreAuthLandingPageCreateComponent implements OnInit {
    userTypes: any;
    tenants: any;
    applicationTypes: any;
    applicationType: any = { id: null };
    userType: any = { id: null };
    tenant: any;
    configuration: any = {
        applicationTypeId: null,
        userTypeId: null
    };
    config: any = [];
    isLoading = false;
    configName: String = '';
    constructor(private _messageService: MessageService, protected translate: TranslateService,
        private router: Router, private route: ActivatedRoute, private _service: PreAuthService,
        private service: PreAuthService, protected store: Store<UserLanguageState>) {
        this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
    }

    ngOnInit() {
        this.getUserType();
        this.getApplicationType();
        this.getTenant();
    }
    getUserType() {
        this.isLoading = true;
        this.service.userType().subscribe(data => {
            this.isLoading = false;
            this.userTypes = data;
            this.userTypes.optionsChecked = [];
        });
    }
    tenantChange() {
        this.config = [];
        if (this.tenant && this.tenant.id) {
            this.service.getExisitngCheckbox(this.tenant.id).subscribe(data => {
                this.config = data;
            });
        }
    }
    getApplicationType() {
        this.isLoading = true;
        this.service.applicationType().subscribe(data => {
            this.isLoading = false;
            this.applicationTypes = data;
        });
    }
    getTenant() {
        this.isLoading = true;
        this.service.tenant().subscribe(data => {
            this.isLoading = false;
            this.tenants = data;
        });
    }

    isSelected(applicationTypeId: number, userTypeId: number) {
        const hasIds = this.config.some((item: any) => (item.userIds.includes(userTypeId)) &&
            (item.applicationTypeId === applicationTypeId));
        if (hasIds) {
            return true;
        } else {
            return false;
        }
    }

    changeSelection(applicationTypeId: number, userTypeId: number, isChecked: MatCheckboxChange) {
        if (isChecked) {
            if (this.config.length > 0) {
                let foundEntry = false;
                this.config.forEach((element: any) => {
                    if (element.applicationTypeId === applicationTypeId) {
                        foundEntry = true;
                        if (isChecked.checked) {
                            element.userIds.push(userTypeId);
                        } else {
                            const index = element.userIds.findIndex((item: any) => item === userTypeId);
                            element.userIds.splice(index, 1);
                        }
                    }
                });
                if (!foundEntry) {
                    const checkedUserId: number[] = [];
                    checkedUserId.push(userTypeId);
                    this.config.push({ userIds: checkedUserId, applicationTypeId: applicationTypeId });
                }
            } else {
                this.config = [];
                const checkedUserId: number[] = [];
                checkedUserId.push(userTypeId);
                this.config.push({ userIds: checkedUserId, applicationTypeId: applicationTypeId });
            }
        }
    }

    saveSelectedConfig() {
        const saveConfig: any = {};
        saveConfig.tenant = this.tenant;
        saveConfig.config = this.config;
        saveConfig.name = this.configName;
        this._service.saveLandingConfig(saveConfig).subscribe(data => {
            this._messageService.info('Landingpage configured successfully');
            this.router.navigate(['./..'], { relativeTo: this.route });
        },
            err => {
                this._messageService.info('Error occured, while configuring Landingpage');
            });
    }
}
