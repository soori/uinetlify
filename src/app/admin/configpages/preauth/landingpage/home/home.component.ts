import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { PreAuthService } from '../../service/preauth.service';

@Component({
  selector: 'app-setup-preauth-landingpage-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class PreAuthLandingPageHomeComponent implements OnInit {
  showDefaultFilter: Boolean = true;
  dataSource: MatTableDataSource<any>;
  displayedColumns: any = ['tenantName', 'name'];
  headers = [
    { columnName: 'tenantName', display_name: 'GRID.DISPLAY-NAME.TENANT' },
    { columnName: 'name', display_name: 'Name' },
  ];
  configItems: any;
  isLoading: Boolean = false;
  noData: any;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  landingPageConfig: any;

  constructor(public service: PreAuthService, protected router: Router, protected route: ActivatedRoute,
    protected translate: TranslateService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.getLandingPageConfig();
  }
  applyFilter(event: any) {
    let filterValue = event.target.value;
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  selectedRowItem(selectedRow: any) {
    localStorage.setItem('selectedLandingPageConfig', JSON.stringify(selectedRow));
    this.router.navigate(['./edit/' + selectedRow.id], { relativeTo: this.route });
  }

  getLandingPageConfig() {
    this.isLoading = true;
    this.service.getLandingConfig().subscribe(data => {
      this.isLoading = false;
      this.landingPageConfig = data;
      this.landingPageConfig.forEach((element: any) => {
        element.tenantName = element.tenant.display_name;
      });
      this.dataSource = new MatTableDataSource(this.landingPageConfig);
      this.noData = this.dataSource.connect().pipe(map(dataa => dataa.length === 0));
      if (this.landingPageConfig.length > 0) {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
}
