import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { PreAuthLandingPageCreateComponent } from './create/create.component';
import { PreAuthLandingPageEditComponent } from './edit/edit.component';
import { PreAuthLandingPageHomeComponent } from './home/home.component';
import { LandingPageConfigRoutingModule } from './landingpage.routing';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PipesModule,
    LandingPageConfigRoutingModule
  ],
  declarations: [
    PreAuthLandingPageCreateComponent,
    PreAuthLandingPageHomeComponent,
    PreAuthLandingPageEditComponent,
  ],
  entryComponents: []
})
export class LandingpageConfigurationModule { }
