import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';


@Injectable({
    providedIn: 'root',
})
export class PreAuthService {
    constructor(public http: HttpClient) { }
    private selectedConfig = new BehaviorSubject<any>({});
    selectedConfigToEdit = this.selectedConfig.asObservable();

    getPermission(): Observable<any> {
        return this.http.get(`${environment.apiEndpoint}/config/global/permission`);
    }
    getAppType(): Observable<any> {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/applicationType `);
    }
    getTenants(): Observable<any> {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/tenant`);
    }

    getPreAuthConfig(): Observable<any> {
        return this.http.get(`${environment.apiEndpoint}/metadata/table/preauthpermissioncode`);
    }

    getGlobalPermissionType(): Observable<any> {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/permissionType`);
    }

    getUserType(): Observable<any> {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/userType`);
    }

    saveSelecteditems(configuration: any) {
        return this.http.post(`${environment.apiEndpoint}/preAuthorizations`, configuration);
    }

    getSelecteditems() {
        return this.http.get(`${environment.apiEndpoint}/preAuthorizations`);
    }

    getAuthCodes(userId: any, appTypeId: any, tenantId: any) {
        return this.http.get(`${environment.apiEndpoint}/config/global/preauthenticationconfiguration/${userId}/${appTypeId}/${tenantId}/authCodes`);
    }

    updateSelecteditems(id: any, selectedConfig: any) {
        return this.http.put(`${environment.apiEndpoint}/preAuthorizations/${id}`, selectedConfig);
    }
    saveLandingConfig(config: any) {
        return this.http.post(`${environment.apiEndpoint}/landingpageconfig`, config);
    }
    updateLandingConfig(config: any, id: any) {
        return this.http.put(`${environment.apiEndpoint}/landingpageconfig/${id}`, config);
    }
    getLandingConfig() {
        return this.http.get(`${environment.apiEndpoint}/landingpageconfig`);
    }
    saveSelectedPermissions(config: any) {
        return this.http.post(`${environment.apiEndpoint}/homepageconfig`, config);
    }
    getHomepagePermissions() {
        return this.http.get(`${environment.apiEndpoint}/homepageconfig`);
    }
    getExisitngCheckbox(tenantId: number) {
        return this.http.get(`${environment.apiEndpoint}/config/global/preauth/landingpage/${tenantId}/details`);
    }
    getExisitngPermissions(tenantId: number) {
        return this.http.get(`${environment.apiEndpoint}/config/global/preauth/homepage/${tenantId}/details`);
    }
    userType() {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/userType`);
    }
    applicationType() {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/applicationType`);
    }
    tenant() {
        return this.http.get(`${environment.apiEndpoint}/metadata/global/tenant`);
    }
    getParentPermissions() {
        return this.http.get(`${environment.apiEndpoint}/metadata/table/preauthpermissioncode/parents`);
    }
    updateHompageItems(id: any, config: any) {
        return this.http.put(`${environment.apiEndpoint}/homepageconfig/${id}`, config);
    }
}
