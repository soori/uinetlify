import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRouteComponent } from '../../route/route.component';

const routes: Routes = [

  { path: 'landingpage', loadChildren: './landingpage/landingpage.module#LandingpageConfigurationModule', data: { breadcrumb: 'Landing Page Configuration' } },
  { path: 'homepage', loadChildren: './homepage/homepage.module#HomepageConfigurationModule', data: { breadcrumb: 'Home Page Configuration' } },
  { path: 'authtype', loadChildren: './authtypes/authtype.module#PreAuthTypeConfigurationModule', data: { breadcrumb: 'Authentication type Configuration' } },
  { path: '**', component: AppRouteComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PreAuthRoutingModule { }
