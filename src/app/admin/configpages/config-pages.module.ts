import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { AdminConfigPagesRoutingModule } from './config-pages.routing';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PipesModule,
    AdminConfigPagesRoutingModule
  ],
  declarations: [
  ],
  entryComponents: [],
  exports: [],
})
export class AdminConfigPagesModule { }
