import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRouteComponent } from '../route/route.component';

const routes: Routes = [
  { path: '', redirectTo: 'template', pathMatch: 'full' },
  { path: 'template', loadChildren: './template/template.module#TemplateBuilderModule', data: { breadcrumb: 'Template Builder' } },
  { path: 'page', loadChildren: './page/page.module#PageBuilderModule', data: { breadcrumb: 'Page Builder' } },
  { path: '**', component: AppRouteComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuilderRoutingModule { }
