import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgColorModule } from 'ng-color';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { BuilderRoutingModule } from './builder.routing.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    NgColorModule,
    DragDropModule,
    BuilderRoutingModule
  ],
  entryComponents: [],
  exports: [],

})
export class BuilderModule { }
