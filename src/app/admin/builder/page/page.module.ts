import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { PageCreateComponent } from './create/create.component';
import { PageEditComponent } from './edit/edit.component';
import { PageHomeComponent } from './home/home.component';
import { PageBuilderRoutingModule } from './page.routing.module';
import { PageViewComponent } from './view/view.component';
import { PageWizardComponent } from './wizard/wizard.component';
import { AdminComponentsModule } from '../../components/components.module';
import { CardComponent } from './config/card/card.component';
import { PageListCreateComponent } from './list/list.component';




@NgModule({
  declarations: [
    PageCreateComponent,
    PageHomeComponent,
    PageViewComponent,
    PageEditComponent,
    PageWizardComponent,
    CardComponent,
    PageListCreateComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    DragDropModule,
    PageBuilderRoutingModule,
    AdminComponentsModule
  ],
  providers: [{ provide: MatDialogRef, useValue: {} },
  { provide: MAT_DIALOG_DATA, useValue: [] }],
  entryComponents: [PageViewComponent, PageWizardComponent]
})
export class PageBuilderModule { }
