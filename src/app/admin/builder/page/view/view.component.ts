import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { FormViewComponent } from '../../../components/form/view/view.component';

@Component({
  selector: 'app-page-view-cmp',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class PageViewComponent implements OnInit, AfterViewInit {

  @Input() pageItems: any;
  @Output() showCreate = new EventEmitter<boolean>();
  @ViewChild(FormViewComponent, { static: false })

  private formViewCMp: FormViewComponent;
  mulipleFormGroup: FormArray;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.mulipleFormGroup = this.fb.array([]);
    this.pageItems.layoutItems.forEach((item: any) => {
      const form = this.initFormGroup(item._id);
      this.mulipleFormGroup.push(form);
    });
  }

  ngAfterViewInit() {
  }

  initFormGroup(name: any): FormGroup {
    name = this.fb.group({
    });
    return name;
  }

  gotoCreate() {
    this.showCreate.emit(false);
  }

  getFormValue(value: any) {
  }

  getPageAction(value: any) {
    if (value.type.id === 1) {
      if (this.formViewCMp) {
        this.formViewCMp.getFormValue();
      }
    } else if (value.type.id === 2) {
      this.formViewCMp.resetFormValue();
    } else if (value.type.id === 3) {
    }
  }

  getMultipleFormValue($event: any) {
  }

}
