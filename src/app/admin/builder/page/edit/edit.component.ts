import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { AppSettings } from 'src/app/app.settings';
import { Settings } from 'src/app/app.settings.model';
import { PageBuilderService } from '../page.service';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class PageEditComponent implements OnInit {

  pageDetails: any = {};
  pageItems: any[] = [];
  public settings: Settings;
  constructor(private router: Router, private route: ActivatedRoute, private pageService: PageBuilderService,
    public appSettings: AppSettings, private appService: AppService) {
    this.settings = this.appSettings.settings;
    this.settings.sidenavIsOpened = false;
    this.appService.changeData(this.settings);
  }

  ngOnInit() {
    const pageId = this.route.snapshot.paramMap.get('id');
    this.getPageItems(pageId);
  }

  getPageItems(pageId: any) {
    this.pageService.getPageItems(pageId).subscribe((data: any[]) => {
      this.pageDetails = data[0];
    });

  }

}
