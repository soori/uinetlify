import { Component, Inject, OnInit } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-page-wizard-cmp',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class PageWizardComponent implements OnInit {
  isClicked: Boolean = false;
  showDevices: Boolean = true;
  isLoading: Boolean = false;
  enableNext: Boolean = false;

  devices: any = [
    { id: 1, name: 'Mobile', icon: 'smartphone' },
    { id: 2, name: 'Tablet', icon: 'tablet' },
    { id: 3, name: 'Laptop', icon: 'laptop' },
  ];

  navTypes: any = [
    { id: 1, name: 'No sidenav', icon: 'view_stream' },
    { id: 2, name: 'Sidenav', icon: 'view_quilt' },
  ];
  constructor(protected router: Router, protected route: ActivatedRoute, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    public bottomSheetRef: MatBottomSheetRef<PageWizardComponent>) { }

  ngOnInit() {
    const selectedDeviceIndex = this.devices.findIndex((type: any) => type.isClicked === true);
    this.enableNext = selectedDeviceIndex >= 0 ? true : false;
  }

  onDeviceClick(dType: any) {
    this.enableNext = true;
    this.devices.forEach((element: any) => {
      element.isClicked = false;
    });
    dType.isClicked = !dType.isClicked;
    localStorage.setItem('selectedDevice', JSON.stringify(dType));
  }
  onDeviceNext() {
    this.showDevices = false;
    const selectedNavIndex = this.navTypes.findIndex((type: any) => type.isClicked === true);
    this.enableNext = selectedNavIndex >= 0 ? true : false;
  }
  onBack() {
    const selectedDeviceIndex = this.devices.findIndex((type: any) => type.isClicked === true);
    this.enableNext = selectedDeviceIndex >= 0 ? true : false;
    this.showDevices = true;
  }
  onNavTypeClick(type: any) {
    this.enableNext = true;
    this.navTypes.forEach((element: any) => {
      element.isClicked = false;
    });
    type.isClicked = !type.isClicked;
    localStorage.setItem('selectedNav', JSON.stringify(type));
  }
  onNavNext() {
    this.bottomSheetRef.dismiss();
    this.router.navigate(['/admin/builder/page/new']);
  }
}
