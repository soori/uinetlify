import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-create-page-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class PageListCreateComponent implements OnInit, OnChanges {
  @Input() listConfig: any = {};
  @Input() listElements: Array<any> = [];

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    if (this.listElements) {
      this.listElements = this.listElements;
    }
    if (this.listConfig) {
      this.listConfig = this.listConfig;
    }
  }
}
