import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { Location } from '@angular/common';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from 'src/app/app.service';
import { AppSettings } from 'src/app/app.settings';
import { Settings } from 'src/app/app.settings.model';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { CardComponent } from '../config/card/card.component';
import { PageBuilderService } from '../page.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class PageCreateComponent implements OnInit, OnChanges {


  @Input() pageDetails: any = {
    name: null,
    parent_code: 'abc',
    router_link: null,
    page_code: 'abc',
    permission_code: 'abc'
  };

  pageTodoItems: any[] = [];
  pageItems: any;
  dynmaicForm: any;
  buttonGroup: any;
  dynamicForm: any;
  dynamicFormFields: any[] = [];
  filteredDynamicFields: any[] = [];
  cmpTemplates: any;
  isLoading: Boolean = false;
  showPagePreview: Boolean = false;
  addPage: any;
  pageData: any[] = [];
  pages: any;
  formTemplates: any[] = [];
  cardTemplates: any[] = [];
  tabTemplates: any[] = [];
  gridTemplates: any[] = [];
  buttonTemplates: any[] = [];
  stepperTemplates: any[] = [];
  navTemplates: any[] = [];
  treeTemplates: any[] = [];

  pageId: string | null;
  isItemReplaced: Boolean = false;
  public settings: Settings;
  isSideNavOpened: Boolean = false;
  searchItems: any = [];
  search404: Boolean = false;
  hideAccordian: Boolean = false;
  pageDragItems: any = [];
  bannerTemplates: any = [];
  pageElements: any[] = [];
  listElements: any[] = [];

  pageGroups: any = [
    { value: 'PG1', name: 'Page Group 1' },
    { value: 'PG2', name: 'Page Group 2' },
    { value: 'PG3', name: 'Page Group 3' },
    { value: 'PG4', name: 'Page Group 4' },
    { value: 'PG5', name: 'Page Group 5' }
  ];
  topPosition = 0;
  viewConfig: any = [
    { id: 1, name: 'Show as is' },
    { id: 2, name: 'Show as Read only' },
    { id: 3, name: 'Show as Label' },
  ];
  pageItemsBackup: any = [];
  listTemplate: any = [];
  showListDropArea = false;
  listConfig: any = {};
  cardData: any;
  hasConfig = false;
  constructor(private service: PageBuilderService, private router: Router, private route: ActivatedRoute,
    private location: Location, public dialog: MatDialog, private _messageService: MessageService, protected translate: TranslateService,
    protected store: Store<UserLanguageState>, public appSettings: AppSettings, private appService: AppService) {
    this.settings = this.appSettings.settings;
    this.settings.sidenavIsOpened = false;
    this.appService.changeData(this.settings);
    this.isSideNavOpened = this.settings.sidenavIsOpened;
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit() {
    this.getPageItems();
    this.pageId = this.route.snapshot.paramMap.get('id');
    window.addEventListener('scroll', this.scroll, true);
    const cloneItems: Array<any> = JSON.parse(localStorage.getItem('clonePageItems') || 'null');
    if (cloneItems?.length > 0) {
      this.pageElements = cloneItems;
    }
  }
  scroll = (event: any): void => {
    const number = event.srcElement.scrollTop;
    this.topPosition = number;
  }

  ngOnChanges() {
    if (this.pageDetails && this.pageDetails.layoutItems) {
      this.pageElements = this.pageDetails.layoutItems;
      if (this.pageElements[0].details.type.id === 13) {
        this.listConfig = this.pageDetails.listDetails;
        this.showListDropArea = true;
        this.hasConfig = true;
      }
    }
  }

  getPageItems() {
    this.isLoading = true;
    this.service.getComponentTemplates().subscribe(data => {
      this.isLoading = false;
      this.pageDragItems = data;
      this.pageItemsBackup = this.createCopy(this.pageDragItems);
      this.processPageData(data);
    });
  }
  processPageData(data: any) {
    this.formTemplates = [];
    this.cardTemplates = [];
    this.tabTemplates = [];
    this.gridTemplates = [];
    this.buttonTemplates = [];
    this.stepperTemplates = [];
    this.navTemplates = [];
    this.treeTemplates = [];
    this.bannerTemplates = [];
    this.cmpTemplates = data;
    this.cmpTemplates.push({ details: { name: 'List', type: { id: 13, name: 'List' } } });
    this.cmpTemplates.forEach((item: any) => {
      if (item.details.type.id === 7 || item.details.type.id === 3) {
        localStorage.removeItem('template');
        item.isPageBuilderView = true;
      }
      this.pageTodoItems.push(item);
      if (item.details.type.id === 1) {
        item.icon = 'assessment';
        this.formTemplates.push(item);
      } else if (item.details.type.id === 2) {
        item.icon = 'credit_card';
        this.cardTemplates.push(item);
      } else if (item.details.type.id === 3) {
        item.icon = 'tab';
        this.tabTemplates.push(item);
      } else if (item.details.type.id === 4) {
        item.icon = 'table_chart';
        this.gridTemplates.push(item);
      } else if (item.details.type.id === 5) {
        item.icon = 'toggle_on';
        this.buttonTemplates.push(item);
      } else if (item.details.type.id === 7) {
        item.icon = 'view_headline';
        this.stepperTemplates.push(item);
      } else if (item.details.type.id === 6) {
        item.icon = 'maximize';
        this.navTemplates.push(item);
      } else if (item.details.type.id === 10) {
        item.icon = 'credit_card';
        this.treeTemplates.push(item);
      } else if (item.details.type.id === 13) {
        item.icon = 'assessment';
        this.listTemplate.push(item);
      }
    });
  }
  drop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer !== event.container) {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      const droppedItem: any = event.container.data[event.currentIndex];
      if (droppedItem.details.type.id === 13) {
        // List items
        this.showListDropArea = true;
        // this.pageElements.splice(event.currentIndex, 1);
      } else {
        if (event.container.id === 'listElements_1') {
          // List elemets drop event
          const doneCopy = this.createCopy(this.listElements[event.currentIndex]);
          this.listElements.forEach((item, i) => {
            item.position = i;
            this.listElements[event.currentIndex] = doneCopy;
          });
          this.hasConfig = true;
          // this.listDataConfig.listData.push(doneCopy);
        } else {
          // Page elemets drop event
          const doneCopy = this.createCopy(this.pageElements[event.currentIndex]);
          this.pageElements.forEach((item, i) => {
            item.position = i;
            if (item.details.type.id === 7 || item.details.type.id === 3 || item.details.type.id === 4 || item.details.type.id === 10) {
              localStorage.removeItem('template');
              item.isPageBuilderView = true;
            }
            if ((doneCopy._id === item._id) && this.pageId) {
              this.pageElements[i] = doneCopy;
            }
            // Page view config for form fields
            this.processPageView(item);
          });
        }
      }
    } else if (event.previousIndex !== event.currentIndex) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
    }
  }

  createCopy(orig: any) {
    return JSON.parse(JSON.stringify(orig));
  }
  removeItem(i: number) {
    this.pageElements.splice(i, 1);
    this.dynamicFormFields.splice(i, 1);
  }

  submitPage() {
    this.pageDetails.permission_code = this.pageDetails.parent_code + '-' + this.pageDetails.page_code;
    const page = {
      ...this.pageDetails,
      layoutItems: this.pageElements,
      listDetails: this.listConfig
    };
    this.service.savePage(page).subscribe(data => {
      this.isLoading = false;
      this._messageService.info('Page saved successfully');
      this.router.navigate(['./../home'], { relativeTo: this.route });
    },
      err => {
        this._messageService.info('Error occured, Try again !');
        this.isLoading = false;
      });
  }

  showPreview() {
    this.showPagePreview = true;
    if (this.listConfig) {
      const listItem = {
        details: {
          name: 'List',
          type: { id: 13, name: 'List' }
        },
        config: this.listConfig
      };
      const listItemIndex: any = this.pageElements.findIndex((item: any) => item.details.type.id === 13)
      if (listItemIndex < 0) {
        this.pageElements.push(listItem);
      } else {
        this.pageElements[listItemIndex] = listItem;
      }
    }
    this.pageItems = {
      layoutItems: this.pageElements,
    };
  }
  getDynamicForm(result: any) {
    this.dynamicForm = result;
  }
  updateView(mode: any) {
    this.showPagePreview = mode;
  }
  goBack() {
    this.location.back();
  }


  UpdatePage() {
    this.pageDetails.permission_code = this.pageDetails.parent_code + '-' + this.pageDetails.page_code;
    this.isLoading = true;
    const page = {
      ...this.pageDetails,
      layoutItems: this.pageElements,
      listDetails: this.listConfig
    };
    this.service.updatePage(page, this.pageId).subscribe((data: any) => {
      this.isLoading = false;
      this._messageService.info('Page Updated successfully');
      this.router.navigate(['./../../home'], { relativeTo: this.route });
    },
      (err: any) => {
        this._messageService.info('Error occured, Try again !');
        this.isLoading = false;
      });
  }

  filterPageItems(event: any) {
    this.cmpTemplates = this.pageDragItems;
    const value = event.target.value;
    if (value) {
      this.searchItems = this.cmpTemplates.filter((el: any) =>
        el.details.name.toLowerCase().toString().includes(value.toLowerCase().toString()));
      this.processPageData(this.searchItems);
    } else {
      this.searchItems = [];
      this.processPageData(this.pageDragItems);
      this.search404 = false;
      this.hideAccordian = false;
    }
  }
  processPageView(item: any) {
    if (item.details.type.id === 1) {
      item.attributes.forEach((attr: any, index: number) => {
        if (attr.securityAttrributes.allowOverride && this.pageDetails.pageViewConfig) {
          if (this.pageDetails.pageViewConfig.id === 1) {
            const fieldItem = this.pageItemsBackup.filter((el: any) => el._id === item._id);
            const field = fieldItem[0].attributes[index];
            attr.securityAttrributes.displayType = field.securityAttrributes.displayType;
          } if (this.pageDetails.pageViewConfig.id === 2) {
            attr.securityAttrributes.displayType = { id: 2, name: 'Readonly', display_name: 'Read Only' };
          } else if (this.pageDetails.pageViewConfig.id === 3) {
            attr.securityAttrributes.displayType = { id: 3, name: 'Label', display_name: 'Label' };
          }
        }
      });
    }
  }
  onPageViewConfigChange(value: any) {
    const pageMode = value;
    this.pageElements.forEach((element: any) => {
      if (element.details.type.id === 1) {
        this.processPageView(element);
      }
    });
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  openCardConfig(item: any) {
    const dialogRef = this.dialog.open(CardComponent, {
      width: '100vw',
      data: JSON.parse(JSON.stringify(this.listConfig))
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listConfig = result;
        this.hasConfig = true;
      }
    });
  }

  removeListItem() {
    this.showListDropArea = false;
    this.listConfig = {};
    this.hasConfig = false;
    const index = this.pageElements.indexOf((item: any) => item.details.type.id === 13);
    this.pageElements.splice(index, 1);
  }

}
