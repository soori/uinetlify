import { Component, OnInit, ViewChild } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { AppService } from 'src/app/app.service';
import { AppSettings } from 'src/app/app.settings';
import { Settings } from 'src/app/app.settings.model';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { PageBuilderService } from '../page.service';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-page-home-cmp',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class PageHomeComponent implements OnInit {
  showDefaultFilter: Boolean = true;
  dataSource: MatTableDataSource<any>;
  displayedColumns: any = ['name', 'router_link', 'parent_code', 'permission_code'];
  headers = [{ columnName: 'name', display_name: 'name' },
  { columnName: 'router_link', display_name: 'URL' },
  { columnName: 'parent_code', display_name: 'parent' },
  { columnName: 'permission_code', display_name: 'permission' }
  ];
  noData: any;
  isLoading: Boolean = false;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  templates: any;
  template: any;
  pages: any;
  public settings: Settings;
  hasClonePermission = false;
  selection = new SelectionModel<any>(false, []);
  enableCloneView = false;
  constructor(protected router: Router, protected route: ActivatedRoute, private service: PageBuilderService,
    protected translate: TranslateService, protected store: Store<UserLanguageState>, public appSettings: AppSettings,
    private _bottomSheet: MatBottomSheet, private appService: AppService) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
    this.settings = this.appSettings.settings;
    this.settings.sidenavIsOpened = true;
    this.appService.changeData(this.settings);
  }

  ngOnInit() {
    this.getPages();
    if (this.hasClonePermission) {
      this.displayedColumns.unshift('select');
      this.headers.unshift({ columnName: 'select', display_name: '' });
    }
  }
  getPages() {
    this.service.getPages().subscribe(data => {
      this.pages = data;
      this.dataSource = new MatTableDataSource(this.pages);
      this.noData = this.dataSource.connect().pipe(map(dataa => dataa.length === 0));
      if (this.pages.length > 0) {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  selectedRowItem(selecteditem: any) {
    this.router.navigate(['./../edit/' + selecteditem._id], { relativeTo: this.route });
  }
  applyFilter(event: any) {
    let filterValue = event.target.value;
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  checkSelection(event: any, selectedItem: any) {
    this.selection.toggle(selectedItem);
    this.selection.isSelected(selectedItem);
    this.enableCloneView = true;
  }
  onCheckBoxChange(event: any, row: any) {
    if (event.checked) {
      this.selection.toggle(row);
      this.selection.isSelected(true);
      this.enableCloneView = true;
    } else {
      this.selection.toggle(row);
      this.selection.isSelected(false);
      this.enableCloneView = false;
    }
  }
  onChecked(event: any, row: any) {
    this.selection.isSelected(row);
    this.enableCloneView = !this.enableCloneView;
  }
  clonePage() {
    const selectedPage: any = this.selection.selected[0];
    localStorage.setItem('clonePageItems', JSON.stringify(selectedPage.layoutItems));
    this.router.navigate(['./../new/'], { relativeTo: this.route });

  }
  modifyPage() {
    this.router.navigate(['./../edit/' + this.selection.selected[0]._id], { relativeTo: this.route });
  }
  routeToNew() {
    localStorage.removeItem('clonePageItems');
    this.router.navigate(['./../new/'], { relativeTo: this.route });
  }
}
