import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageCreateComponent } from './create/create.component';
import { PageEditComponent } from './edit/edit.component';
import { PageHomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: PageHomeComponent, data: { breadcrumb: 'Home' } },
  { path: 'new', component: PageCreateComponent, data: { breadcrumb: 'Create' } },
  { path: 'edit/:id', component: PageEditComponent, data: { breadcrumb: 'Edit' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageBuilderRoutingModule { }
