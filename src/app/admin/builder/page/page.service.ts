import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class PageBuilderService {

    constructor(private http: HttpClient) { }


    savePage(page: any) {
        return this.http.post(`${environment.apiEndpoint}/pagebuilder`, page);
    }
    getPages() {
        return this.http.get(`${environment.apiEndpoint}/pagebuilder`);
    }
    getComponentTemplates() {
        return this.http.get(`${environment.apiEndpoint}/formbuilder`);
    }
    updateForm(form: any, id: any) {
        return this.http.put(`${environment.apiEndpoint}/formbuilder/${id}`, form);
    }
    deleteForm(id: any) {
        return this.http.delete(`${environment.apiEndpoint}/formbuilder/${id}`);
    }
    getPageItems(id: string) {
        return this.http.get(`${environment.apiEndpoint}/pagebuilder/${id}`);
    }
    updatePage(page: any, id: string | null) {
        return this.http.put(`${environment.apiEndpoint}/pagebuilder/${id}`, page);
    }
    getCards(endpoint: string) {
        return this.http.get(`${endpoint}`);
    }
}
// ./assets/data/cards.json