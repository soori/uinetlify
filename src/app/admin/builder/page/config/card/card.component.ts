import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { PageBuilderService } from '../../page.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  layouts: any[] = [
    { id: 0, name: 'row wrap', dispalyName: 'Row' },
    { id: 1, name: 'column', dispalyName: 'Column' }
  ];
  layoutColumns: any[] = [
    { id: 0, name: 'One Column', flex: '1 1 100', flexGap: '5px' },
    { id: 1, name: 'Two Column', flex: '1 1 47', flexGap: '5px' },
    { id: 2, name: 'Three Column', flex: '1 1 29', flexGap: '5px' },
    { id: 3, name: 'Four Column', flex: '1 1 21', flexGap: '10px' },
  ];
  cardConfig: any = {};
  isLoading = false;
  constructor(public dialogRef: MatDialogRef<CardComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    protected translate: TranslateService, private service: PageBuilderService, protected store: Store<UserLanguageState>) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
  }

  ngOnInit(): void {
    if (this.data) {
      this.cardConfig = this.data;
    }
  }
  onNoClick() {
    this.dialogRef.close();
  }

  onYesClick() {
    this.dialogRef.close(this.cardConfig);
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
