import { Component, OnInit, ViewChild } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { AppService } from 'src/app/app.service';
import { AppSettings } from 'src/app/app.settings';
import { Settings } from 'src/app/app.settings.model';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { BottomSheetViewComponent } from '../../../components/bottom-sheet/view/view.component';
import { DialogViewComponent } from '../../../components/dialog/view/view.component';
import { TemplateBuilderService } from '../template.service';
import { TemplateWizardComponent } from '../wizard/wizard.component';

@Component({
  selector: 'app-template-home-cmp',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class FormTemplateHomeComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  showDefaultFilter: Boolean = true;
  dataSource: MatTableDataSource<any>;
  displayedColumns: any = ['name', 'cmpType', 'layoutName', 'layoutColumn'];
  headers = [{ columnName: 'name', display_name: 'template name' },
  { columnName: 'cmpType', display_name: 'type' },
  { columnName: 'layoutName', display_name: 'layout' },
  { columnName: 'layoutColumn', display_name: 'no of layout columns' }
  ];
  noData: any;
  isLoading: Boolean = false;
  templates: any;
  public settings: Settings;
  constructor(protected router: Router, protected route: ActivatedRoute, private service: TemplateBuilderService,
    protected translate: TranslateService, protected store: Store<UserLanguageState>, public appSettings: AppSettings,
    private _bottomSheet: MatBottomSheet, private dialog: MatDialog, private appService: AppService) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
    this.settings = this.appSettings.settings;
    this.settings.sidenavIsOpened = true;
    this.appService.changeData(this.settings);
  }

  ngOnInit() {
    this.getTemplates();
  }
  applyFilter(event: any) {
    let filterValue = event.target.value;
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  selectedRowItem(selectedRow: any) {
    selectedRow.isTemplateView = true;
    localStorage.setItem('template', JSON.stringify(selectedRow));
    this.router.navigate(['./../edit/' + selectedRow._id], { relativeTo: this.route });
  }
  getTemplates() {
    this.isLoading = true;
    this.service.getTemplates().subscribe(data => {
      this.templates = data;
      this.isLoading = false;
      for (const row of this.templates) {
        row.layoutName = row.layout ? row.layout.name === 'row wrap' ? 'row' : 'column' : 'Not Applicable';
        row.layoutColumn = row.layout ? row.layout.column ? row.layout.column : 'Not Applicable' : 'Not Applicable';
        row.name = row.details.name;
        row.cmpType = row.details.type.name;
      }
      this.dataSource = new MatTableDataSource(this.templates);
      this.noData = this.dataSource.connect().pipe(map(data1 => data1.length === 0));
      if (this.templates.length > 0) {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  routeCreate() {
    const template = JSON.parse(localStorage.getItem('template') || 'null');
    if (template) {
      template.isTemplateView = false;
    }
    localStorage.setItem('template', JSON.stringify(template));
    this.openWizard();
  }
  openWizard(): void {
    localStorage.removeItem('template');
    const bottomSheetRef = this._bottomSheet.open(TemplateWizardComponent, {
      hasBackdrop: true,
      panelClass: 'custom-bottom-sheet-wizard-width'
    });
  }
}
