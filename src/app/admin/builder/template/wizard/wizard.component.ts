import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-template-wizard-cmp',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class TemplateWizardComponent implements OnInit {

  inputTypes: any = [
    { id: 1, name: 'Form', icon: 'assignment' },
    { id: 2, name: 'Card', icon: 'web_asset' },
    { id: 3, name: 'Tab', icon: 'tab' },
    { id: 4, name: 'Grid', icon: 'view_module' },
    { id: 5, name: 'Button', icon: 'keyboard' },
    { id: 7, name: 'Stepper', icon: 'format_list_numbered' },
    { id: 8, name: 'Bottom sheet', icon: 'call_to_action' },
    { id: 9, name: 'Dialog', icon: 'crop_16_9' },
    { id: 10, name: 'Tree', icon: 'account_tree' },
    { id: 11, name: 'Charts', icon: 'show_chart' },
    { id: 12, name: 'Banner', icon: 'photo' }

  ];

  basicTypes: any = [
    { id: 1, name: 'TEMPLATE.WIZARD.FORM', icon: 'assignment' },
    { id: 2, name: 'TEMPLATE.WIZARD.CARD', icon: 'web_asset' },
    { id: 5, name: 'TEMPLATE.WIZARD.BUTTON', icon: 'keyboard' },
    { id: 8, name: 'TEMPLATE.WIZARD.BOTTOM-SHEET', icon: 'call_to_action' },
    { id: 9, name: 'TEMPLATE.WIZARD.DIALOG', icon: 'crop_16_9' },
    { id: 12, name: 'TEMPLATE.WIZARD.BANNER', icon: 'photo' }
  ];

  advancedTypes: any = [
    { id: 3, name: 'TEMPLATE.WIZARD.TAB', icon: 'tab' },
    { id: 4, name: 'TEMPLATE.WIZARD.GRID', icon: 'view_module' },
    { id: 7, name: 'TEMPLATE.WIZARD.STEPPER', icon: 'format_list_numbered' },
    { id: 10, name: 'TEMPLATE.WIZARD.TREE', icon: 'account_tree' },
    { id: 11, name: 'TEMPLATE.WIZARD.CHARTS', icon: 'show_chart' },
  ];
  isClicked: Boolean = false;
  enableNext: Boolean = false;
  preventSingleClick: Boolean = false;
  timer: any;
  delay: Number;
  constructor(protected router: Router, protected route: ActivatedRoute,
    public bottomSheetRef: MatBottomSheetRef<TemplateWizardComponent>) { }

  ngOnInit() {
  }
  onCardClick(card: any) {
    this.enableNext = true;
    this.basicTypes.forEach((item: any) => {
      item.isClicked = false;
    });
    this.advancedTypes.forEach((item: any) => {
      item.isClicked = false;
    });
    card.isClicked = !card.isClicked;
    localStorage.setItem('selectedTemplate', JSON.stringify(card));
  }
  onCardDblClick(card: any) {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    this.basicTypes.forEach((item: any) => {
      item.isClicked = false;
    });
    this.advancedTypes.forEach((item: any) => {
      item.isClicked = false;
    });
    card.isClicked = !card.isClicked;
    this.bottomSheetRef.dismiss();
    this.router.navigate(['/admin/builder/template/new']);
    localStorage.setItem('selectedTemplate', JSON.stringify(card));
  }
  onNext() {
    this.bottomSheetRef.dismiss();
    this.timer = 0;
    this.preventSingleClick = false;
    const delay = 200;
    this.timer = setTimeout(() => {
      if (!this.preventSingleClick) {
        this.router.navigate(['/admin/builder/template/new']);
      }
    }, delay);
  }
}
