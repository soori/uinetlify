import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class TemplateBuilderService {
    constructor(private http: HttpClient) { }
    deleteTemplate(id: any) {
        return this.http.delete(`${environment.apiEndpoint}/formbuilder/${id}`);
    }
    getTemplates() {
        return this.http.get(`${environment.apiEndpoint}/formbuilder`);
    }
}
