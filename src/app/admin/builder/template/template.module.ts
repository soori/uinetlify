import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemeModule } from 'src/app/theme/theme.module';
import { AdminComponentsModule } from '../../components/components.module';
import { FormTemplateCreateComponent } from './create/create.component';
import { FormTemplateEditComponent } from './edit/edit.component';
import { FormTemplateHomeComponent } from './home/home.component';
import { TemplateBuilderRoutingModule } from './template-routing.module';
import { TemplateWizardComponent } from './wizard/wizard.component';


@NgModule({
  declarations: [
    FormTemplateHomeComponent,
    FormTemplateCreateComponent,
    FormTemplateEditComponent,
    TemplateWizardComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    DragDropModule,
    AdminComponentsModule,
    TemplateBuilderRoutingModule,
    AdminComponentsModule
  ],
  entryComponents: [TemplateWizardComponent]
})
export class TemplateBuilderModule { }
