import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from 'src/app/app.service';
import { AppSettings } from 'src/app/app.settings';
import { Settings } from 'src/app/app.settings.model';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { TemplateBuilderService } from '../template.service';

@Component({
  selector: 'app-template-create-cmp',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class FormTemplateCreateComponent implements OnInit {
  config: any = { type: {}, name: '' };
  public settings: Settings;
  constructor(public dialog: MatDialog, protected translate: TranslateService, protected store: Store<UserLanguageState>,
    private templateService: TemplateBuilderService, public appSettings: AppSettings, private appService: AppService) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
    this.settings = this.appSettings.settings;
    this.settings.sidenavIsOpened = false;
    this.appService.changeData(this.settings);
  }

  ngOnInit() {
    this.config.type = JSON.parse(localStorage.getItem('selectedTemplate') || 'null');
  }

  onResponsiveChange() {
    if (this.config.isResponsive.id === 0) {
      this.config.layout = { id: 0, name: 'row wrap', dispalyName: 'Row' };
      this.config.layoutColumn = { id: 0, name: 'One Column', flex: '1 1 auto', flexGap: '10px' };
    }
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

}
