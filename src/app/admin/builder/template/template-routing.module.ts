import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormTemplateCreateComponent } from './create/create.component';
import { FormTemplateEditComponent } from './edit/edit.component';
import { FormTemplateHomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: FormTemplateHomeComponent, pathMatch: 'full', data: { breadcrumb: 'Home' } },
  {
    path: 'new', component: FormTemplateCreateComponent, pathMatch: 'full', data: { breadcrumb: 'Create' }

  },
  { path: 'edit/:id', component: FormTemplateEditComponent, pathMatch: 'full', data: { breadcrumb: 'Preview' } },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateBuilderRoutingModule { }
