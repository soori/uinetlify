import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from 'src/app/app.service';
import { AppSettings } from 'src/app/app.settings';
import { Settings } from 'src/app/app.settings.model';
import { MessageService } from 'src/app/shared/snackbar/message.service';
import { UserLanguageState } from 'src/app/store/userlang.state';
import { TemplateBuilderService } from '../template.service';

@Component({
  selector: 'app-template-edit-cmp',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class FormTemplateEditComponent implements OnInit {
  template: any;
  config: any;
  cmpAttr: any;
  isLoading: Boolean = false;
  public settings: Settings;
  constructor(private router: Router, private route: ActivatedRoute,
    private service: TemplateBuilderService, private messageService: MessageService,
    protected translate: TranslateService, protected store: Store<UserLanguageState>, public appSettings: AppSettings,
    private appService: AppService) {
    this.store.select(state => state.userLangPref).subscribe(userContext => this.translate.use(userContext[0].prefLang));
    this.settings = this.appSettings.settings;
    this.settings.sidenavIsOpened = false;
    this.appService.changeData(this.settings);
  }

  ngOnInit() {
    this.template = JSON.parse(localStorage.getItem('template') || 'null');
    this.config = this.template.details;
    this.cmpAttr = this.template.attributes;
  }
  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
  delete() {
    this.isLoading = true;
    this.service.deleteTemplate(this.template._id).subscribe(data => {
      this.isLoading = false;
      this.messageService.info('Template deleted successfully');
      this.router.navigate(['./../../']);
    },
      err => {
        this.isLoading = false;
        this.messageService.warn('Error occures, Try again');
      });
  }
}
