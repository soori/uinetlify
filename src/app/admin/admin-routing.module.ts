import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WithSideNavRoutingComponent } from '../theme/routingpage/withsidenav.component';
import { AppRouteComponent } from './route/route.component';

export const routes = [
  {
    path: '', component: WithSideNavRoutingComponent,
    children: [
      { path: '', redirectTo: 'builder', pathMatch: 'full' },
      { path: 'builder', loadChildren: './builder/builder.module#BuilderModule' },
      { path: 'pages', loadChildren: './configpages/config-pages.module#AdminConfigPagesModule' },
      { path: '**', component: AppRouteComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
