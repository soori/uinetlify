import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Settings } from './app.settings.model';

@Injectable({
    providedIn: 'root'
})
export class AppService {
    public settings: Settings;
    private dataSource = new BehaviorSubject(this.settings);
    currentData = this.dataSource.asObservable();

    constructor(private http: HttpClient) { }

    getPages() {
        return this.http.get(`${environment.apiEndpoint}/pagebuilder`);
    }

    getPageByUrl(url: any) {
        return this.http.get(`${environment.apiEndpoint}/pagebuilder/byUrl${url}`);
    }

    changeData(data: any) {
        this.dataSource.next(data);
    }
}
